function Configs = load_configs()

Configs = containers.Map;
Configs('2019-8-13_7-16-36_xyz_samples.txt') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);
Configs('2019-8-13_8-18-21_samples.txt') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);
Configs('2019-8-13_10-7-16_samples.txt') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);
Configs('2019-8-22_7-49-2_samples.txt') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);

% square 20x20 NHorz = 3, splitting 2
Configs('2019-9-4_11-12-39_sampler.log') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);

% poche Erwan NHorz = 3, splitting 2
Configs('2019-9-4_11-14-47_sampler.log') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);

% Demo piece v4 NHorz = 3, splitting 2
Configs('2019-9-4_11-20-2_sampler.log') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);

% Demo piece v4 NHorz = 5, split 200 
Configs('2019-9-4_11-40-13_sampler.log') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);

Configs('2019-9-4_11-43-35_sampler.log') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);

Configs('2019-9-4_11-54-45_sampler.log') = struct('vmax', 15, 'amax', 20000, 'jmax', 1500000, 'dt', 1e-4);
end