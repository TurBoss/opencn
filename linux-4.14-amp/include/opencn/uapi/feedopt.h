/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef UAPI_FEEDOPT_H
#define UAPI_FEEDOPT_H

#include <matlab_headers.h>

#include <opencn/uapi/hal.h>
#include <c_spline.h>

#define FEEDOPT_DEV_NAME "/dev/opencn/feedopt/0"
#define FEEDOPT_DEV_MAJOR 108

#define FEEDOPT_SHMEM_NAME "feedopt"
#define FEEDOPT_SHMEM_KEY_CONFIG 65
#define FEEDOPT_SHMEM_KEY_CURV 66


/* Feedopt component */
#define FEEDOPT_DEV_MINOR 0

/*
 * IOCTL codes
 */
#define FEEDOPT_IOCTL_CONNECT 		_IOW(0x05000000, 0, char)
#define FEEDOPT_IOCTL_DISCONNECT 	_IOW(0x05000000, 1, char)
#define FEEDOPT_IOCTL_WRITE_CFG		_IOW(0x05000000, 2, char)
#define FEEDOPT_IOCTL_READ_CFG		_IOW(0x05000000, 3, char)
#define FEEDOPT_IOCTL_RESET			_IOW(0x05000000, 4, char)

#define FEEDOPT_RT_QUEUE_SIZE 50

/* Limits from matlab code generation */
#define FOPT_MAX_NCOEFF 120
#define FOPT_MAX_NDISCR 200
#define FOPT_MAX_NBREAK 100

#define FOPT_EPS (1e-6)

#define FOPT_SPLINE_ELEM_MAX (FOPT_MAX_NCOEFF * FOPT_MAX_NDISCR)


typedef struct {
	int ndiscr;
	int nbreak;
	int nhorz;
	double vmax, amax, jmax;
	int spline_degree;
} feedopt_opt_config;


typedef struct {
	char name[HAL_NAME_LEN];
	int channel;
	int depth;
} feedopt_connect_args_t;

typedef struct {
	CurvStruct curve;
	double coeffs[FOPT_MAX_NCOEFF];
	int coeff_count;
} optimized_curv_struct_t;

typedef struct {
	double BasisVal[FOPT_SPLINE_ELEM_MAX];
	double BasisValD[FOPT_SPLINE_ELEM_MAX];
	double BasisValDD[FOPT_SPLINE_ELEM_MAX];
	double BasisIntegr[FOPT_MAX_NCOEFF];
	size_t n_sample, n_coeff;
	int size_val[2];
	int size_integr[2];
} spline_base_t;

PushStatus feedopt_push_opt(CurvStruct opt_struct, double *coeff, int ncoeff);
void queue_push(int id, CurvStruct value);
void queue_resize(int id, int n, CurvStruct default_value);
CurvStruct queue_get(int id, int k);
void queue_set(int id, int k, CurvStruct value);
void queue_clear(int id);
int queue_size(int id);
int feedopt_read_gcode(int id);

#endif /* FEEDOPT_H */
