#ifndef __GSL_MATRIX_H__
#define __GSL_MATRIX_H__

#include <opencn/gsl/gsl_matrix_complex_long_double.h>
#include <opencn/gsl/gsl_matrix_complex_double.h>
#include <opencn/gsl/gsl_matrix_complex_float.h>

#include <opencn/gsl/gsl_matrix_long_double.h>
#include <opencn/gsl/gsl_matrix_double.h>
#include <opencn/gsl/gsl_matrix_float.h>

#include <opencn/gsl/gsl_matrix_ulong.h>
#include <opencn/gsl/gsl_matrix_long.h>

#include <opencn/gsl/gsl_matrix_uint.h>
#include <opencn/gsl/gsl_matrix_int.h>

#include <opencn/gsl/gsl_matrix_ushort.h>
#include <opencn/gsl/gsl_matrix_short.h>

#include <opencn/gsl/gsl_matrix_uchar.h>
#include <opencn/gsl/gsl_matrix_char.h>


#endif /* __GSL_MATRIX_H__ */
