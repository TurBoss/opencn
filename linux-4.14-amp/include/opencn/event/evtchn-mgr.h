
/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef EVTCHN_MGR_H
#define EVTCHN_MGR_H

#define __HYPERVISOR_event_channel_op      1

/*
 * Directcomm event management
 */
typedef enum {
	DC_NO_EVENT,
	DC_ENABLE_RT,		/* as an example from non-RT to RT... */
	DC_VDUMMY_INIT,
	DC_VLOG_INIT,
	DC_VLOG_FLUSH,
	DC_LCEC_INIT,

	DC_EVENT_MAX		/* Used to determine the number of DC events */
} dc_event_t;

/*
 * Callback function associated to dc_event.
 */
typedef void(dc_event_fn_t)(dc_event_t dc_event);

extern atomic_t dc_outgoing_domID[DC_EVENT_MAX];
extern atomic_t dc_incoming_domID[DC_EVENT_MAX];

void evtchn_mgr_init(void);
long do_event_channel_op(int cmd, void *args);
void dump_evtchn_info(void) ;
int alloc_evtchn(domid_t domid, int *evtchn);

void do_sync_dom(int slotID, dc_event_t);
void rtdm_do_sync_dom(int domID, dc_event_t dc_event);

void dc_stable(int dc_event);
void rtdm_tell_dc_stable(int dc_event);

void register_dc_event_callback(dc_event_t dc_event, dc_event_fn_t *callback);
void rtdm_register_dc_event_callback(dc_event_t dc_event, dc_event_fn_t *callback);

asmlinkage void evtchn_do_upcall(struct pt_regs *regs);

static inline int HYPERVISOR_event_channel_op(int cmd, void *op)
{
	unsigned long flags;
	struct pt_regs pt_regs;

	/* Prepare to enter a pseudo-hypercall context.
	 * IRQs are disabled and a context frame is built up.
	 * It replaces the standard path in AVZ taking into account
	 * possible event channel processing in the upcall.
	 */

	pt_regs.flags = native_save_fl();

	local_irq_save(flags);

	do_event_channel_op(cmd, op);

	if (per_cpu(domain, smp_processor_id()).shared_info->evtchn_upcall_pending)
		evtchn_do_upcall(&pt_regs);

	local_irq_restore(flags);

	return 0;
}

#endif /* EVTCHN_MGR_H */
