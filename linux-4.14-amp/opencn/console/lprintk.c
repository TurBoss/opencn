/*
 * lprintk.c Light printk
 */

#include <linux/smp.h>

#include <opencn/opencn.h>
#include <opencn/console.h>

#include <opencn/frontend/vlog.h>

extern int vsnprintf(char *buf, size_t size, const char *fmt, va_list args);
extern bool send_to_uart(char *str);
extern void smp_kick_vt_out(void);

/* opencn - Used to store the character to display. */
atomic64_t str_to_print;

void __lprintk(const char *format, va_list va) {
	char buf[CONSOLEIO_BUFFER_SIZE];
	char *__start;

	vsnprintf(buf, CONSOLEIO_BUFFER_SIZE, format, va);

	__start = buf;

	/* Skip printk prefix issued by the standard Linux printk if any */
	if ((*__start != 0) && (*__start < 10))
		__start += 2;

	if (vlog_enabled && (smp_processor_id() == OPENCN_RT_CPU)) {

		vlog_send(__start);
		return;
	}

	if (!send_to_uart(__start)) {
		if (smp_processor_id() == OPENCN_RT_CPU) {
			/* Make sure a previous IPI is not being processed... */
			while (atomic64_cmpxchg(&str_to_print, 0ull, (u64) __start) != 0ull) ;

			smp_kick_vt_out();
		} else {
			if (*__start == 0) {
				/* On OPENCN_CPU0, printk() is executed natively */
				printk("%s", (char *) atomic64_read(&str_to_print));

				atomic64_set(&str_to_print, 0);
			} else
				printk("%s", __start);
		}
	}
}

void lprintk(char *format, ...) {

	va_list va;
	va_start(va, format);

	__lprintk(format, va);

	va_end(va);
}

/**
 * Print the contents of a buffer.
 */
void lprintk_buffer(void *buffer, uint32_t n) {
	uint32_t i;

	for (i = 0; i < n; i++)
		lprintk("%02x ", ((char *) buffer)[i]);
	lprintk("\n");
}

/**
 * Print the contents of a buffer. Each element is separated using a given character.
 */
void lprintk_buffer_separator(void *buffer, uint32_t n, char separator) {
	uint32_t i;

	for (i = 0; i < n; i++)
		lprintk("%02x%c", ((char *) buffer)[i], separator);
	lprintk("\n");
}

/**
 * Print an uint64_t number and concatenate a string.
 */
void lprintk_int64_post(s64 number, char *post) {
	uint32_t msb = number >> 32;
	uint32_t lsb = number & 0xffffffff;

	lprintk("%08x %08x%s", msb, lsb, post);
}

/**
 * Print an uint64_t number.
 */
void lprintk_int64(s64 number) {
	lprintk_int64_post(number, "\n");
}

void lprintk_init(void) {
	atomic64_set(&str_to_print, 0);
}
