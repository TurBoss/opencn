/********************************************************************
 *  Copyright (C) 2019  Peter Lichard  <peter.lichard@heig-vd.ch>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

/**
  * @file lcct.c
  */

#include "lcct_gcode.h"
#include "lcct_home.h"
#include "lcct_internal.h"
#include "lcct_jog.h"
#include "lcct_stream.h"

#include <opencn/uapi/lcct.h>

#define HAL_CHECK(expr)                                                                                                \
	{                                                                                                                  \
		int __retval = expr;                                                                                           \
		if (__retval != 0)                                                                                             \
			return __retval;                                                                                           \
	}

#define LCCT_STR_MAXLEN 48

typedef enum { MAIN_IDLE, MAIN_HOMING, MAIN_STREAM, MAIN_GCODE, MAIN_JOG, MAIN_ERR } MAIN_STATE;

// hal_pin_bit_newf(hal_user_t *hal_user, hal_pin_dir_t dir, hal_bit_t **data_ptr_addr, int comp_id, const char *fmt,
// ...)
// int hal_pin_newfv(hal_user_t *hal_user, hal_type_t type, hal_pin_dir_t dir, void **data_ptr_addr, int comp_id, const
// char *fmt, va_list ap)

int hal_pin_newf(hal_user_t *hal_user, hal_type_t type, hal_pin_dir_t dir, void **data_ptr_addr, int comp_id,
				 const char *fmt, ...)
{
	int ret;
	va_list va;
	va_start(va, fmt);
	ret = hal_pin_newfv(hal_user, type, dir, data_ptr_addr, comp_id, fmt, va);
	va_end(va);
	return ret;
}

/*
typedef struct {
	hal_type_t pin_type;
	hal_pin_dir_t pin_dir;
	int off;
	const char *name;
} pin_def_t;
*/


int init_pins_from_def(const pin_def_t *defs, int comp_id, void* pbase)
{
	int ret;
	BUG_ON(pbase == NULL);
	
	while(defs->pin_type != HAL_TYPE_UNSPECIFIED) {
		
		// if (defs->off > base_size - sizeof(void*)) {
		// 	opencn_cprintf(OPENCN_COLOR_BRED, "init_pins_from_def: offset too big !!\n");
		// 	BUG_ON(1);
		// }

		if ((ret = hal_pin_newf(__core_hal_user, defs->pin_type, defs->pin_dir, (void**)(pbase + defs->off), comp_id,
								defs->name)) != 0) {
			opencn_cprintf(OPENCN_COLOR_BRED, "init_pins_from_def: Failed to create pin '%s'\n", defs->name);
			return ret;
		}
	

		defs++;
	}
	return 0;
}

typedef struct {
	// Joint position input sources
	hal_float_t *spindle_cmd_in;

	hal_float_t *joint_pos_cur_in[4];
	hal_float_t *spindle_cur_in;

	hal_bit_t *homed_out;

	// Joint position output
	hal_float_t *target_position_out[4];
	hal_float_t *spindle_cmd_out;
	hal_float_t *spindle_target_velocity;
	hal_bit_t *spindle_active;

	hal_bit_t *disable_abs_jog;

	// Axis mode input
	hal_bit_t *in_mode_csp_in[4];
	hal_bit_t *in_mode_csv_in[4];
	hal_bit_t *in_mode_hm_in[4];
	hal_bit_t *in_mode_inactive_in[4];

	// Axis mode output
	hal_bit_t *set_mode_csp_out[4];
	hal_bit_t *set_mode_csv_out[4];
	hal_bit_t *set_mode_hm_out[4];
	hal_bit_t *set_mode_inactive_out[4];

	// Faults
	hal_bit_t *in_fault_in[4];

	// Offset double controls
	hal_float_t *spinbox_offset_in[3];
	hal_float_t *home_position_in[3];

	// machine mode
	hal_bit_t *set_machine_mode_gcode_in;
	hal_bit_t *set_machine_mode_stream_in;
	hal_bit_t *set_machine_mode_homing_in;
	hal_bit_t *set_machine_mode_jog_in;
	hal_bit_t *set_machine_mode_inactive_in;

	// machine mode leds
	hal_bit_t *in_machine_mode_gcode_out;
	hal_bit_t *in_machine_mode_stream_out;
	hal_bit_t *in_machine_mode_homing_out;
	hal_bit_t *in_machine_mode_jog_out;
	hal_bit_t *in_machine_mode_inactive_out;

	hal_bit_t *homing_finished_out;
	hal_bit_t *gcode_finished_out;
	hal_bit_t *stream_finished_out;
	hal_bit_t *jog_finished_out;

	// sampling control
	hal_bit_t *sampler_enable_out;

	hal_bit_t *fault_reset_in;
	hal_bit_t *fault_reset_out[4];

	hal_s32_t *external_trigger_out;
	hal_s32_t *electrovalve_in;
	hal_s32_t *electrovalve_out;
	hal_float_t *spindle_cur_out;

} lcct_data_t;

#define PIN(member) offsetof(lcct_data_t, member)

static lcct_data_t *data = NULL;
static hal_bit_t *hal_buttons[1024] = {0};
static int hal_button_count = 0;
void add_hal_button(hal_bit_t *pin) { hal_buttons[hal_button_count++] = pin; }

void reset_hal_buttons(void)
{
	int i;
	for (i = 0; i < hal_button_count; i++) {
		*hal_buttons[i] = 0;
	}
}

typedef struct {
	double min;
	double max;
} limits_t;

typedef struct interp {
	double x0, x1;
	double t0;
	double linear_speed;
	double T;
} interp_t;

// static const char *axis_prefix[AXIS__COUNT] = {
// 	[AXIS_X] = "lcec.0.TSD0",
// 	[AXIS_Y] = "lcec.0.TSD0",
// 	[AXIS_Z] = "lcec.0.TSD1",
// 	[AXIS_W] = "lcec.0.TSD1",
// };

static const int axis_index[AXIS__COUNT] = { //
	[AXIS_X_OFFSET] = 0,
	[AXIS_Y_OFFSET] = 1,
	[AXIS_Z_OFFSET] = 0,
	[AXIS_W_OFFSET] = 1};

static const limits_t axis_limit[AXIS__COUNT] = {[AXIS_X_OFFSET] = {.min = -20, .max = 20},
												 [AXIS_Y_OFFSET] = {.min = -20, .max = 20},
												 [AXIS_Z_OFFSET] = {.min = -20, .max = 20},
												 [AXIS_W_OFFSET] = {.min = 0, .max = 0}};

typedef enum {
	CMD_NONE,
	CMD_MOVE_AXIS, // single axis move
} COMMAND;

static struct {
	COMMAND type;
	int finished;
	union {
		struct {
			interp_t interp;
			AXES_ENUM axis;
		} move_axis;
	};
} _current_command;

static double max_spindle_acc = 400;

static MAIN_STATE main_state;

static int homing_done = 0;
static double desired_target_position[3] = {0};

static double cur_time = 0; // in seconds
static double dt = 0;

static double desired_spindle_vel = 0;
static int spindle_was_active = 0;
static double spindle_start_time = 0.0;

static int comp_id; /* component ID */
char lcct_module_name[LCCT_STR_MAXLEN];

double lcct_time(void) { return cur_time; }

/************************************************************************
 *                       LCCT PINS CONNECTIONS                          *
 ************************************************************************/

static const pin_def_t pin_def[] = {
	{HAL_FLOAT, HAL_OUT, PIN(spindle_cmd_out), 		"lcct.spindle-cmd-out"},
	{HAL_FLOAT, HAL_IN,  PIN(spindle_cur_in), 		"lcct.spindle-cur-in"},
	{HAL_FLOAT, HAL_OUT,  PIN(spindle_cur_out), 		"lcct.spindle-cur-out"},

	{HAL_FLOAT, HAL_IN,  PIN(joint_pos_cur_in[0]), "lcct.joint-pos-cur-in-0"},
	{HAL_FLOAT, HAL_IN,  PIN(joint_pos_cur_in[1]), "lcct.joint-pos-cur-in-1"},
	{HAL_FLOAT, HAL_IN,  PIN(joint_pos_cur_in[2]), "lcct.joint-pos-cur-in-2"},
	{HAL_FLOAT, HAL_IN,  PIN(joint_pos_cur_in[3]), "lcct.joint-pos-cur-in-3"},

	{HAL_FLOAT, HAL_OUT,  PIN(target_position_out[0]), "lcct.target-position-0"},
	{HAL_FLOAT, HAL_OUT,  PIN(target_position_out[1]), "lcct.target-position-1"},
	{HAL_FLOAT, HAL_OUT,  PIN(target_position_out[2]), "lcct.target-position-2"},
	{HAL_FLOAT, HAL_OUT,  PIN(target_position_out[3]), "lcct.target-position-3"},

	{HAL_BIT,   HAL_IN, PIN(in_mode_csp_in[0]), "lcct.in-mode-csp-0"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_csp_in[1]), "lcct.in-mode-csp-1"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_csp_in[2]), "lcct.in-mode-csp-2"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_csp_in[3]), "lcct.in-mode-csp-3"},

	{HAL_BIT,   HAL_IN, PIN(in_mode_csv_in[0]), "lcct.in-mode-csv-0"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_csv_in[1]), "lcct.in-mode-csv-1"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_csv_in[2]), "lcct.in-mode-csv-2"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_csv_in[3]), "lcct.in-mode-csv-3"},

	{HAL_BIT,   HAL_IN, PIN(in_mode_hm_in[0]), "lcct.in-mode-hm-0"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_hm_in[1]), "lcct.in-mode-hm-1"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_hm_in[2]), "lcct.in-mode-hm-2"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_hm_in[3]), "lcct.in-mode-hm-3"},

	{HAL_BIT,   HAL_IN, PIN(in_mode_inactive_in[0]), "lcct.in-mode-inactive-0"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_inactive_in[1]), "lcct.in-mode-inactive-1"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_inactive_in[2]), "lcct.in-mode-inactive-2"},
	{HAL_BIT,   HAL_IN, PIN(in_mode_inactive_in[3]), "lcct.in-mode-inactive-3"},

	{HAL_BIT,   HAL_IN, PIN(in_fault_in[0]), "lcct.in-fault-0"},
	{HAL_BIT,   HAL_IN, PIN(in_fault_in[1]), "lcct.in-fault-1"},
	{HAL_BIT,   HAL_IN, PIN(in_fault_in[2]), "lcct.in-fault-2"},
	{HAL_BIT,   HAL_IN, PIN(in_fault_in[3]), "lcct.in-fault-3"},

	{HAL_BIT,   HAL_OUT, PIN(set_mode_csp_out[0]), "lcct.set-mode-csp-0"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_csp_out[1]), "lcct.set-mode-csp-1"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_csp_out[2]), "lcct.set-mode-csp-2"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_csp_out[3]), "lcct.set-mode-csp-3"},

	{HAL_BIT,   HAL_OUT, PIN(set_mode_csv_out[0]), "lcct.set-mode-csv-0"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_csv_out[1]), "lcct.set-mode-csv-1"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_csv_out[2]), "lcct.set-mode-csv-2"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_csv_out[3]), "lcct.set-mode-csv-3"},

	{HAL_BIT,   HAL_OUT, PIN(set_mode_hm_out[0]), "lcct.set-mode-hm-0"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_hm_out[1]), "lcct.set-mode-hm-1"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_hm_out[2]), "lcct.set-mode-hm-2"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_hm_out[3]), "lcct.set-mode-hm-3"},

	{HAL_BIT,   HAL_OUT, PIN(set_mode_inactive_out[0]), "lcct.set-mode-inactive-0"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_inactive_out[1]), "lcct.set-mode-inactive-1"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_inactive_out[2]), "lcct.set-mode-inactive-2"},
	{HAL_BIT,   HAL_OUT, PIN(set_mode_inactive_out[3]), "lcct.set-mode-inactive-3"},

	{HAL_BIT,   HAL_IN,  PIN(set_machine_mode_gcode_in), "lcct.set-machine-mode-gcode"},
	{HAL_BIT,   HAL_IN,  PIN(set_machine_mode_stream_in), "lcct.set-machine-mode-stream"},
	{HAL_BIT,   HAL_IN,  PIN(set_machine_mode_homing_in), "lcct.set-machine-mode-homing"},
	{HAL_BIT,   HAL_IN,  PIN(set_machine_mode_jog_in), "lcct.set-machine-mode-jog"},
	{HAL_BIT,   HAL_IN,  PIN(set_machine_mode_inactive_in), "lcct.set-machine-mode-inactive"},

	{HAL_BIT,   HAL_OUT,  PIN(in_machine_mode_gcode_out), "lcct.in-machine-mode-gcode"},
	{HAL_BIT,   HAL_OUT,  PIN(in_machine_mode_stream_out), "lcct.in-machine-mode-stream"},
	{HAL_BIT,   HAL_OUT,  PIN(in_machine_mode_homing_out), "lcct.in-machine-mode-homing"},
	{HAL_BIT,   HAL_OUT,  PIN(in_machine_mode_jog_out), "lcct.in-machine-mode-jog"},
	{HAL_BIT,   HAL_OUT,  PIN(in_machine_mode_inactive_out), "lcct.in-machine-mode-inactive"},

	{HAL_BIT,	HAL_OUT, PIN(homing_finished_out), 	"lcct.homing-finished"},
	{HAL_BIT,	HAL_OUT, PIN(stream_finished_out), 	"lcct.stream-finished"},
	{HAL_BIT,	HAL_OUT, PIN(jog_finished_out), 	"lcct.jog-finished"},
	{HAL_BIT,	HAL_OUT, PIN(gcode_finished_out), 	"lcct.gcode-finished"},

	{HAL_BIT, HAL_IN,  PIN(fault_reset_in), "lcct.fault-reset"},
	{HAL_BIT, HAL_OUT, PIN(fault_reset_out[0]), "lcct.fault-reset-0"},
	{HAL_BIT, HAL_OUT, PIN(fault_reset_out[1]), "lcct.fault-reset-1"},
	{HAL_BIT, HAL_OUT, PIN(fault_reset_out[2]), "lcct.fault-reset-2"},
	{HAL_BIT, HAL_OUT, PIN(fault_reset_out[3]), "lcct.fault-reset-3"},

	{HAL_FLOAT, HAL_IN, PIN(spinbox_offset_in[AXIS_X_OFFSET]), "lcct.spinbox-offset-X"},
	{HAL_FLOAT, HAL_IN, PIN(spinbox_offset_in[AXIS_Y_OFFSET]), "lcct.spinbox-offset-Y"},
	{HAL_FLOAT, HAL_IN, PIN(spinbox_offset_in[AXIS_Z_OFFSET]), "lcct.spinbox-offset-Z"},

	{HAL_FLOAT, HAL_IN, PIN(home_position_in[AXIS_X_OFFSET]), "lcct.home-position-X"},
	{HAL_FLOAT, HAL_IN, PIN(home_position_in[AXIS_Y_OFFSET]), "lcct.home-position-Y"},
	{HAL_FLOAT, HAL_IN, PIN(home_position_in[AXIS_Z_OFFSET]), "lcct.home-position-Z"},

	{HAL_FLOAT, HAL_IN, PIN(spindle_target_velocity), "lcct.spindle-target-velocity"},
	{HAL_BIT, HAL_IN, PIN(spindle_active), "lcct.spindle-active"},

	{HAL_BIT, HAL_OUT, PIN(disable_abs_jog), "lcct.disable-abs-jog"},
	{HAL_BIT, HAL_OUT, PIN(homed_out), "lcct.homed"},

	{HAL_BIT, HAL_OUT, PIN(sampler_enable_out), "lcct.sampler-enable-out"},
	{HAL_S32, HAL_OUT, PIN(external_trigger_out), "lcct.external-trigger"},
	{HAL_S32, HAL_OUT, PIN(electrovalve_out), "lcct.electrovalve"},

	{HAL_TYPE_UNSPECIFIED}
};

static void init_lcct_buttons(void) {
	add_hal_button(data->set_machine_mode_gcode_in);
	add_hal_button(data->set_machine_mode_stream_in);
	add_hal_button(data->set_machine_mode_homing_in);
	add_hal_button(data->set_machine_mode_jog_in);
	add_hal_button(data->set_machine_mode_inactive_in);
	add_hal_button(data->fault_reset_in);
}

/************************************************************************
 *                       EXPORTED FUNCTONS                              *
 ************************************************************************/

static bool interp_get(interp_t *ip, double *output)
{
	double t = (cur_time - ip->t0) / ip->T; // normalized time
    if (t > 1.0) {
        *output = ip->x1;
        return false;
    } else {
        *output = ip->x0 + (1.0 - cos(t * M_PI)) * 0.5 * (ip->x1 - ip->x0);
        return true;
    }
}

static void cmd_exec(void)
{
	switch (_current_command.type) {
	case CMD_NONE:
		_current_command.finished = 1;
		break;
	case CMD_MOVE_AXIS: {
		double target = 0;
		if (!interp_get(&_current_command.move_axis.interp, &target)) {
			_current_command.finished = 1;
			_current_command.type = CMD_NONE;
		}
		set_position(_current_command.move_axis.axis, target);
	} break;
	}
}

static int limit_value_change(double current_value, double target_value, double max_change)
{
	if (fabs(target_value - current_value) < max_change) {
		//*limit_val = target_value;

		return 0; // success

	} else {

		// if (target_value > current_value) {
		//     *limit_val = current_value + max_change;
		// } else {
		//     *limit_val = current_value - max_change;
		// }

		return -1; // limit reached
	}
}

static int set_target_pos_xyz(void)
{
	const double max_speed_xy = 2000;
	const double max_speed_z = 2000;
	int retval = 0;

	retval |= limit_value_change(*data->joint_pos_cur_in[AXIS_X_OFFSET], desired_target_position[AXIS_X_OFFSET],
								 dt * max_speed_xy);
	retval |= limit_value_change(*data->joint_pos_cur_in[AXIS_Y_OFFSET], desired_target_position[AXIS_Y_OFFSET],
								 dt * max_speed_xy);
	retval |= limit_value_change(*data->joint_pos_cur_in[AXIS_Z_OFFSET], desired_target_position[AXIS_Z_OFFSET],
								 dt * max_speed_z);
	retval = 0; // TODO Do some more testing on max speed

	if (retval) {
		rtapi_print_msg(RTAPI_MSG_ERR, "Warning: limit velocity reached!\n");
		return -1;
	}

	switch (main_state) {
	case MAIN_HOMING:

		break;

	case MAIN_GCODE:
	case MAIN_STREAM:
	case MAIN_JOG:
		*data->target_position_out[AXIS_X_OFFSET] = desired_target_position[AXIS_X_OFFSET];
		*data->target_position_out[AXIS_Y_OFFSET] = desired_target_position[AXIS_Y_OFFSET];
		*data->target_position_out[AXIS_Z_OFFSET] = desired_target_position[AXIS_Z_OFFSET];

		break;
	case MAIN_IDLE:
		break;
	case MAIN_ERR:
		set_mode_inactive(AXIS_ALL);
		break;
	}

	return 0;
}

static int cmd_check_done(void)
{
	if (!_current_command.finished) {
		rtapi_print_msg(RTAPI_MSG_ERR, "COMMAND not finished yet!\n");
		_current_command.type = CMD_NONE;
		main_state = MAIN_ERR;
		return 0;
	}
	return 1;
}

static void interp_init(interp_t *ip, double x0, double x1, double speed)
{
	ip->x0 = x0;
	ip->x1 = x1;
	ip->t0 = cur_time;
	ip->linear_speed = speed;
	ip->T = LCCT_MAX((fabs(x1 - x0) / speed), 0.1);
}

int axis_offset(AXES_ENUM axis)
{
	switch (axis) {
	case AXIS_X:
		return AXIS_X_OFFSET;
	case AXIS_Y:
		return AXIS_Y_OFFSET;
	case AXIS_Z:
		return AXIS_Z_OFFSET;
	case AXIS_W:
		return AXIS_W_OFFSET;
	default:
		lprintk("LCCT: Wrong axis ID\n");
		BUG_ON(1);
	}

	return 0;
}

double get_position(AXES_ENUM axis) { return *data->joint_pos_cur_in[axis_offset(axis)]; }
double get_offset(AXES_ENUM axis) { return *data->spinbox_offset_in[axis_offset(axis)]; }
void set_position(AXES_ENUM axis, double value) { desired_target_position[axis_offset(axis)] = value; }
void ext_trigger_enable(int b) { *data->external_trigger_out = b; }

double get_target_spindle_speed(void) { return desired_spindle_vel; }

double get_spindle_speed(void) { return *data->spindle_cur_in; }

void sampler_enable(int b) { *data->sampler_enable_out = b; }

void cmd_move_axis_rel(AXES_ENUM axis, double offset, double speed)
{
	if (!cmd_check_done()) {
		opencn_printf("cmd_move_axis_rel: Command not done, refusing to issue a new one\n");
		return;
	}
	interp_init(&_current_command.move_axis.interp, get_position(axis), get_position(axis) + offset, speed);
	_current_command.type = CMD_MOVE_AXIS;
	_current_command.move_axis.axis = axis;
	_current_command.finished = 0;
}

void cmd_move_axis_abs(AXES_ENUM axis, double target_position, double speed)
{
	if (!cmd_check_done()) {
		opencn_printf("cmd_move_axis_abs: !!! Command not done, refusing to issue a new one !!!\n");
		return;
	} else if (!homing_done) {
		rtapi_print_msg(RTAPI_MSG_ERR, "Please do homing first\n");
		return;
	}
	opencn_printf("cmd_move_axis_abs: axis = %d, target_position = %f, speed = %f\n", axis, target_position, speed);
	interp_init(&_current_command.move_axis.interp, get_position(axis), target_position, speed);
	_current_command.type = CMD_MOVE_AXIS;
	_current_command.move_axis.axis = axis;
	_current_command.finished = 0;
}

void cmd_stop(void) {
    _current_command.finished = 1;
    _current_command.type = CMD_NONE;
}

void set_mode_csp_axis(AXES_ENUM axis)
{
	int k = axis_offset(axis);
	if (*data->in_mode_csp_in[k] == 0) {
		// prepare the target position to be the current position
		// before going into CSP mode
		*data->target_position_out[k] = get_position(axis);
		*data->set_mode_csp_out[k] = 1;
	}
	desired_target_position[k] = get_position(axis);
}

void set_mode_csp(int flags)
{
	if (flags & AXIS_X)
		set_mode_csp_axis(AXIS_X);
	if (flags & AXIS_Y)
		set_mode_csp_axis(AXIS_Y);
	if (flags & AXIS_Z)
		set_mode_csp_axis(AXIS_Z);
	if (flags & AXIS_W)
		set_mode_csp_axis(AXIS_W);
}

void set_mode_csv_axis(AXES_ENUM axis)
{
	int k = axis_offset(axis);
	if (*data->in_mode_csv_in[k] == 0) {
		*data->set_mode_csv_out[k] = 1;
	}
}

void set_mode_csv(int flags)
{
	if (flags & AXIS_X)
		set_mode_csv_axis(AXIS_X);
	if (flags & AXIS_Y)
		set_mode_csv_axis(AXIS_Y);
	if (flags & AXIS_Z)
		set_mode_csv_axis(AXIS_Z);
	if (flags & AXIS_W)
		set_mode_csv_axis(AXIS_W);
}

void set_mode_hm_axis(AXES_ENUM axis)
{
	int k = axis_offset(axis);
	if (*data->in_mode_hm_in[k] == 0) {
		*data->set_mode_hm_out[k] = 1;
	}
}

void set_mode_hm(int flags)
{
	if (flags & AXIS_X)
		set_mode_hm_axis(AXIS_X);
	if (flags & AXIS_Y)
		set_mode_hm_axis(AXIS_Y);
	if (flags & AXIS_Z)
		set_mode_hm_axis(AXIS_Z);
	if (flags & AXIS_W)
		set_mode_hm_axis(AXIS_W);
}

void set_mode_inactive_axis(AXES_ENUM axis)
{
	int k = axis_offset(axis);
	if (*data->in_mode_inactive_in[k] == 0) {
		*data->set_mode_inactive_out[k] = 1;
	}
}

void set_mode_inactive(int flags)
{
	if (flags & AXIS_X)
		set_mode_inactive_axis(AXIS_X);
	if (flags & AXIS_Y)
		set_mode_inactive_axis(AXIS_Y);
	if (flags & AXIS_Z)
		set_mode_inactive_axis(AXIS_Z);
	if (flags & AXIS_W)
		set_mode_inactive_axis(AXIS_W);
}

double get_home_pos_x(void) {
	return *data->home_position_in[AXIS_X_OFFSET];
}

double get_home_pos_y(void) {
	return *data->home_position_in[AXIS_Y_OFFSET];
}

double get_home_pos_z(void) {
    return *data->home_position_in[AXIS_Z_OFFSET];
}


int cmd_done(void) { return _current_command.finished; }


/**
 * @brief       HAL-exported function acting as the realtime-callback
 * \callgraph
 */
static void lcct_update(void *arg, long period)
{
	int axis_i;
	dt = period / 1e9;
	cur_time = cur_time + dt; // update current time in second

	homing_done = lcct_is_homed();
	// TODO: Comment on full machine
	//	homing_done = 1;

	// Start by checking if any axis is in a fault state,
	// if it is, we want to go to being inactive immediatly
	if (*data->in_fault_in[0] || *data->in_fault_in[1] || *data->in_fault_in[2] || *data->in_fault_in[3]) {
		main_state = MAIN_IDLE;
	}

	if (*data->fault_reset_in) {
		opencn_printf("Reseting drive faults\n");
		for (axis_i = 0; axis_i < AXIS__COUNT; axis_i++) {
			*data->fault_reset_out[axis_i] = 1;
		}
	}

	if (*data->set_machine_mode_homing_in) {
		main_state = MAIN_HOMING;
		lcct_home_reset();
		rtapi_print_msg(RTAPI_MSG_INFO, "Mode set to MAIN_HOMING\n");
	} else if (*data->set_machine_mode_gcode_in) {
		if (!homing_done) {
			rtapi_print_msg(RTAPI_MSG_ERR, "Please do homing first\n");
		} else {
			main_state = MAIN_GCODE;
			lcct_gcode_reset();
			rtapi_print_msg(RTAPI_MSG_INFO, "Mode set to MAIN_GCODE\n");
		}
	} else if (*data->set_machine_mode_stream_in) {
		if (!homing_done) {
			rtapi_print_msg(RTAPI_MSG_ERR, "Please do homing first\n");
		} else {
			main_state = MAIN_STREAM;
			lcct_stream_reset();
			rtapi_print_msg(RTAPI_MSG_INFO, "Mode set to MAIN_STREAM\n");
		}

	} else if (*data->set_machine_mode_jog_in) {
		main_state = MAIN_JOG;
		lcct_jog_reset();
		rtapi_print_msg(RTAPI_MSG_INFO, "Mode set to MAIN_JOG\n");
	} else if (*data->set_machine_mode_inactive_in) {
		main_state = MAIN_IDLE;
		set_mode_inactive(AXIS_ALL);
		rtapi_print_msg(RTAPI_MSG_INFO, "Mode set to MAIN_IDLE\n");
	}
	// else if (*data->machine_mode_idle_in && main_state != MAIN_IDLE) {
	//     main_state = MAIN_IDLE;
	// }

	cmd_exec();
	if (set_target_pos_xyz()) {
		//		*data->sampler_enable_out = 0;
		main_state = MAIN_ERR;
	}

	*data->in_machine_mode_gcode_out = main_state == MAIN_GCODE;
	*data->in_machine_mode_stream_out = main_state == MAIN_STREAM;
	*data->in_machine_mode_jog_out = main_state == MAIN_JOG;
	*data->in_machine_mode_homing_out = main_state == MAIN_HOMING;
	*data->in_machine_mode_inactive_out = main_state == MAIN_IDLE;

	*data->disable_abs_jog = !homing_done;

	switch (main_state) {
	case MAIN_HOMING:
		*data->homing_finished_out = lcct_home();
		break;
	case MAIN_STREAM:
		*data->stream_finished_out = lcct_stream();
		break;
	case MAIN_GCODE:
		*data->gcode_finished_out = lcct_gcode();
		break;
	case MAIN_JOG:
		*data->jog_finished_out = lcct_jog();
		break;
	case MAIN_IDLE:
		break;
	case MAIN_ERR:
		break;
	}

	if (*data->spindle_active) {
		if (!spindle_was_active) {
			*data->set_mode_csv_out[3] = 1;
			spindle_was_active = 1;
			spindle_start_time = cur_time;
			*data->spindle_cmd_out = 0;
		}
		desired_spindle_vel = *data->spindle_target_velocity / 60.0f;
	} else {
		if (spindle_was_active) {
			*data->set_mode_inactive_out[3] = 1;
			spindle_was_active = 0;
			*data->spindle_cmd_out = 0;
		}
		desired_spindle_vel = 0;
	}

	if (fabs(desired_spindle_vel - *data->spindle_cmd_out) > max_spindle_acc * dt) {
		if (*data->in_mode_csv_in[3] && (cur_time - spindle_start_time > 2.0)) {
			*data->spindle_cmd_out =
				*data->spindle_cmd_out + SIGN(desired_spindle_vel - *data->spindle_cmd_out) * max_spindle_acc * dt;
		}
	} else {
		*data->spindle_cmd_out = desired_spindle_vel;
	}

	if (fabs(*data->spindle_cur_in) > 1) {
		*data->electrovalve_out = 1;
	} else {
		*data->electrovalve_out = 0;
	}

	*data->spindle_cur_out = *data->spindle_cur_in * 60;
	if (*data->fault_reset_in) {
		set_mode_inactive(AXIS_ALL);
		main_state = MAIN_IDLE;
	}

	*data->homed_out = homing_done;
	*data->sampler_enable_out = 1;
	reset_hal_buttons();
}

/************************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

static int lcct_app_main(int n, lcct_connect_args_t *args)
{
	rtapi_set_msg_level(RTAPI_MSG_WARN);

	/* Store component name */
	strcpy(lcct_module_name, args->name);

	if ((comp_id = hal_init(__core_hal_user, lcct_module_name)) < 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "LCCT: hal_init() failed\n");
		return -EINVAL;
	}

	memset(hal_buttons, 0, sizeof(hal_buttons));

	_current_command.finished = 1;

    hal_export_funct(__core_hal_user, "lcct.update", lcct_update, NULL, 1, 0, comp_id);

	HAL_INIT_PINS(pin_def, comp_id, data);
	init_lcct_buttons();

	HAL_CHECK(lcct_home_init(comp_id));
	HAL_CHECK(lcct_jog_init(comp_id));
	HAL_CHECK(lcct_stream_init(comp_id));
	HAL_CHECK(lcct_gcode_init(comp_id));

	/* --------------------------------- */

	hal_ready(__core_hal_user, comp_id);

	main_state = MAIN_IDLE;
	return 0;
}

static void lcct_app_exit(void) { hal_exit(__core_hal_user, comp_id); }

/************************************************************************
 *                Char Device & file operation definitions              *
 ************************************************************************/

static int lcct_open(struct inode *inode, struct file *file) { return 0; }

static int lcct_release(struct inode *inode, struct file *filp) { return 0; }

static long lcct_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int rc = 0, major, minor;
	hal_user_t *hal_user;

	major = imajor(filp->f_path.dentry->d_inode);
	minor = iminor(filp->f_path.dentry->d_inode);

	switch (cmd) {

	case LCCT_IOCTL_CONNECT:

		rc = lcct_app_main(minor, (lcct_connect_args_t *)arg);
		if (rc) {
			printk("%s: failed to initialize...\n", __func__);
			goto out;
		}
		break;

	case LCCT_IOCTL_DISCONNECT:
		lcct_app_exit();

		hal_user = find_hal_user_by_dev(major, minor);
		BUG_ON(hal_user == NULL);
		hal_exit(hal_user, hal_user->comp_id);
		break;
	}

	return 0;

out:
	hal_exit(__core_hal_user, comp_id);

	return rc;
}

struct file_operations lcct_fops = {
	.owner = THIS_MODULE,
	.open = lcct_open,
	.release = lcct_release,
	.unlocked_ioctl = lcct_ioctl,
};

int lcct_comp_init(void)
{
	int rc;

	printk("OpenCN: lcct subsystem initialization.\n");

	/* Registering device */
	rc = register_chrdev(LCCT_DEV_MAJOR, LCCT_DEV_NAME, &lcct_fops);
	if (rc < 0) {
		printk("Cannot obtain the major number %d\n", LCCT_DEV_MAJOR);
		return rc;
	}

	printk("OpenCN: lcct subsystem initialized\n");

	return 0;
}

late_initcall(lcct_comp_init)
