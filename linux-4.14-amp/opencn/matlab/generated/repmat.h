/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: repmat.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

#ifndef REPMAT_H
#define REPMAT_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void b_repmat(const double a_data[], const int a_size[2], double b_data[],
                     int b_size[2]);
extern void repmat(const double a[3], double varargin_2, double b_data[], int
                   b_size[2]);

#endif

/*
 * File trailer for repmat.h
 *
 * [EOF]
 */
