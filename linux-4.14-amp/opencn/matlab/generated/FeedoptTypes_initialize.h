/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes_initialize.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

#ifndef FEEDOPTTYPES_INITIALIZE_H
#define FEEDOPTTYPES_INITIALIZE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void FeedoptTypes_initialize(void);

#endif

/*
 * File trailer for FeedoptTypes_initialize.h
 *
 * [EOF]
 */
