/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_FeedoptTypes_api.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "_coder_FeedoptTypes_api.h"
#include "_coder_FeedoptTypes_mex.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131483U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "FeedoptTypes",                      /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

/* Function Declarations */
static struct0_T ab_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, CurvStruct *y);
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const
  FeedoptConfigStruct *u);
static uint64_T bb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static CurveType c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static const mxArray *c_emlrt_marshallOut(const real_T u);
static void cb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[3]);
static ZSpdMode d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *d_emlrt_marshallOut(const real_T u[3]);
static real_T db_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[3]);
static const mxArray *e_emlrt_marshallOut(const boolean_T u);
static void eb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[6][3]);
static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *b_CurvStruct,
  const char_T *identifier, CurvStruct *y);
static const mxArray *emlrt_marshallOut(const real_T u_data[], const int32_T
  u_size[2]);
static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static int32_T fb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[6][3]);
static void gb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[3]);
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *FeedoptConfig, const char_T *identifier, FeedoptConfigStruct *y);
static void hb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret[1024]);
static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, FeedoptConfigStruct *y);
static boolean_T ib_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static int32_T j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static void jb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T **ret_data, int32_T ret_size[2]);
static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[3]);
static void kb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[120]);
static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y[1024]);
static uint64_T lb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static boolean_T m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static QueueId n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Q, const
  char_T *identifier);
static QueueId o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static PushStatus p_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *push_status, const char_T *identifier);
static PushStatus q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static real_T r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *x0, const
  char_T *identifier);
static int32_T s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *N, const
  char_T *identifier);
static void t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u_vec, const
  char_T *identifier, real_T **y_data, int32_T y_size[2]);
static void u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T **y_data, int32_T y_size[2]);
static void v_emlrt_marshallIn(const emlrtStack *sp, const mxArray *CurOptStruct,
  const char_T *identifier, OptStruct *y);
static void w_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, OptStruct *y);
static void x_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[120]);
static struct0_T y_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Bl,
  const char_T *identifier);

/* Function Definitions */

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : struct0_T
 */
static struct0_T ab_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  struct0_T y;
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[2] = { "handle", "n" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 2, fieldNames, 0U, &dims);
  thisId.fIdentifier = "handle";
  y.handle = bb_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "handle")), &thisId);
  thisId.fIdentifier = "n";
  y.n = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1, "n")),
    &thisId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                CurvStruct *y
 * Return Type  : void
 */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, CurvStruct *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[9] = { "Type", "ZSpdMode", "P0", "P1", "evec",
    "theta", "pitch", "CoeffP5", "FeedRate" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 9, fieldNames, 0U, &dims);
  thisId.fIdentifier = "Type";
  y->Type = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "Type")), &thisId);
  thisId.fIdentifier = "ZSpdMode";
  y->ZSpdMode = d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    1, "ZSpdMode")), &thisId);
  thisId.fIdentifier = "P0";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2, "P0")),
                     &thisId, y->P0);
  thisId.fIdentifier = "P1";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3, "P1")),
                     &thisId, y->P1);
  thisId.fIdentifier = "evec";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 4, "evec")),
                     &thisId, y->evec);
  thisId.fIdentifier = "theta";
  y->theta = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 5,
    "theta")), &thisId);
  thisId.fIdentifier = "pitch";
  y->pitch = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 6,
    "pitch")), &thisId);
  thisId.fIdentifier = "CoeffP5";
  g_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 7, "CoeffP5")),
                     &thisId, y->CoeffP5);
  thisId.fIdentifier = "FeedRate";
  y->FeedRate = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    8, "FeedRate")), &thisId);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const FeedoptConfigStruct *u
 * Return Type  : const mxArray *
 */
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const
  FeedoptConfigStruct *u)
{
  const mxArray *y;
  static const char * sv[18] = { "NDiscr", "NBreak", "NHorz", "MaxNHorz",
    "MaxNDiscr", "MaxNCoeff", "vmax", "amax", "jmax", "SplineDegree", "CutOff",
    "LSplit", "v_0", "at_0", "v_1", "at_1", "source", "DebugPrint" };

  const mxArray *b_y;
  const mxArray *m;
  int32_T i;
  char_T b_u[1024];
  static const int32_T iv[1] = { 1024 };

  y = NULL;
  emlrtAssign(&y, emlrtCreateStructMatrix(1, 1, 18, sv));
  b_y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m) = u->NDiscr;
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "NDiscr", b_y, 0);
  b_y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m) = u->NBreak;
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "NBreak", b_y, 1);
  b_y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m) = u->NHorz;
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "NHorz", b_y, 2);
  b_y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m) = u->MaxNHorz;
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "MaxNHorz", b_y, 3);
  b_y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m) = u->MaxNDiscr;
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "MaxNDiscr", b_y, 4);
  b_y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m) = u->MaxNCoeff;
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "MaxNCoeff", b_y, 5);
  emlrtSetFieldR2017b(y, 0, "vmax", c_emlrt_marshallOut(u->vmax), 6);
  emlrtSetFieldR2017b(y, 0, "amax", d_emlrt_marshallOut(u->amax), 7);
  emlrtSetFieldR2017b(y, 0, "jmax", d_emlrt_marshallOut(u->jmax), 8);
  b_y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m) = u->SplineDegree;
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "SplineDegree", b_y, 9);
  emlrtSetFieldR2017b(y, 0, "CutOff", c_emlrt_marshallOut(u->CutOff), 10);
  emlrtSetFieldR2017b(y, 0, "LSplit", c_emlrt_marshallOut(u->LSplit), 11);
  emlrtSetFieldR2017b(y, 0, "v_0", c_emlrt_marshallOut(u->v_0), 12);
  emlrtSetFieldR2017b(y, 0, "at_0", c_emlrt_marshallOut(u->at_0), 13);
  emlrtSetFieldR2017b(y, 0, "v_1", c_emlrt_marshallOut(u->v_1), 14);
  emlrtSetFieldR2017b(y, 0, "at_1", c_emlrt_marshallOut(u->at_1), 15);
  for (i = 0; i < 1024; i++) {
    b_u[i] = u->source[i];
  }

  b_y = NULL;
  m = emlrtCreateCharArray(1, iv);
  emlrtInitCharArrayR2013a(sp, 1024, m, &b_u[0]);
  emlrtAssign(&b_y, m);
  emlrtSetFieldR2017b(y, 0, "source", b_y, 16);
  emlrtSetFieldR2017b(y, 0, "DebugPrint", e_emlrt_marshallOut(u->DebugPrint), 17);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : uint64_T
 */
static uint64_T bb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  uint64_T y;
  y = lb_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : CurveType
 */
static CurveType c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  CurveType y;
  static const char * enumNames[4] = { "None", "Line", "Helix", "TransP5" };

  static const int32_T enumValues[4] = { 0, 1, 2, 4 };

  static const int32_T dims = 0;
  emlrtCheckEnumR2012b(sp, "CurveType", 4, enumNames, enumValues);
  emlrtCheckBuiltInR2012b(sp, parentId, u, "CurveType", false, 0U, &dims);
  y = (CurveType)emlrtGetEnumElementR2009a(u, 0);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const real_T u
 * Return Type  : const mxArray *
 */
static const mxArray *c_emlrt_marshallOut(const real_T u)
{
  const mxArray *y;
  const mxArray *m;
  y = NULL;
  m = emlrtCreateDoubleScalar(u);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 *                real_T ret[3]
 * Return Type  : void
 */
static void cb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[3])
{
  static const int32_T dims[1] = { 3 };

  real_T (*r)[3];
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  r = (real_T (*)[3])emlrtMxGetData(src);
  ret[0] = (*r)[0];
  ret[1] = (*r)[1];
  ret[2] = (*r)[2];
  emlrtDestroyArray(&src);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : ZSpdMode
 */
static ZSpdMode d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  ZSpdMode y;
  static const char * enumNames[4] = { "NN", "ZN", "NZ", "ZZ" };

  static const int32_T enumValues[4] = { 0, 1, 2, 3 };

  static const int32_T dims = 0;
  emlrtCheckEnumR2012b(sp, "ZSpdMode", 4, enumNames, enumValues);
  emlrtCheckBuiltInR2012b(sp, parentId, u, "ZSpdMode", false, 0U, &dims);
  y = (ZSpdMode)emlrtGetEnumElementR2009a(u, 0);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const real_T u[3]
 * Return Type  : const mxArray *
 */
static const mxArray *d_emlrt_marshallOut(const real_T u[3])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[2] = { 1, 3 };

  real_T *pData;
  y = NULL;
  m = emlrtCreateNumericArray(2, iv, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m);
  pData[0] = u[0];
  pData[1] = u[1];
  pData[2] = u[2];
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : real_T
 */
static real_T db_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                real_T y[3]
 * Return Type  : void
 */
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[3])
{
  cb_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const boolean_T u
 * Return Type  : const mxArray *
 */
static const mxArray *e_emlrt_marshallOut(const boolean_T u)
{
  const mxArray *y;
  const mxArray *m;
  y = NULL;
  m = emlrtCreateLogicalScalar(u);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 *                real_T ret[6][3]
 * Return Type  : void
 */
static void eb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[6][3])
{
  static const int32_T dims[2] = { 3, 6 };

  real_T (*r)[6][3];
  int32_T i;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims);
  r = (real_T (*)[6][3])emlrtMxGetData(src);
  for (i = 0; i < 6; i++) {
    ret[i][0] = (*r)[i][0];
    ret[i][1] = (*r)[i][1];
    ret[i][2] = (*r)[i][2];
  }

  emlrtDestroyArray(&src);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *b_CurvStruct
 *                const char_T *identifier
 *                CurvStruct *y
 * Return Type  : void
 */
static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *b_CurvStruct,
  const char_T *identifier, CurvStruct *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  b_emlrt_marshallIn(sp, emlrtAlias(b_CurvStruct), &thisId, y);
  emlrtDestroyArray(&b_CurvStruct);
}

/*
 * Arguments    : const real_T u_data[]
 *                const int32_T u_size[2]
 * Return Type  : const mxArray *
 */
static const mxArray *emlrt_marshallOut(const real_T u_data[], const int32_T
  u_size[2])
{
  const mxArray *y;
  const mxArray *m;
  static const int32_T iv[2] = { 0, 0 };

  y = NULL;
  m = emlrtCreateNumericArray(2, iv, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, (void *)&u_data[0]);
  emlrtSetDimensions((mxArray *)m, *(int32_T (*)[2])&u_size[0], 2);
  emlrtAssign(&y, m);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : real_T
 */
static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = db_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : int32_T
 */
static int32_T fb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  int32_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "int32", false, 0U, &dims);
  ret = *(int32_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                real_T y[6][3]
 * Return Type  : void
 */
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[6][3])
{
  eb_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 *                real_T ret[3]
 * Return Type  : void
 */
static void gb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[3])
{
  static const int32_T dims[2] = { 1, 3 };

  real_T (*r)[3];
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims);
  r = (real_T (*)[3])emlrtMxGetData(src);
  ret[0] = (*r)[0];
  ret[1] = (*r)[1];
  ret[2] = (*r)[2];
  emlrtDestroyArray(&src);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *FeedoptConfig
 *                const char_T *identifier
 *                FeedoptConfigStruct *y
 * Return Type  : void
 */
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *FeedoptConfig, const char_T *identifier, FeedoptConfigStruct *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  i_emlrt_marshallIn(sp, emlrtAlias(FeedoptConfig), &thisId, y);
  emlrtDestroyArray(&FeedoptConfig);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 *                char_T ret[1024]
 * Return Type  : void
 */
static void hb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret[1024])
{
  static const int32_T dims[1] = { 1024 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "char", false, 1U, dims);
  emlrtImportCharArrayR2015b(sp, src, &ret[0], 1024);
  emlrtDestroyArray(&src);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                FeedoptConfigStruct *y
 * Return Type  : void
 */
static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, FeedoptConfigStruct *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[18] = { "NDiscr", "NBreak", "NHorz", "MaxNHorz",
    "MaxNDiscr", "MaxNCoeff", "vmax", "amax", "jmax", "SplineDegree", "CutOff",
    "LSplit", "v_0", "at_0", "v_1", "at_1", "source", "DebugPrint" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 18, fieldNames, 0U, &dims);
  thisId.fIdentifier = "NDiscr";
  y->NDiscr = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "NDiscr")), &thisId);
  thisId.fIdentifier = "NBreak";
  y->NBreak = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1,
    "NBreak")), &thisId);
  thisId.fIdentifier = "NHorz";
  y->NHorz = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2,
    "NHorz")), &thisId);
  thisId.fIdentifier = "MaxNHorz";
  y->MaxNHorz = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    3, "MaxNHorz")), &thisId);
  thisId.fIdentifier = "MaxNDiscr";
  y->MaxNDiscr = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    4, "MaxNDiscr")), &thisId);
  thisId.fIdentifier = "MaxNCoeff";
  y->MaxNCoeff = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    5, "MaxNCoeff")), &thisId);
  thisId.fIdentifier = "vmax";
  y->vmax = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 6,
    "vmax")), &thisId);
  thisId.fIdentifier = "amax";
  k_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 7, "amax")),
                     &thisId, y->amax);
  thisId.fIdentifier = "jmax";
  k_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 8, "jmax")),
                     &thisId, y->jmax);
  thisId.fIdentifier = "SplineDegree";
  y->SplineDegree = j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u,
    0, 9, "SplineDegree")), &thisId);
  thisId.fIdentifier = "CutOff";
  y->CutOff = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 10,
    "CutOff")), &thisId);
  thisId.fIdentifier = "LSplit";
  y->LSplit = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 11,
    "LSplit")), &thisId);
  thisId.fIdentifier = "v_0";
  y->v_0 = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 12,
    "v_0")), &thisId);
  thisId.fIdentifier = "at_0";
  y->at_0 = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 13,
    "at_0")), &thisId);
  thisId.fIdentifier = "v_1";
  y->v_1 = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 14,
    "v_1")), &thisId);
  thisId.fIdentifier = "at_1";
  y->at_1 = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 15,
    "at_1")), &thisId);
  thisId.fIdentifier = "source";
  l_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 16, "source")),
                     &thisId, y->source);
  thisId.fIdentifier = "DebugPrint";
  y->DebugPrint = m_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    17, "DebugPrint")), &thisId);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : boolean_T
 */
static boolean_T ib_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  boolean_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "logical", false, 0U, &dims);
  ret = *emlrtMxGetLogicals(src);
  emlrtDestroyArray(&src);
  return ret;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : int32_T
 */
static int32_T j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  int32_T y;
  y = fb_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 *                real_T **ret_data
 *                int32_T ret_size[2]
 * Return Type  : void
 */
static void jb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T **ret_data, int32_T ret_size[2])
{
  static const int32_T dims[2] = { 1, 200 };

  const boolean_T bv[2] = { false, true };

  int32_T iv[2];
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv[0],
    iv);
  ret_size[0] = iv[0];
  ret_size[1] = iv[1];
  *ret_data = (real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                real_T y[3]
 * Return Type  : void
 */
static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[3])
{
  gb_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 *                real_T ret[120]
 * Return Type  : void
 */
static void kb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[120])
{
  static const int32_T dims[1] = { 120 };

  real_T (*r)[120];
  int32_T i;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  r = (real_T (*)[120])emlrtMxGetData(src);
  for (i = 0; i < 120; i++) {
    ret[i] = (*r)[i];
  }

  emlrtDestroyArray(&src);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                char_T y[1024]
 * Return Type  : void
 */
static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y[1024])
{
  hb_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *src
 *                const emlrtMsgIdentifier *msgId
 * Return Type  : uint64_T
 */
static uint64_T lb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  uint64_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "uint64", false, 0U, &dims);
  ret = *(uint64_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : boolean_T
 */
static boolean_T m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  boolean_T y;
  y = ib_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *Q
 *                const char_T *identifier
 * Return Type  : QueueId
 */
static QueueId n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Q, const
  char_T *identifier)
{
  QueueId y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = o_emlrt_marshallIn(sp, emlrtAlias(Q), &thisId);
  emlrtDestroyArray(&Q);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : QueueId
 */
static QueueId o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  QueueId y;
  static const char * enumNames[5] = { "GCode", "Smooth", "Split", "Opt",
    "COUNT" };

  static const int32_T enumValues[5] = { 0, 1, 2, 3, 4 };

  static const int32_T dims = 0;
  emlrtCheckEnumR2012b(sp, "QueueId", 5, enumNames, enumValues);
  emlrtCheckBuiltInR2012b(sp, parentId, u, "QueueId", false, 0U, &dims);
  y = (QueueId)emlrtGetEnumElementR2009a(u, 0);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *push_status
 *                const char_T *identifier
 * Return Type  : PushStatus
 */
static PushStatus p_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *push_status, const char_T *identifier)
{
  PushStatus y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = q_emlrt_marshallIn(sp, emlrtAlias(push_status), &thisId);
  emlrtDestroyArray(&push_status);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 * Return Type  : PushStatus
 */
static PushStatus q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  PushStatus y;
  static const char * enumNames[3] = { "Success", "TryAgain", "Finished" };

  static const int32_T enumValues[3] = { 0, 1, 2 };

  static const int32_T dims = 0;
  emlrtCheckEnumR2012b(sp, "PushStatus", 3, enumNames, enumValues);
  emlrtCheckBuiltInR2012b(sp, parentId, u, "PushStatus", false, 0U, &dims);
  y = (PushStatus)emlrtGetEnumElementR2009a(u, 0);
  emlrtDestroyArray(&u);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *x0
 *                const char_T *identifier
 * Return Type  : real_T
 */
static real_T r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *x0, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = f_emlrt_marshallIn(sp, emlrtAlias(x0), &thisId);
  emlrtDestroyArray(&x0);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *N
 *                const char_T *identifier
 * Return Type  : int32_T
 */
static int32_T s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *N, const
  char_T *identifier)
{
  int32_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = j_emlrt_marshallIn(sp, emlrtAlias(N), &thisId);
  emlrtDestroyArray(&N);
  return y;
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u_vec
 *                const char_T *identifier
 *                real_T **y_data
 *                int32_T y_size[2]
 * Return Type  : void
 */
static void t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u_vec, const
  char_T *identifier, real_T **y_data, int32_T y_size[2])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  u_emlrt_marshallIn(sp, emlrtAlias(u_vec), &thisId, y_data, y_size);
  emlrtDestroyArray(&u_vec);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                real_T **y_data
 *                int32_T y_size[2]
 * Return Type  : void
 */
static void u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T **y_data, int32_T y_size[2])
{
  real_T *r;
  jb_emlrt_marshallIn(sp, emlrtAlias(u), parentId, &r, y_size);
  *y_data = r;
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *CurOptStruct
 *                const char_T *identifier
 *                OptStruct *y
 * Return Type  : void
 */
static void v_emlrt_marshallIn(const emlrtStack *sp, const mxArray *CurOptStruct,
  const char_T *identifier, OptStruct *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  w_emlrt_marshallIn(sp, emlrtAlias(CurOptStruct), &thisId, y);
  emlrtDestroyArray(&CurOptStruct);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                OptStruct *y
 * Return Type  : void
 */
static void w_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, OptStruct *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[2] = { "Coeff", "CurvStruct" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 2, fieldNames, 0U, &dims);
  thisId.fIdentifier = "Coeff";
  x_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0, "Coeff")),
                     &thisId, y->Coeff);
  thisId.fIdentifier = "CurvStruct";
  b_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1,
    "CurvStruct")), &thisId, &y->CurvStruct);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *u
 *                const emlrtMsgIdentifier *parentId
 *                real_T y[120]
 * Return Type  : void
 */
static void x_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[120])
{
  kb_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

/*
 * Arguments    : const emlrtStack *sp
 *                const mxArray *Bl
 *                const char_T *identifier
 * Return Type  : struct0_T
 */
static struct0_T y_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Bl,
  const char_T *identifier)
{
  struct0_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = ab_emlrt_marshallIn(sp, emlrtAlias(Bl), &thisId);
  emlrtDestroyArray(&Bl);
  return y;
}

/*
 * Arguments    : const mxArray * const prhs[2]
 *                int32_T nlhs
 *                const mxArray *plhs[4]
 * Return Type  : void
 */
void EvalCurvStruct_api(const mxArray * const prhs[2], int32_T nlhs, const
  mxArray *plhs[4])
{
  real_T (*r0D_data)[600];
  real_T (*r1D_data)[600];
  real_T (*r2D_data)[600];
  real_T (*r3D_data)[600];
  const mxArray *prhs_copy_idx_0;
  const mxArray *prhs_copy_idx_1;
  CurvStruct b_CurvStruct;
  real_T (*u_vec_data)[200];
  int32_T u_vec_size[2];
  int32_T r0D_size[2];
  int32_T r1D_size[2];
  int32_T r2D_size[2];
  int32_T r3D_size[2];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  r0D_data = (real_T (*)[600])mxMalloc(sizeof(real_T [600]));
  r1D_data = (real_T (*)[600])mxMalloc(sizeof(real_T [600]));
  r2D_data = (real_T (*)[600])mxMalloc(sizeof(real_T [600]));
  r3D_data = (real_T (*)[600])mxMalloc(sizeof(real_T [600]));
  prhs_copy_idx_0 = prhs[0];
  prhs_copy_idx_1 = emlrtProtectR2012b(prhs[1], 1, false, 200);

  /* Marshall function inputs */
  emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_0), "CurvStruct",
                   &b_CurvStruct);
  t_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_1), "u_vec", (real_T **)
                     &u_vec_data, u_vec_size);

  /* Invoke the target function */
  EvalCurvStruct(&b_CurvStruct, *u_vec_data, u_vec_size, *r0D_data, r0D_size,
                 *r1D_data, r1D_size, *r2D_data, r2D_size, *r3D_data, r3D_size);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*r0D_data, r0D_size);
  if (nlhs > 1) {
    plhs[1] = emlrt_marshallOut(*r1D_data, r1D_size);
  }

  if (nlhs > 2) {
    plhs[2] = emlrt_marshallOut(*r2D_data, r2D_size);
  }

  if (nlhs > 3) {
    plhs[3] = emlrt_marshallOut(*r3D_data, r3D_size);
  }
}

/*
 * Arguments    : int32_T nlhs
 *                const mxArray *plhs[1]
 * Return Type  : void
 */
void FeedoptDefaultConfig_api(int32_T nlhs, const mxArray *plhs[1])
{
  FeedoptConfigStruct cfg;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;

  /* Invoke the target function */
  FeedoptDefaultConfig(&cfg);

  /* Marshall function outputs */
  plhs[0] = b_emlrt_marshallOut(&st, &cfg);
}

/*
 * Arguments    : const mxArray * const prhs[4]
 *                int32_T nlhs
 * Return Type  : void
 */
void FeedoptTypes_api(const mxArray * const prhs[4], int32_T nlhs)
{
  CurvStruct b_CurvStruct;
  FeedoptConfigStruct FeedoptConfig;
  QueueId Q;
  PushStatus push_status;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "CurvStruct", &b_CurvStruct);
  h_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "FeedoptConfig", &FeedoptConfig);
  Q = n_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "Q");
  push_status = p_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "push_status");

  /* Invoke the target function */
  FeedoptTypes(&b_CurvStruct, &FeedoptConfig, Q, push_status);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void FeedoptTypes_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  FeedoptTypes_xil_terminate();
  FeedoptTypes_xil_shutdown();
  emlrtExitTimeCleanup(&emlrtContextGlobal);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void FeedoptTypes_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void FeedoptTypes_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/*
 * Arguments    : int32_T nlhs
 * Return Type  : void
 */
void InitConfig_api(int32_T nlhs)
{
  (void)nlhs;

  /* Invoke the target function */
  InitConfig();
}

/*
 * Arguments    : const mxArray * const prhs[1]
 *                int32_T nlhs
 * Return Type  : void
 */
void PrintCurvStruct_api(const mxArray * const prhs[1], int32_T nlhs)
{
  CurvStruct S;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "S", &S);

  /* Invoke the target function */
  PrintCurvStruct(&S);
}

/*
 * Arguments    : const mxArray * const prhs[5]
 *                int32_T nlhs
 *                const mxArray *plhs[3]
 * Return Type  : void
 */
void ResampleTick_api(const mxArray * const prhs[5], int32_T nlhs, const mxArray
                      *plhs[3])
{
  OptStruct CurOptStruct;
  OptStruct NextOptStruct;
  struct0_T Bl;
  real_T u;
  real_T dt;
  boolean_T pop_next;
  boolean_T finished;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  v_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "CurOptStruct", &CurOptStruct);
  v_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "NextOptStruct", &NextOptStruct);
  Bl = y_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "Bl");
  u = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "u");
  dt = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[4]), "dt");

  /* Invoke the target function */
  ResampleTick(CurOptStruct, NextOptStruct, Bl, &u, dt, &pop_next, &finished);

  /* Marshall function outputs */
  plhs[0] = c_emlrt_marshallOut(u);
  if (nlhs > 1) {
    plhs[1] = e_emlrt_marshallOut(pop_next);
  }

  if (nlhs > 2) {
    plhs[2] = e_emlrt_marshallOut(finished);
  }
}

/*
 * Arguments    : const mxArray * const prhs[3]
 *                int32_T nlhs
 *                const mxArray *plhs[1]
 * Return Type  : void
 */
void c_linspace_api(const mxArray * const prhs[3], int32_T nlhs, const mxArray
                    *plhs[1])
{
  real_T (*A_data)[200];
  real_T x0;
  real_T x1;
  int32_T N;
  int32_T A_size[2];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;
  A_data = (real_T (*)[200])mxMalloc(sizeof(real_T [200]));

  /* Marshall function inputs */
  x0 = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "x0");
  x1 = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "x1");
  N = s_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "N");

  /* Invoke the target function */
  c_linspace(x0, x1, N, *A_data, A_size);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*A_data, A_size);
}

/*
 * Arguments    : const mxArray * const prhs[3]
 *                int32_T nlhs
 *                const mxArray *plhs[1]
 * Return Type  : void
 */
void sinspace_api(const mxArray * const prhs[3], int32_T nlhs, const mxArray
                  *plhs[1])
{
  real_T (*x_data)[200];
  real_T x0;
  real_T x1;
  int32_T N;
  int32_T x_size[2];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;
  x_data = (real_T (*)[200])mxMalloc(sizeof(real_T [200]));

  /* Marshall function inputs */
  x0 = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "x0");
  x1 = r_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "x1");
  N = s_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "N");

  /* Invoke the target function */
  sinspace(x0, x1, N, *x_data, x_size);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*x_data, x_size);
}

/*
 * File trailer for _coder_FeedoptTypes_api.c
 *
 * [EOF]
 */
