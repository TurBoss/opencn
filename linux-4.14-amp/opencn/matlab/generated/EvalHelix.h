/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalHelix.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

#ifndef EVALHELIX_H
#define EVALHELIX_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void EvalHelix(const double CurvStruct_P0[3], const double CurvStruct_P1
                      [3], const double CurvStruct_evec[3], double
                      CurvStruct_theta, double CurvStruct_pitch, const double
                      u_vec_data[], const int u_vec_size[2], double r0D_data[],
                      int r0D_size[2], double r1D_data[], int r1D_size[2],
                      double r2D_data[], int r2D_size[2], double r3D_data[], int
                      r3D_size[2]);

#endif

/*
 * File trailer for EvalHelix.h
 *
 * [EOF]
 */
