/* -*- linux-c -*-
 * linux/kernel/ipipe/core.c
 *
 * Copyright (C) 2002-2012 Philippe Gerum.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, Inc., 675 Mass Ave, Cambridge MA 02139,
 * USA; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Architecture-independent I-PIPE core support.
 */

#if 0
#define DEBUG
#endif

#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/kallsyms.h>
#include <linux/bitops.h>
#include <linux/tick.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>
#ifdef CONFIG_PROC_FS
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#endif	/* CONFIG_PROC_FS */
#include <linux/ipipe_trace.h>
#include <linux/ipipe.h>

#include <ipipe/setup.h>

#include <asm/cacheflush.h>

#include <asm/arch_timer.h>

#include <opencn/uapi/debug.h>

#include <opencn/opencn.h>
#include <opencn/console.h>
#include <opencn/event/evtchn.h>

ipipe_irqdesc_t irqdescs[NR_PIRQS];

struct ipipe_domain ipipe_root;
EXPORT_SYMBOL_GPL(ipipe_root);

struct ipipe_domain *ipipe_head_domain = &ipipe_root;
EXPORT_SYMBOL_GPL(ipipe_head_domain);

DEFINE_PER_CPU(struct ipipe_percpu_data, ipipe_percpu) = {
	.root = {
		.status = IPIPE_STALL_MASK,
		.domain = &ipipe_root,
	},

	.hrtimer_irq = -1,
#ifdef CONFIG_IPIPE_DEBUG_CONTEXT
	.context_check = 1,
#endif
};
EXPORT_PER_CPU_SYMBOL(ipipe_percpu);

struct task_struct * volatile task_hijacked = NULL;

DEFINE_SPINLOCK(__ipipe_lock);

#ifdef CONFIG_PRINTK
unsigned int __ipipe_printk_virq;
int __ipipe_printk_bypass;
#endif /* CONFIG_PRINTK */

#ifdef CONFIG_PROC_FS

struct proc_dir_entry *ipipe_proc_root;

static int __ipipe_version_info_show(struct seq_file *p, void *data)
{
	seq_printf(p, "%d\n", IPIPE_CORE_RELEASE);
	return 0;
}

static int __ipipe_version_info_open(struct inode *inode, struct file *file)
{
	return single_open(file, __ipipe_version_info_show, NULL);
}

static const struct file_operations __ipipe_version_proc_ops = {
	.open		= __ipipe_version_info_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __ipipe_common_info_show(struct seq_file *p, void *data)
{
	struct ipipe_domain *ipd = (struct ipipe_domain *)p->private;
	char handling, lockbit;
	unsigned long ctlbits;
	unsigned int irq;

	seq_printf(p, "        +--- Handled\n");
	seq_printf(p, "        |+-- Locked\n");
	seq_printf(p, "        ||+- Virtual\n");
	seq_printf(p, " [IRQ]  |||  Handler\n");

	mutex_lock(&ipd->mutex);

	for (irq = 0; irq < IPIPE_NR_IRQS; irq++) {
		ctlbits = ipd->irqs[irq].control;

		if (ctlbits & IPIPE_HANDLE_MASK)
			handling = 'H';
		else
			handling = '.';

		if (ctlbits & IPIPE_LOCK_MASK)
			lockbit = 'L';
		else
			lockbit = '.';

		if (ctlbits & IPIPE_HANDLE_MASK)
			seq_printf(p, " %4u:  %c%c  %pf\n",
				   irq, handling, lockbit,
				   ipd->irqs[irq].handler);
		else
			seq_printf(p, " %4u:  %c%c\n",
				   irq, handling, lockbit);
	}

	mutex_unlock(&ipd->mutex);

	return 0;
}

static int __ipipe_common_info_open(struct inode *inode, struct file *file)
{
	return single_open(file, __ipipe_common_info_show, PDE_DATA(inode));
}

static const struct file_operations __ipipe_info_proc_ops = {
	.owner		= THIS_MODULE,
	.open		= __ipipe_common_info_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

void add_domain_proc(struct ipipe_domain *ipd)
{
	proc_create_data(ipd->name, 0444, ipipe_proc_root,
			 &__ipipe_info_proc_ops, ipd);
}

void remove_domain_proc(struct ipipe_domain *ipd)
{
	remove_proc_entry(ipd->name, ipipe_proc_root);
}

void __init __ipipe_init_proc(void)
{
	ipipe_proc_root = proc_mkdir("ipipe", NULL);
	proc_create("version", 0444, ipipe_proc_root,
		    &__ipipe_version_proc_ops);
	add_domain_proc(ipipe_root_domain);

	__ipipe_init_tracer();
}

#else

#endif	/* CONFIG_PROC_FS */

static void init_stage(struct ipipe_domain *ipd)
{
	memset(&ipd->irqs, 0, sizeof(ipd->irqs));
	mutex_init(&ipd->mutex);
}

#ifdef CONFIG_SMP


#else /* !CONFIG_SMP */

static inline void fixup_percpu_data(void) { }

#endif /* CONFIG_SMP */

void __init __ipipe_init_early(void)
{
	int i;

	/* Initialize all IRQ descriptors */
	for (i = 0; i < NR_PIRQS; i++)
		memset(&irqdescs[i], 0, sizeof(ipipe_irqdesc_t));

	/* IPIs are managed by a unique handler in the non-RT domain (arch/arm/kernel/smp.c) */

	for (i = 16; i < NR_PIRQS; i++) {
		irqdescs[i].irq = i;
		ipipe_assign_chip(&irqdescs[i]);
	}
#ifdef CONFIG_ARM
	irqdescs[IRQ_ARM_TIMER].irq = IRQ_ARM_TIMER;
	ipipe_assign_chip(&irqdescs[IRQ_ARM_TIMER]);
#endif

}

static inline void init_head_stage(struct ipipe_domain *ipd)
{
	struct ipipe_percpu_domain_data *p;
	int cpu;

	/* Must be set first, used in ipipe_percpu_context(). */
	ipd->context_offset = offsetof(struct ipipe_percpu_data, head);

	for_each_online_cpu(cpu) {
		p = ipipe_percpu_context(ipd, cpu);
		memset(p, 0, sizeof(*p));
		p->domain = ipd;
	}

	init_stage(ipd);
}


void ipipe_request_irq(unsigned int irq, ipipe_irq_handler_t handler, void *cookie)
{
	BUG_ON(irqdescs[irq].handler != NULL);

	irqdescs[irq].handler = handler;
	irqdescs[irq].data = cookie;
}


extern void kstat_incr_irq_this_cpu(unsigned int irq);
extern void rtdm_clear_irq(void);

/*
 * Main IRQ/IPI dispatch function - IRQs are disabled.
 *
 */
void __ipipe_dispatch_irq(unsigned int irq, bool reset) {
	struct irq_chip *chip;

	DBG("%s %d CPU %d irq: %d\n", __func__, __LINE__, smp_processor_id(), irq);

	BUG_ON(smp_processor_id() == 0);

	chip = irqdescs[irq].irq_data.chip;

	/* Perform the acknowledge (and mask) */
	if (chip)
		chip->irq_mask(&irqdescs[irq].irq_data);


	/* At the very beginning, a first IRQ might be sent to CPU #1 to awake it up.
	 * However, Xenomai/cobalt is not initialized yet.
	 */
	if (unlikely(!__cobalt_ready)) {

#ifdef CONFIG_ARM
		if (irq == IRQ_ARM_TIMER)
			__ipipe_timer_handler(IRQ_ARM_TIMER, NULL);
#endif

		if (chip) {
			chip->irq_unmask(&irqdescs[irq].irq_data);
			chip->irq_eoi(&irqdescs[irq].irq_data);
		}
		return ;
	}

	/* We increment the number of timer IRQ for monitoring purposes... */
	if (irq_to_desc(irq) != NULL)
		kstat_incr_irq_this_cpu(irq);

	barrier();

	__xnintr_irq_handler(irq);

	BUG_ON(!hard_irqs_disabled());

	if (chip)
		chip->irq_unmask(&irqdescs[irq].irq_data);

	/* An IPI does not have an eoi routine. */
	if (chip && (chip->irq_eoi)) /* There is no eoi function for VIRQ */
		chip->irq_eoi(&irqdescs[irq].irq_data);

	/* Now we are safe to process the tick handler leading to potential context switching. */
#ifdef CONFIG_ARM
	if (irq == IRQ_ARM_TIMER)
		xnintr_core_clock_handler();
#endif

#ifdef CONFIG_ARCH_BCM2835
	if (reset)
		rtdm_clear_irq();
#endif
	/* Invoke the scheduler since the state could have been changed during the interrupt processing */
	/* Remark: if this function from evtchn_do_upcall() so that in_upcall_progress is true, the
	 * scheduler will not be activated here, but in irq_exit after the end of evtchn_do_upcall along
	 * the IPI handling termination (see smp.c).
	 */
	xnsched_run();

	return ;

}

#define __ipipe_preempt_schedule_irq()	do { } while (0)

