
/* 
 * Just a simple example to access aflib functions
 * Execute trycomp script to compile it.
 * The binary will be deployed in the root dir of the rootfs.
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <af.h>

extern int get_pins();

int main(int argc, char *argv[]) {
    hal_pin_t *thePin;
    void *pin_data_ptr;

    float f;
    
    printf("## initializing AF lib.\n");
    af_init();

    pin_new("onepin", HAL_S32, HAL_IO);
    pin_new("otherpin", HAL_BIT, HAL_OUT);
    
    //printf("Retrieving now streamer.0.pin.0\n");
    printf("Retrieving onepin\n");
    thePin = pin_find_by_name("onepin");

    printf("## resulting : %s\n", thePin->name);

      printf("## value of this pin: %d\n", pin_get_value(thePin)->s);
      
    pin_get_value(thePin)->s = 26;
    
    printf("## value of this pin: %d\n", pin_get_value(thePin)->s);
    
    pin_delete("onepin");

}
