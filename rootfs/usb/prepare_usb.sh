#!/bin/sh

abort()
{
  echo "Error: $1"
  exit -1
}

usage()
{
  echo "Usage: $0 [OPTIONS]"
  echo ""
  echo "Where OPTIONS are:"
  echo "  -d <DEVICE>    Set the disk device (/dev/sdc for instance)"
  echo "  -f             Format and label partitions"
  echo "  -g             Install GRUB"
  echo "  -h             Display this help"
  echo "  -k <KERNEL>    Set the kernel image file path"
  echo "  -r             Copy rootfs"
  exit 1
}

while getopts "d:fghk:r" o; do
  case "$o" in
    d)
      dev_disk=$OPTARG
      ;;
    f)
      format_partitions=y
      ;;
    g)
      install_grub=y
      ;;
    h)
      usage
      ;;
    k)
      kernel_image_path=$OPTARG
      ;;
    r)
      copy_rootfs=y
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

# Check arguments
if [ -z "$dev_disk" ]; then
  echo "Error: Please specify partition block device (-d)"
  usage
fi

# if [ -z "$kernel_image_path" ]; then
#   echo "Error: Please specify kernel image file path (-k)"
#   usage
# fi

# ARGS
dev_partition=${dev_disk}1
script_dir="$(cd "$(dirname "$0")" && pwd)"
usb_partition_root_dir=$script_dir/usb-partition
opencn_dir=$usb_partition_root_dir/boot
usb_partition_boot_dir=$usb_partition_root_dir/boot
rootfs_cpio_file=$script_dir/../images/rootfs.cpio

if [ -n "$format_partitions" ]; then
  echo "Erasing partitions on $dev_disk"
  sudo dd if=/dev/zero of=$dev_disk count=10k conv=notrunc || abort "Failed to erase $dev_disk"

  echo "Creating rootfs partition: $dev_partition"
  sudo parted -s $dev_disk mklabel msdos mkpart primary ext4 1MiB 100%
  sleep 2
  sudo mke2fs -F -t ext4 $dev_partition
  sudo e2label $dev_partition rootfs
fi

echo "Creating $usb_partition_root_dir"
mkdir -p $usb_partition_root_dir || abort "Failed to create dir $usb_partition_root_dir"

echo "Mounting boot partition: $dev_partition"
sudo mount $dev_partition $usb_partition_root_dir || abort "Failed to mount $dev_partition"

echo "Creating $opencn_dir"
sudo mkdir -p $opencn_dir || abort "failed to create $opencn_dir"

if [ -n "$kernel_image_path" ]; then
    echo "Copying $kernel_image_path"
    sudo cp $kernel_image_path $opencn_dir/. || abort "Failed to copy kernel image $kernel_image_path"
fi


if [ -n "$install_grub" ]; then
  echo "Installing grub on $dev_disk"
  sudo grub-install --target=i386-pc --boot-directory=$usb_partition_boot_dir $dev_disk --directory=$script_dir/boot/grub/i386-pc/ || abort "Failed to install grub in $dev_disk"
  sudo cp $script_dir/boot/grub/grub.cfg $usb_partition_root_dir/boot/grub || abort "Failed to copy $script_dir/boot/grub/grub.cfg"
fi

echo "Unmounting $usb_partition_root_dir"
sync
sudo umount $usb_partition_root_dir || abort "Failed to umount $usb_partition_root_dir"
rmdir $usb_partition_root_dir

if [ -n "$copy_rootfs" ]; then
  echo "Creating $usb_partition_root_dir"
  mkdir -p $usb_partition_root_dir || abort "Failed to create dir $usb_partition_root_dir"

  echo "Mounting rootfs partition: $dev_partition"
  sudo mount $dev_partition $usb_partition_root_dir || abort "Failed to mount $dev_partition"

  echo "Extraction rootfs: $rootfs_cpio_file"
  sudo cpio -i -D $usb_partition_root_dir < $rootfs_cpio_file || abort "Failed to extract $rootfs_cpio_file"

  echo "Unmounting $usb_rootfs_partition_root_dir"
  sync
  sudo umount $usb_partition_root_dir || abort "Failed to umount $usb_partition_root_dir"
  rmdir $usb_partition_root_dir

fi

echo "Success!"
