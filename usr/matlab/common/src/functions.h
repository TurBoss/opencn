#ifndef C_FUNCTIONS_H
#define C_FUNCTIONS_H

#include "c_spline.h"

#include <matlab_headers.h>

#ifdef __cplusplus
extern "C" {
#endif

void c_opt_geom(double *X, double *Y);
void c_roots(double *coeffs, creal_T *roots, int n);

/* void linspace(double *out, double x0, double x1, int n); */
void c_assert_(const char *message);

#ifdef __cplusplus
}
#endif

#endif
