#include "functions.h"

#ifdef __KERNEL__
#include <linux/slab.h>
#else
#include <stdio.h>
#include <gsl/gsl_poly.h>
#endif

void c_opt_geom(double *X, double *Y) {}
void c_roots(double *coeffs, creal_T *roots, int n) {
    double *roots_as_double = (double *)roots;
    gsl_poly_complex_workspace *ws = gsl_poly_complex_workspace_alloc(n);
    gsl_poly_complex_solve(coeffs, n, ws, roots_as_double);
    gsl_poly_complex_workspace_free(ws);
}

/*void linspace(double *out, double x0, double x1, int n) {
    int i;
    for (i = 0; i < n; i++) {
        out[i] = x0 + (x1 - x0) / (n - 1) * i;
    }
}
*/
void c_assert_(const char *msg) {
#ifdef __KERNEL__
    printk(KERN_ERR "(c_assert) %s\n", msg);
#else
    fprintf(stderr, "(c_assert) %s\n", msg);
#endif
}
