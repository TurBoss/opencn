function Test_findpeaks

    t = linspace(0,1,100);
    Fs = 1/(t(2) - t(1));
    y = sin(t*2*pi*2);
    [values, locs] = findpeaks(-y, Fs, 'NPeaks', 1);
%     locs = 1 - locs;
    figure(1)
    plot(t,y, [locs, locs], [-1, 1])
    
    
end