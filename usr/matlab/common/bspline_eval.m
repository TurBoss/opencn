function [x, xd, xdd] = bspline_eval(Bl, coeffs, x) %#codegen
% void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]);
    X = zeros(1, 3);
    if coder.target('rtw') || coder.target('mex')
        if x < 0
            fprintf('ERROR: C_BSPLINE_EVAL: X < 0 (%f)\n', x);
            x = 0;
        elseif x > 1
            fprintf('ERROR: C_BSPLINE_EVAL: X > 1 (%f)\n', x);
            x = 1;
        end
        coder.updateBuildInfo('addSourceFiles','c_spline.c');
        coder.updateBuildInfo('addLinkFlags', '-lgsl');
        coder.cinclude('c_spline.h');
        coder.ceval('c_bspline_eval', coder.rref(Bl.handle), coder.rref(coeffs), x, coder.wref(X));
        x = X(1);
        xd = X(2);
        xdd = X(3);
    else
        [x, xd, xdd] = bspline_eval_mex(Bl, coeffs, x);
    end
end