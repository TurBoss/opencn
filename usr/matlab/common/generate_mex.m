clear; clc;
cfg = coder.config('mex', 'ecoder', true);
cfg.GenCodeOnly = false;
cfg.FilePartitionMethod = 'MapMFileTOCFile';
cfg.PreserveArrayDimensions = true;
cfg.GenerateReport = false;
cfg.TargetLang = 'C';
% cfg.CodeReplacementLibrary = 'Intel AVX (Linux)';
% cfg.Hardware = 'Intel->x86-64 (Linux 64)';
% cfg.HardwareImplementation.TargetHWDeviceType = 'Intel->x86-64 (Linux 64)';
cfg.HardwareImplementation.ProdHWDeviceType = 'Intel->x86-64 (Linux 64)';

cfg.CustomInclude = '../src ../../usr/include';
cfg.CustomSource = '../src/cpp_simplex.cpp';
cfg.CustomLibrary = ['/usr/lib/x86_64-linux-gnu/libClp.so ' ...
'/usr/lib/x86_64-linux-gnu/libCoinUtils.so'];
cfg.CustomHeaderCode = '#include "c_simplex.hpp"';
% cfg.CustomSourceCode = '#define MEX_COMPILE_FLAG';
cfg.EnableVariableSizing = true;
cfg.DynamicMemoryAllocation = 'Threshold';
% cfg.SILPILCheckConstantInputs = false;
% cfg.SILPILSyncGlobalData = false;
cfg.SaturateOnIntegerOverflow = false;
% cfg.StackUsageMax = 8096*1024;

% cfg.HighlightPotentialDataTypeIssues = true;
cfg.HighlightPotentialRowMajorIssues = true;

cfg.EnableMemcpy = false;           % Disabme memcopy
cfg.InitFltsAndDblsToZero = false;  % Disable memset

addpath('c_planner_v5')

global g_FeedoptConfig
InitConfig();


Nmax = FeedoptLimits.MaxNCoeff;
Ndiscr_max = FeedoptLimits.MaxNDiscr;

cfg.DynamicMemoryAllocationThreshold = FeedoptLimits.MaxNCoeff*FeedoptLimits.MaxNDiscr*16 + 1;

load c_simplex_args


% c_simplex(f, A, b, Aeq, beq)
codegen('-config', cfg,'-d', 'gen_mex/c_simplex', ...
    'c_simplex', '-args', {...
        coder.typeof(0.0, size(f), [1,1]), ...
        coder.typeof(sparse(0.0), size(A), [1,1]),...
        coder.typeof(0.0, size(b), [1,1]),...
        coder.typeof(0.0, size(Aeq), [1,1]),...
        coder.typeof(0.0, size(beq), [1,1])});
    
codegen('-config', cfg,'-d', 'gen_mex/bspline_create', ...
    'bspline_create', '-args', ...
        {0, 1, g_FeedoptConfig.SplineDegree, g_FeedoptConfig.NBreak});

Bl = bspline_create(0, 1, g_FeedoptConfig.SplineDegree, g_FeedoptConfig.NBreak);
 
codegen('-config', cfg,'-d', 'gen_mex/bspline_destroy', ...
    'bspline_destroy', '-args', Bl);

codegen('-config', cfg,'-d', 'gen_mex/bspline_base_eval', ...
    'bspline_base_eval', '-args', {Bl, coder.typeof(0.0, [1, FeedoptLimits.MaxNDiscr], [0, 1])});

codegen('-config', cfg,'-d', 'gen_mex/bspline_eval', ...
    'bspline_eval', '-args', ...
        {Bl, coder.typeof(0.0, [FeedoptLimits.MaxNCoeff, 1], [1, 0]), 0.5});
    
bspline_destroy(Bl);

