function [BasisVal, BasisValD, BasisValDD, BasisIntegr] = bspline_base_eval(Bl, xvec) %#codegen
    if coder.target('rtw') || coder.target('mex')
        % n, bspline_n
        samples = int32(numel(xvec));
        BasisVal = zeros(samples, Bl.n);
        BasisValD = BasisVal;
        BasisValDD = BasisVal; 
        BasisIntegr = BasisVal(1, :)';
        
        coder.updateBuildInfo('addSourceFiles','c_spline.c');
        coder.updateBuildInfo('addLinkFlags', '-lgsl');
        coder.cinclude('c_spline.h');
        coder.ceval('c_bspline_base_eval', coder.rref(Bl.handle), samples, coder.rref(xvec), ...,
            coder.ref(BasisVal), coder.ref(BasisValD), coder.ref(BasisValDD),...
            coder.ref(BasisIntegr));
    else
        [BasisVal, BasisValD, BasisValDD, BasisIntegr] = bspline_base_eval_mex(Bl, xvec);
    end
end