function SplitHelix(CrvStrct, QueueOut, L_split)
%
theta   = CrvStrct.theta;
pitch   = CrvStrct.pitch;
%
L = LengthHelix(CrvStrct);
%
N             = ceil(L/L_split);
L_split       = L/N;             % corrected L_split
% CrvStrctSplit = repmat(CrvStrct, 1, N); % preallocation (MALLOC TO BE RECHECKED !)
%
u_old     = 0;
%
for k = 1:N
    TmpCrvStrct = CrvStrct;
%     CrvStrctSplit(k).theta = theta/N;
%     CrvStrctSplit(k).pitch = pitch/N;
%     %
%     P0prim = EvalHelix(CrvStrct, u_old);
%     CrvStrctSplit(k).P0 = P0prim;    
%     %
%     u     = k*L_split/L;
%     P1prim = EvalHelix(CrvStrct, u);
%     CrvStrctSplit(k).P1 = P1prim;
%     %
%     u_old = u;
    TmpCrvStrct.theta = theta/N;
    TmpCrvStrct.pitch = pitch/N;
    %
    P0prim = EvalHelix(CrvStrct, u_old);
    TmpCrvStrct.P0 = P0prim;    
    %
    u     = k*L_split/L;
    P1prim = EvalHelix(CrvStrct, u);
    TmpCrvStrct.P1 = P1prim;
    %
    u_old = u;
    QueueOut.Push(TmpCrvStrct);
end





