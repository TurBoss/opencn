function CurvStruct = ConstrTransP5Struct(CoeffP5, FeedRate)
P0          = zeros(3, 1);
P1          = zeros(3, 1);
evec        = zeros(3, 1);
theta       = 0;
pitch       = 0;
CurvStruct  = ConstrCurvStruct(CurveType.TransP5, ZSpdMode.NN, P0, P1, evec, theta, pitch, CoeffP5, FeedRate);
