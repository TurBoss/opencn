function [A, b, Aeq, beq] = BuildConstr_v4(CurvStructs, amax, v_0, at_0, v_1, at_1, ...
    BasisVal, BasisValD, u_vec)

DebugLog('BuildConstr_v4 with Ncrv = %d, amax = [%f, %f, %f], v_0 = %f, at_0 = %f, v_1 = %f, at_1 = %f\n', ...
    int32(numel(CurvStructs)), amax(1), amax(2), amax(3), v_0, at_0, v_1, at_1);


c_prof_in(mfilename);
Ncrv   = length(CurvStructs);
[M, N] = size(BasisVal);
%
A      = sparse(7*M*Ncrv,   N*Ncrv);  % preallocation
b      = zeros(7*M*Ncrv,   1);        % preallocation
Aeq    = zeros(2*(Ncrv+1), N*Ncrv);   % preallocation
beq    = zeros(2*(Ncrv+1), 1);        % preallocation

% coder.varsize('BasisVal', [FeedoptLimits.MaxNDiscr, FeedoptLimits.MaxNCoeff], [1, 1]);
% coder.varsize('BasisValD', [FeedoptLimits.MaxNDiscr, FeedoptLimits.MaxNCoeff], [1, 1]);
% coder.varsize('A', [7*FeedoptLimits.MaxNDiscr*FeedoptLimits.MaxNHorz, FeedoptLimits.MaxNCoeff*FeedoptLimits.MaxNHorz], [1,1]);
coder.varsize('b', [7*FeedoptLimits.MaxNDiscr*FeedoptLimits.MaxNHorz, 1], [1,0]);
coder.varsize('Aeq', [2*(FeedoptLimits.MaxNHorz+1), FeedoptLimits.MaxNCoeff*FeedoptLimits.MaxNHorz], [1,1]);
coder.varsize('beq', [2*(FeedoptLimits.MaxNHorz+1), 1], [1,0]);

%
[~, r1D, r2D] = EvalCurvStruct(CurvStructs(1), u_vec);
vmax          = CurvStructs(1).FeedRate;
r1D_sqnorm    = sum(r1D.^2);      % squared norm
%
t_0 = r1D(:, 1)/norm(r1D(:, 1));  % unit tangent vector @ start
%
%
R1 = bsxfun(@times, r2D(1, :)' , BasisVal) + 0.5*bsxfun(@times, r1D(1, :)' , BasisValD);
R2 = bsxfun(@times, r2D(2, :)' , BasisVal) + 0.5*bsxfun(@times, r1D(2, :)' , BasisValD);
R3 = bsxfun(@times, r2D(3, :)' , BasisVal) + 0.5*bsxfun(@times, r1D(3, :)' , BasisValD);

%
A(1:7*M, 1:N)  = [BasisVal;
    R1;
    -R1;
    R2;
    -R2;
    R3;
    -R3];
%
bC1 = (vmax)^2./r1D_sqnorm';
bC2 = amax(1)*ones(M, 1);
bC3 = amax(2)*ones(M, 1);
bC4 = amax(3)*ones(M, 1);
%
b(1:7*M)       = [bC1;
    bC2;
    bC2;
    bC3;
    bC3;
    bC4;
    bC4];
%
Aeq(1:2, 1:N)   = [BasisVal(1, :);
    t_0' * [r2D(1, 1)   * BasisVal(1, :)   + 0.5*r1D(1, 1)   * BasisValD(1, :);
    r2D(2, 1)   * BasisVal(1, :)   + 0.5*r1D(2, 1)   * BasisValD(1, :);
    r2D(3, 1)   * BasisVal(1, :)   + 0.5*r1D(3, 1)   * BasisValD(1, :)]];
beq(1:2)       = [(v_0(1)^2)/r1D_sqnorm(1);
    at_0(1)];
%
r1Dn_sqnorm = zeros(1, M);
for k = 1:Ncrv-1
    [~, r1Dn, r2Dn] = EvalCurvStruct(CurvStructs(k+1), u_vec);
    vmax            = CurvStructs(k+1).FeedRate;
    r1Dn_sqnorm     = sum(r1Dn.^2);        % squared norm
    bC1 = (vmax)^2./r1Dn_sqnorm';
    t_1 = r1D(:, end)/norm(r1D(:, end));   % unit tangent vector @ end of previous piece
    %
    R1 = bsxfun(@times, r2Dn(1, :)' , BasisVal) + 0.5*bsxfun(@times, r1Dn(1, :)' , BasisValD);
    R2 = bsxfun(@times, r2Dn(2, :)' , BasisVal) + 0.5*bsxfun(@times, r1Dn(2, :)' , BasisValD);
    R3 = bsxfun(@times, r2Dn(3, :)' , BasisVal) + 0.5*bsxfun(@times, r1Dn(3, :)' , BasisValD);
    %
    A(k*7*M+1:(k+1)*7*M, k*N+1:(k+1)*N) = ...
        [BasisVal;
        R1;
        -R1;
        R2;
        -R2;
        R3;
        -R3];
    %
    b(k*7*M+1:(k+1)*7*M) = [bC1;
        bC2;
        bC2;
        bC3;
        bC3;
        bC4;
        bC4];
    %
    Aeq(2*k+1, (k-1)*N+1:k*N) =  (t_1'*r1D(:, end))^2 * BasisVal(end, :);
    Aeq(2*k+1, k*N+1:(k+1)*N) = -(t_1'*r1Dn(:, 1))^2  * BasisVal(1,   :);
    
    Aeq(2*k+2, (k-1)*N+1:k*N) =   t_1'*[r2D(1, end)   * BasisVal(end, :)   + 0.5*r1D(1, end)   * BasisValD(end, :);
        r2D(2, end)   * BasisVal(end, :)   + 0.5*r1D(2, end)   * BasisValD(end, :);
        r2D(3, end)   * BasisVal(end, :)   + 0.5*r1D(3, end)   * BasisValD(end, :)];
    %
    Aeq(2*k+2, k*N+1:(k+1)*N) = -(t_1'*[r2Dn(1, 1)   * BasisVal(1, :)   + 0.5*r1Dn(1, 1)   * BasisValD(1, :);
        r2Dn(2, 1)   * BasisVal(1, :)   + 0.5*r1Dn(2, 1)   * BasisValD(1, :);
        r2Dn(3, 1)   * BasisVal(1, :)   + 0.5*r1Dn(3, 1)   * BasisValD(1, :)]);
    %
    r1D = r1Dn;
    r2D = r2Dn;
end
%
t_1 = r1D(:, end)/norm(r1D(:, end));   % unit tangent vector @ end of previous piece
%
Aeq(end-1:end, end-N+1:end)   = [BasisVal(end, :);
    t_1' * [r2D(1, end) * BasisVal(end, :) + 0.5*r1D(1, end) * BasisValD(end, :);
    r2D(2, end) * BasisVal(end, :) + 0.5*r1D(2, end) * BasisValD(end, :);
    r2D(3, end) * BasisVal(end, :) + 0.5*r1D(3, end) * BasisValD(end, :)]];
%
beq(end-1:end) = [(v_1^2)/r1Dn_sqnorm(end);
    at_1];
c_prof_out(mfilename);
end

