function [Coeff, NCoeff, v_0, at_0, success] = FeedratePlanning_v4(CurvStructs0, amax, jmax, v_0, at_0, v_1, at_1, ...
                                     BasisVal, BasisValD, BasisValDD, BasisIntegr, Bl, u_vec, N_Hor)
persistent f
%
c_prof_in(mfilename);
CurvStructs = CurvStructs0(1:N_Hor);
coder.varsize('CurvStructs', [1, FeedoptLimits.MaxNHorz], [0,1]);
Ncrv   = length(CurvStructs);
[~, N] = size(BasisVal);
%
%% FIRST setup of Linear Program (LP) WITHOUT jerk constraint
f       = -repmat(BasisIntegr, 1, Ncrv);  % maximize integral of q
%% equality constraints

[A, b, Aeq, beq] = BuildConstr_v4(CurvStructs, amax, v_0, at_0, v_1, at_1, ...
                                  BasisVal, BasisValD, u_vec);
                              
                              
%
%
%% LP solver options        
% options = optimoptions('linprog', 'Algorithm', 'interior-point', 'OptimalityTolerance', 1e-6, ...
%                        'ConstraintTolerance', 1e-6, 'MaxIterations', 2000);
% options = optimoptions('linprog', 'OptimalityTolerance', 1e-6, ...
%                        'ConstraintTolerance', 1e-6, 'Display', 'off');
%% run LP solver first time
% Coeff0 = linprog(f, A, b, Aeq, beq, [], [], options);
        
coder.varsize('f', [FeedoptLimits.MaxNCoeff, FeedoptLimits.MaxNHorz], [1, 1]);
[Coeff0, success] = c_simplex(f, A, b, Aeq, beq);
%
% tic
% Coeff1 = linprog(f, A, b, Aeq, beq, [], [], options);
% toc Coeff(:, end-N_Hor+2:end) = C(:, 2:end);
%
if ~success
    Coeff = [];
    NCoeff = int32(0);
    v_0 = v_0;
    at_0 = at_0;
    return;
end
Coeff1  = reshape(Coeff0, N, Ncrv);
DebugLog('Coeff1 = ')
for k = 1:N
    DebugLog('%.4f ', Coeff1(k, 1));
end
DebugLog('\n')
%
%% SECOND setup of Linear Program (LP) WITH jerk constraint
[A_jerk, b_jerk] = BuildConstrJerk_v4(CurvStructs, Coeff1, jmax,  ...
                                      BasisVal, BasisValD, BasisValDD, Bl, u_vec);

%
Atot = [A;
        A_jerk];
%    
btot = [b;
        b_jerk];
%
%% run LP solver second time
% Coeff2 = linprog(f, Atot, btot, Aeq, beq, [], [], options);
[Coeff2, success] = c_simplex(f, Atot, btot, Aeq, beq);
if ~success
    Coeff = [];
    NCoeff = int32(0);
    v_0 = v_0;
    at_0 = at_0;
    return;
end
%
% tic
% Coeff1 = linprog(f, Atot, btot, Aeq, beq, [], [], options);
% toc
%
Coeff3 = reshape(Coeff2, N, Ncrv);
DebugLog('Coeff3 = ')
for k = 1:N
    DebugLog('%.4f ', Coeff3(k, 1));
end
DebugLog('\n')
% % 
[v_0, a_0]  = CalcVAJ_v5(CurvStructs(1), Bl, Coeff3(:, 1), {1});
[~, r1D]         = EvalCurvStruct(CurvStructs(1), 1);
t_end            = r1D/MyNorm(r1D); % unit tangential vector
at_0 = a_0'*t_end; % tangential acceleration at the end of first piece in horizon
% Coeff = Coeff3(:, 1);
Coeff = zeros(FeedoptLimits.MaxNCoeff, FeedoptLimits.MaxNHorz);
coder.varsize('Coeff', [FeedoptLimits.MaxNCoeff, FeedoptLimits.MaxNHorz], [1,1]);
Coeff(1:N,1:N_Hor) = Coeff3;
NCoeff = int32(N);
c_prof_out(mfilename);

% CurOptStruct = struct('Coeff', Coeff(1:N, 1));
% u = 0;
% uvec = zeros(10000, 1);
% for ktick = 1:length(uvec)
%     u = ResampleTick(CurOptStruct, Bl, u, 1e-4);
%     uvec(ktick) = u;
% end
% 
% plot(uvec);
% ylim([-0.1, 1.1])
% 
% uvec