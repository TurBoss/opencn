function PrintCurvStruct(S)

DebugLog('CURVE STRUCT\n')
switch S.Type
    case CurveType.Line
        DebugLog('    Type: Line\n')
        DebugLog('ZSpdMode: %s\n', PrintZSpdMode(S.ZSpdMode))
        DebugLog('      P0: [%.3f %.3f %.3f]\n', S.P0(1), S.P0(2), S.P0(3))
        DebugLog('      P1: [%.3f %.3f %.3f]\n', S.P1(1), S.P1(2), S.P1(3))
        DebugLog('FeedRate: %.2f\n', S.FeedRate)
    case CurveType.Helix
        DebugLog('    Type: Helix\n')
        DebugLog('ZSpdMode: %s\n', PrintZSpdMode(S.ZSpdMode))
        DebugLog('      P0: [%.3f %.3f %.3f]\n', S.P0(1), S.P0(2), S.P0(3))
        DebugLog('      P1: [%.3f %.3f %.3f]\n', S.P1(1), S.P1(2), S.P1(3))
        DebugLog('    evec: [%.3f %.3f %.3f]\n', S.evec(1), S.evec(2), S.evec(3))
        DebugLog('   theta: %.3f\n', S.theta);
        DebugLog('   pitch: %.3f\n', S.pitch);
        DebugLog('FeedRate: %.2f\n', S.FeedRate)
    case CurveType.TransP5
        DebugLog('    Type: TransP5\n')
        DebugLog('ZSpdMode: %s\n', PrintZSpdMode(S.ZSpdMode))
        DebugLog(' CoeffP5: \n');
        DebugLog('| ')
        for CoeffRow = S.CoeffP5'
            for v = CoeffRow
                DebugLog('%.3f ', v(1))
            end
            DebugLog('| ')
        end
        DebugLog('\n')
        DebugLog('FeedRate: %.2f\n', S.FeedRate)
    otherwise
        fprintf('!!! Type = %d, UNKNOWN !!!\n', int32(S.Type))
end
end

function str = PrintZSpdMode(m)
switch m
    case ZSpdMode.NN
        str = "NN";
    case ZSpdMode.ZN
        str = "ZN";
    case ZSpdMode.NZ
        str = "NZ";
    case ZSpdMode.ZZ
        str = "ZZ";
    otherwise
        str = "<UNKNOWN>";
end
end