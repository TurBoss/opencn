function CurvStruct1 = CutCurvStruct(CurvStruct, d0, d1)
%
Type  = CurvStruct.Type;
%

L = LengthCurv(CurvStruct);
if d0 >= L || L - d1 <= 0
    CurvStruct1 = CurvStruct;
else
    switch Type
        case CurveType.Line % line (G01)
            CurvStruct1 = CutLine(CurvStruct, d0, d1);
        case CurveType.Helix % arc of circle / helix (G02, G03)
            CurvStruct1 = CutHelix(CurvStruct, d0, d1);
        otherwise
            CurvStruct1 = CurvStruct;
            CurvStruct1.Type = CurveType.None;
            c_assert(0, 'Unknown Curve Type for Cut.');
    end
end
