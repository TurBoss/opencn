classdef Queue
    properties(Constant, Access = public)
        ValueType = ConstrLineStruct([1,0,0]', [0,0,0]', 0.2, ZSpdMode.NN);
    end
    
    properties
        id
    end
    
    properties
        Size
    end
    
    methods
        function obj = Queue(id)
            if coder.target('rtw')
                obj.id = int32(id);
            else
                obj.id = int32(id) + 1;
            end
        end
        
        function obj = Push(obj, Value)
            if coder.target('rtw')
                coder.ceval('queue_push', obj.id, Value);
            else
                QueuePush(obj.id, Value);
            end
        end
        
        function obj = Resize(obj, N, DefaultValue)
            if coder.target('rtw')
                coder.ceval('queue_resize', obj.id, int32(N), DefaultValue);
            else
                QueueResize(obj.id, N, DefaultValue);
            end
        end
        
        function value = Get(obj, k)
            % Forward Get
            if coder.target('rtw')
                value = obj.ValueType;
                value = coder.ceval('queue_get', obj.id, int32(k - 1));
            else
                value = QueueGet(obj.id, k);
            end
        end
        
%         function values = GetRange(obj, kbeg, kend)
%             if coder.target('rtw')
%                 values = repmat(obj.ValueType, 1, g_FeedoptConfig.MaxNHorz);
%                 coder.eval('queue_get_range', obj.id, kbeg, kbed, coder.wref(values));
%             else
%                 values = QueueGetRange(obj.id, kbeg, kend);
%             end
%         end
        
        function value = RGet(obj, k)
            % Reverse Get
            if coder.target('rtw')
                value = obj.ValueType;
                value = coder.ceval('queue_get', obj.id, int32(obj.Size - k - 1));
            else
                value = QueueGet(obj.id, QueueSize(obj.id) - k);
            end
        end
        
        function obj = Set(obj, k, Value)
            if coder.target('rtw')
                coder.ceval('queue_set', obj.id, int32(k - 1), Value);
            else
                QueueSet(obj.id, k, Value);
            end
        end
        
        function obj = RSet(obj, k, Value)
            if coder.target('rtw')
                coder.ceval('queue_set', obj.id, int32(obj.Size - k - 1), Value);
            else
                QueueSet(obj.id, QueueSize(obj.id) - k, Value);
            end
        end
        
        function obj = Clear(obj)
            fprintf('Queue.Clear id = %d\n', obj.id);
            if coder.target('rtw')
                coder.ceval('queue_clear', obj.id);
            else
                QueueClear(obj.id);
            end
        end
        
        function values = Values(obj)
            if coder.target('rtw')
                values = ConstrLineStruct([1,1,1]', [0,0,0]', 0.2, ZSpdMode.NN);
                coder.varsize('values', [1 1], [0, 1]);
                coder.ceval('queue_data', obj.id, coder.wref(values));
            else
                values = QueueData(obj.id);
            end
        end
        
        function v = get.Size(obj)
            if coder.target('rtw')
                v = int32(0);
                v = coder.ceval('queue_size', obj.id);
            else
                v = QueueSize(obj.id);
            end
        end
    end
end