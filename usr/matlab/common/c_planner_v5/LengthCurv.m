function L = LengthCurv(Curv)

switch Curv.Type
    case CurveType.Helix
        L = LengthHelix(Curv);
    case CurveType.Line
        L = norm(Curv.P1 - Curv.P0);
    otherwise
        c_assert(0, 'BAD CURVE TYPE IN LENGTH CURV');
        L = 0;
end

end