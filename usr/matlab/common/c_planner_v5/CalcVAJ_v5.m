function [v_norm, a, j] = CalcVAJ_v5(CurvStructs, Bl, Coeff, u_cell)
%
% import splines.*
%
[~, Ncrv] = size(Coeff);

% Ntot = 0;
% for k = 1:Ncrv
%     Ntot = Ntot + size(u_cell{k}, 2);
% end

v_norm    = zeros(1, 1);
a         = zeros(3, 1);
j         = zeros(3, 1);

% v_norm = [];
% a = [];
% j = [];

index = 1;

%
for k = 1:1
%     qSpl = Function(Bl, Coeff(:, k));
    u_vec = u_cell{k};
    %
    [~, r1D, r2D, r3D] = EvalCurvStruct(CurvStructs(k), u_vec);
    r1D_norm             = sqrt(sum(r1D.^2));  % norm
    %
%     q_val   = qSpl.fast_eval(u_vec);
%     qD_val  = qSpl.derivative.fast_eval(u_vec);
%     qDD_val = qSpl.derivative(2).fast_eval(u_vec);
    
    q_val = zeros(size(u_vec));
    qD_val = zeros(size(u_vec));
    qDD_val = zeros(size(u_vec));

    % TODO: Optimize this with a single call to eval, and maybe a basis
    % precompute?
    for iu = 1:numel(u_vec)
        [v1, v2, v3] = bspline_eval(Bl, Coeff(:, k), u_vec(iu));
        q_val(iu) = v1;
        qD_val(iu) = v2;
        qDD_val(iu) = v3;
    end
    
    
    q_val = q_val';
    qD_val = qD_val';
    qDD_val = qDD_val';
    %
%     r1D_norm.*sqrt(q_val')

    tmp1 = bsxfun(@times, r1D_norm, sqrt(q_val'));
    tmp2 = bsxfun(@times, r2D, q_val') + 0.5*bsxfun(@times, r1D, qD_val');
    tmp3 = bsxfun(@times, r3D, (q_val.^(3/2))') + ...
        1.5*bsxfun(@times, r2D, bsxfun(@times, qD_val',sqrt(q_val)')) + ...
        0.5*bsxfun(@times, r1D, bsxfun(@times, qDD_val',sqrt(q_val)')); 

    v_norm = tmp1;
    a = tmp2;%zeros(3, size(tmp2, 2));
    j = tmp3;
end





