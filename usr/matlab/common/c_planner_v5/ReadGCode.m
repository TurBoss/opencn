function status = ReadGCode(QueueOut)
% Wrapper for pulling the next gcode line from the interpreter
global g_FeedoptConfig
persistent n data
if coder.target('rtw')
    status = int32(0);
    status = coder.ceval('feedopt_read_gcode', QueueOut.id);
else 
    if isempty(n)
        n = 0;
        data = load('pieces_demo/PieceDemoLinuxCNC_HV_V04_CS');
    end
    
    if n < length(data.CurvStructs)
        status = 1;
%         if LengthCurv(data.CurvStructs(n+1)) > 2*g_FeedoptConfig.CutOff
        QueueOut.Push(data.CurvStructs(n+1));
%         end
    else
        status = 0;
    end
    
%     switch n
%         case 0
%             OptStruct = ConstrLineStruct([0,0,0]', [1,0,0]', 1, ZSpdMode.NN);
%             QueueOut.Push(OptStruct);
%             status = 1;
%         case 1
%             OptStruct = ConstrLineStruct([1,0,0]', [1,1,0]', 1, ZSpdMode.NN);
%             QueueOut.Push(OptStruct);
%             status = 1;
%         case 2
%             OptStruct = ConstrLineStruct([1,1,0]', [0,1,0]', 1, ZSpdMode.NN);
%             QueueOut.Push(OptStruct);
%             status = 1;
%         case 3
%             OptStruct = ConstrLineStruct([0,1,0]', [0,0,0]', 1, ZSpdMode.NN);
%             QueueOut.Push(OptStruct);
%             status = 1;
%         otherwise
%             status = 0;
%     end
%     n = mod(n + 1, 4);
    n = n + 1;
end

end