/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: LengthHelix.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "LengthHelix.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 * Return Type  : double
 */
double LengthHelix(const double CurvStruct_P0[3], const double CurvStruct_P1[3],
                   const double CurvStruct_evec[3], double CurvStruct_theta,
                   double CurvStruct_pitch)
{
    double d;
    double P0P1_idx_0;
    double P0P1;
    double P0P1_idx_1;
    double a;

    /*  */
    /*  */
    d = CurvStruct_P1[0] - CurvStruct_P0[0];
    P0P1_idx_0 = d;
    P0P1 = d * CurvStruct_evec[0];
    d = CurvStruct_P1[1] - CurvStruct_P0[1];
    P0P1_idx_1 = d;
    P0P1 += d * CurvStruct_evec[1];
    d = CurvStruct_P1[2] - CurvStruct_P0[2];
    P0P1 += d * CurvStruct_evec[2];
    a = 1.0 / tan(CurvStruct_theta / 2.0);

    /*  */
    return CurvStruct_theta * sqrt(pow(sqrt((pow(CurvStruct_P0[0] -
        ((CurvStruct_P0[0] + (CurvStruct_P1[0] - P0P1 * CurvStruct_evec[0])) + a
         * (CurvStruct_evec[1] * d - CurvStruct_evec[2] * P0P1_idx_1)) / 2.0,
        2.0) + pow(CurvStruct_P0[1] - ((CurvStruct_P0[1] + (CurvStruct_P1[1] -
        P0P1 * CurvStruct_evec[1])) + a * (CurvStruct_evec[2] * P0P1_idx_0 -
        CurvStruct_evec[0] * d)) / 2.0, 2.0)) + pow(CurvStruct_P0[2] -
        ((CurvStruct_P0[2] + (CurvStruct_P1[2] - P0P1 * CurvStruct_evec[2])) + a
         * (CurvStruct_evec[0] * P0P1_idx_1 - CurvStruct_evec[1] * P0P1_idx_0)) /
        2.0, 2.0)), 2.0) + pow(CurvStruct_pitch / 6.2831853071795862, 2.0));
}

/*
 * File trailer for LengthHelix.c
 *
 * [EOF]
 */
