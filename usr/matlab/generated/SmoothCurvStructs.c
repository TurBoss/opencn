/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: SmoothCurvStructs.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "SmoothCurvStructs.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "InitConfig.h"
#include "LengthHelix.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <math.h>
#include <stdio.h>

/* Function Definitions */

/*
 * Arguments    : int QueueIn_id
 *                int QueueOut_id
 *                double CutOff
 * Return Type  : void
 */
void SmoothCurvStructs(int QueueIn_id, int QueueOut_id, double CutOff)
{
    int Ncrv;
    int k;
    CurvStruct value;
    CurvStruct CurvStruct1;
    CurvStruct CurvStruct2_C;
    int i;
    double L;
    double scale;
    double absxk;
    char message[29];
    boolean_T guard1 = false;
    boolean_T guard2 = false;
    double t;
    double x[3];
    double r0D1[3];
    double r1D1[3];
    CurvStruct CurvStruct1_C;
    CurvStruct CurvStruct_T;
    Ncrv = queue_size(QueueIn_id);
    if (Ncrv != 0) {
        /*  */
        /*  calculate the number of transition curves */
        /*  Ntrans = 0; */
        /*  % */
        /*  for k = 1:Ncrv-1 */
        /*      if (CurvStructs(k).ZSpdMode == ZSpdMode.NN) || (CurvStructs(k).ZSpdMode == ZSpdMode.ZN) */
        /*          Ntrans = Ntrans + 1; */
        /*      end */
        /*  end */
        /*  */
        for (k = 0; k <= Ncrv - 2; k++) {
            /*  Forward Get */
            value = queue_get(QueueIn_id, k);
            if (!(value.ZSpdMode == ZSpdMode_NN)) {
                /*  Forward Get */
                queue_get(QueueIn_id, k);
            }
        }

        /*  preallocation of smoothed curve array */
        /*  QueueOut.Resize(Ncrv + Ntrans, QueueIn.Get(1)); % CurvStructs1 = repmat(CurvStructs(1), 1, Ncrv+Ntrans); */
        /*  Forward Get */
        CurvStruct1 = queue_get(QueueIn_id, 0);

        /*  CurvStruct1 = CurvStructs(1); */
        if (Ncrv > 1) {
            /*  Satisfy CurvStruct2_C is not fully defined on some execution paths */
            /*  @HACK HANDLE THIS BETTER */
            CurvStruct2_C = CurvStruct1;

            /*  */
            /*  */
            for (k = 0; k <= Ncrv - 2; k++) {
                if ((CurvStruct1.ZSpdMode != ZSpdMode_NZ) &&
                        (CurvStruct1.ZSpdMode != ZSpdMode_ZZ)) {
                    /*          [CurvStruct1_C, CurvStruct_T, CurvStruct2_C]  = ... */
                    /*              CalcTransition(CurvStruct1, CurvStructs(k+1), CutOff); */
                    /*  Forward Get */
                    value = queue_get(QueueIn_id, k + 1);
                    switch (value.Type) {
                      case CurveType_Helix:
                        L = LengthHelix(value.P0, value.P1, value.evec,
                                        value.theta, value.pitch);
                        break;

                      case CurveType_Line:
                        scale = 3.3121686421112381E-170;
                        absxk = fabs(value.P1[0] - value.P0[0]);
                        if (absxk > 3.3121686421112381E-170) {
                            L = 1.0;
                            scale = absxk;
                        } else {
                            t = absxk / 3.3121686421112381E-170;
                            L = t * t;
                        }

                        absxk = fabs(value.P1[1] - value.P0[1]);
                        if (absxk > scale) {
                            t = scale / absxk;
                            L = L * t * t + 1.0;
                            scale = absxk;
                        } else {
                            t = absxk / scale;
                            L += t * t;
                        }

                        absxk = fabs(value.P1[2] - value.P0[2]);
                        if (absxk > scale) {
                            t = scale / absxk;
                            L = L * t * t + 1.0;
                            scale = absxk;
                        } else {
                            t = absxk / scale;
                            L += t * t;
                        }

                        L = scale * sqrt(L);
                        break;

                      default:
                        for (i = 0; i < 29; i++) {
                            message[i] = cv3[i];
                        }

                        c_assert_(&message[0]);
                        L = 0.0;
                        break;
                    }

                    guard1 = false;
                    guard2 = false;
                    if (L > 3.0 * CutOff) {
                        b_EvalCurvStruct(CurvStruct1.Type, CurvStruct1.P0,
                                         CurvStruct1.P1, CurvStruct1.evec,
                                         CurvStruct1.theta, CurvStruct1.pitch,
                                         CurvStruct1.CoeffP5, x, r0D1);

                        /*  Forward Get */
                        value = queue_get(QueueIn_id, k + 1);
                        c_EvalCurvStruct(value.Type, value.P0, value.P1,
                                         value.evec, value.theta, value.pitch,
                                         value.CoeffP5, x, r1D1);
                        if (CurvStruct1.Type == CurveType_Line) {
                            /*  Forward Get */
                            value = queue_get(QueueIn_id, k + 1);
                            if (value.Type == CurveType_Line) {
                                scale = 3.3121686421112381E-170;
                                absxk = fabs(r0D1[1] * r1D1[2] - r0D1[2] * r1D1
                                             [1]);
                                if (absxk > 3.3121686421112381E-170) {
                                    L = 1.0;
                                    scale = absxk;
                                } else {
                                    t = absxk / 3.3121686421112381E-170;
                                    L = t * t;
                                }

                                absxk = fabs(r0D1[2] * r1D1[0] - r0D1[0] * r1D1
                                             [2]);
                                if (absxk > scale) {
                                    t = scale / absxk;
                                    L = L * t * t + 1.0;
                                    scale = absxk;
                                } else {
                                    t = absxk / scale;
                                    L += t * t;
                                }

                                absxk = fabs(r0D1[0] * r1D1[1] - r0D1[1] * r1D1
                                             [0]);
                                if (absxk > scale) {
                                    t = scale / absxk;
                                    L = L * t * t + 1.0;
                                    scale = absxk;
                                } else {
                                    t = absxk / scale;
                                    L += t * t;
                                }

                                L = scale * sqrt(L);
                                if (L < 1.0E-6) {
                                    queue_push(QueueOut_id, CurvStruct1);

                                    /*  Forward Get */
                                    CurvStruct1 = queue_get(QueueIn_id, k + 1);
                                } else {
                                    guard2 = true;
                                }
                            } else {
                                guard2 = true;
                            }
                        } else {
                            guard2 = true;
                        }
                    } else {
                        /*  Forward Get */
                        value = queue_get(QueueIn_id, k + 1);
                        switch (value.Type) {
                          case CurveType_Helix:
                            L = LengthHelix(value.P0, value.P1, value.evec,
                                            value.theta, value.pitch);
                            break;

                          case CurveType_Line:
                            scale = 3.3121686421112381E-170;
                            absxk = fabs(value.P1[0] - value.P0[0]);
                            if (absxk > 3.3121686421112381E-170) {
                                L = 1.0;
                                scale = absxk;
                            } else {
                                t = absxk / 3.3121686421112381E-170;
                                L = t * t;
                            }

                            absxk = fabs(value.P1[1] - value.P0[1]);
                            if (absxk > scale) {
                                t = scale / absxk;
                                L = L * t * t + 1.0;
                                scale = absxk;
                            } else {
                                t = absxk / scale;
                                L += t * t;
                            }

                            absxk = fabs(value.P1[2] - value.P0[2]);
                            if (absxk > scale) {
                                t = scale / absxk;
                                L = L * t * t + 1.0;
                                scale = absxk;
                            } else {
                                t = absxk / scale;
                                L += t * t;
                            }

                            L = scale * sqrt(L);
                            break;

                          default:
                            for (i = 0; i < 29; i++) {
                                message[i] = cv3[i];
                            }

                            c_assert_(&message[0]);
                            L = 0.0;
                            break;
                        }

                        if (L > 2.0 * CutOff) {
                            b_EvalCurvStruct(CurvStruct1.Type, CurvStruct1.P0,
                                             CurvStruct1.P1, CurvStruct1.evec,
                                             CurvStruct1.theta,
                                             CurvStruct1.pitch,
                                             CurvStruct1.CoeffP5, x, r0D1);

                            /*  Forward Get */
                            value = queue_get(QueueIn_id, k + 1);
                            c_EvalCurvStruct(value.Type, value.P0, value.P1,
                                             value.evec, value.theta,
                                             value.pitch, value.CoeffP5, x, r1D1);
                            if (CurvStruct1.Type == CurveType_Line) {
                                /*  Forward Get */
                                value = queue_get(QueueIn_id, k + 1);
                                if (value.Type == CurveType_Line) {
                                    scale = 3.3121686421112381E-170;
                                    absxk = fabs(r0D1[1] * r1D1[2] - r0D1[2] *
                                                 r1D1[1]);
                                    if (absxk > 3.3121686421112381E-170) {
                                        L = 1.0;
                                        scale = absxk;
                                    } else {
                                        t = absxk / 3.3121686421112381E-170;
                                        L = t * t;
                                    }

                                    absxk = fabs(r0D1[2] * r1D1[0] - r0D1[0] *
                                                 r1D1[2]);
                                    if (absxk > scale) {
                                        t = scale / absxk;
                                        L = L * t * t + 1.0;
                                        scale = absxk;
                                    } else {
                                        t = absxk / scale;
                                        L += t * t;
                                    }

                                    absxk = fabs(r0D1[0] * r1D1[1] - r0D1[1] *
                                                 r1D1[0]);
                                    if (absxk > scale) {
                                        t = scale / absxk;
                                        L = L * t * t + 1.0;
                                        scale = absxk;
                                    } else {
                                        t = absxk / scale;
                                        L += t * t;
                                    }

                                    L = scale * sqrt(L);
                                    if (L < 1.0E-6) {
                                        queue_push(QueueOut_id, CurvStruct1);

                                        /*  Forward Get */
                                        CurvStruct1 = queue_get(QueueIn_id, k +
                                                                1);
                                    } else {
                                        guard1 = true;
                                    }
                                } else {
                                    guard1 = true;
                                }
                            } else {
                                guard1 = true;
                            }
                        } else {
                            if (g_FeedoptConfig.DebugPrint) {
                                printf("!!! SKIPPING CURVE !!!\n");
                                fflush(stdout);
                            }

                            /*  Forward Get */
                            value = queue_get(QueueIn_id, k + 1);
                            PrintCurvStruct(&value);
                        }
                    }

                    if (guard2) {
                        /*  Forward Get */
                        value = queue_get(QueueIn_id, k + 1);
                        CalcTransition(&CurvStruct1, &value, CutOff,
                                       &CurvStruct1_C, &CurvStruct_T,
                                       &CurvStruct2_C);
                        queue_push(QueueOut_id, CurvStruct1_C);

                        /*  CurvStructs1(kk)   = CurvStruct1_C; */
                        queue_push(QueueOut_id, CurvStruct_T);

                        /*  CurvStructs1(kk+1) = CurvStruct_T; */
                        CurvStruct1 = CurvStruct2_C;
                    }

                    if (guard1) {
                        /*  If the the segment length is between 2 and 3 cutoffs, use a */
                        /*  half cutoff */
                        /*  Forward Get */
                        value = queue_get(QueueIn_id, k + 1);
                        CalcTransition(&CurvStruct1, &value, CutOff / 2.0,
                                       &CurvStruct1_C, &CurvStruct_T,
                                       &CurvStruct2_C);
                        queue_push(QueueOut_id, CurvStruct1_C);

                        /*  CurvStructs1(kk)   = CurvStruct1_C; */
                        queue_push(QueueOut_id, CurvStruct_T);

                        /*  CurvStructs1(kk+1) = CurvStruct_T; */
                        CurvStruct1 = CurvStruct2_C;
                    }
                } else {
                    /*  Forward Get */
                    value = queue_get(QueueIn_id, k);
                    queue_push(QueueOut_id, value);

                    /*  CurvStructs1(kk)   = CurvStructs(k); */
                    /*  Forward Get */
                    CurvStruct1 = queue_get(QueueIn_id, k + 1);

                    /*  CurvStruct1 = CurvStructs(k+1); */
                }
            }

            /*  */
            /*  if (CurvStructs(end-1).ZSpdMode ~= ZSpdMode.NZ) && (CurvStructs(end-1).ZSpdMode ~= ZSpdMode.ZZ) */
            /*      CurvStructs1(end) = CurvStruct2_C; */
            /*  else */
            /*      CurvStructs1(end) = CurvStructs(end); */
            /*  end */
            /*  Reverse Get */
            /*          function values = GetRange(obj, kbeg, kend) */
            /*              if coder.target('rtw') */
            /*                  values = repmat(obj.ValueType, 1, g_FeedoptConfig.MaxNHorz); */
            /*                  coder.eval('queue_get_range', obj.id, kbeg, kbed, coder.wref(values)); */
            /*              else */
            /*                  values = QueueGetRange(obj.id, kbeg, kend); */
            /*              end */
            /*          end */
            Ncrv = queue_size(QueueIn_id);
            value = queue_get(QueueIn_id, Ncrv - 2);
            guard1 = false;
            if (value.ZSpdMode != ZSpdMode_NZ) {
                /*  Reverse Get */
                /*          function values = GetRange(obj, kbeg, kend) */
                /*              if coder.target('rtw') */
                /*                  values = repmat(obj.ValueType, 1, g_FeedoptConfig.MaxNHorz); */
                /*                  coder.eval('queue_get_range', obj.id, kbeg, kbed, coder.wref(values)); */
                /*              else */
                /*                  values = QueueGetRange(obj.id, kbeg, kend); */
                /*              end */
                /*          end */
                Ncrv = queue_size(QueueIn_id);
                value = queue_get(QueueIn_id, Ncrv - 2);
                if (value.ZSpdMode != ZSpdMode_ZZ) {
                    /*          QueueOut.RSet(0, CurvStruct2_C); */
                    queue_push(QueueOut_id, CurvStruct2_C);
                } else {
                    guard1 = true;
                }
            } else {
                guard1 = true;
            }

            if (guard1) {
                /*          QueueOut.RSet(0, QueueIn.RGet(0)); */
                /*  Reverse Get */
                /*          function values = GetRange(obj, kbeg, kend) */
                /*              if coder.target('rtw') */
                /*                  values = repmat(obj.ValueType, 1, g_FeedoptConfig.MaxNHorz); */
                /*                  coder.eval('queue_get_range', obj.id, kbeg, kbed, coder.wref(values)); */
                /*              else */
                /*                  values = QueueGetRange(obj.id, kbeg, kend); */
                /*              end */
                /*          end */
                Ncrv = queue_size(QueueIn_id);
                value = queue_get(QueueIn_id, Ncrv - 1);
                queue_push(QueueOut_id, value);
            }
        } else {
            /*  if Ncrv > 1 */
            /*  Forward Get */
            value = queue_get(QueueIn_id, 0);
            queue_push(QueueOut_id, value);
        }
    }
}

/*
 * File trailer for SmoothCurvStructs.c
 *
 * [EOF]
 */
