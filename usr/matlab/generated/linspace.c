/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: linspace.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "linspace.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : int n1
 *                emxArray_real_T *y
 * Return Type  : void
 */
void linspace(int n1, emxArray_real_T *y)
{
    int i;
    double delta1;
    int k;
    if (n1 < 0) {
        n1 = 0;
    }

    i = y->size[0] * y->size[1];
    y->size[0] = 1;
    y->size[1] = n1;
    emxEnsureCapacity_real_T(y, i);
    if (n1 >= 1) {
        y->data[n1 - 1] = 0.0;
        if (y->size[1] >= 2) {
            y->data[0] = -1.0;
            if (y->size[1] >= 3) {
                delta1 = 1.0 / ((double)y->size[1] - 1.0);
                i = y->size[1];
                for (k = 0; k <= i - 3; k++) {
                    y->data[k + 1] = ((double)k + 1.0) * delta1 + -1.0;
                }
            }
        }
    }
}

/*
 * File trailer for linspace.c
 *
 * [EOF]
 */
