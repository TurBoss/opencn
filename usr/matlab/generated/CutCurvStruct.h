/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CutCurvStruct.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CUTCURVSTRUCT_H
#define CUTCURVSTRUCT_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void CutCurvStruct(const CurvStruct *b_CurvStruct, double d0, double d1,
    CurvStruct *CurvStruct1);

#endif

/*
 * File trailer for CutCurvStruct.h
 *
 * [EOF]
 */
