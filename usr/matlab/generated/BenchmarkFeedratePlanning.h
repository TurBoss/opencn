/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: BenchmarkFeedratePlanning.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef BENCHMARKFEEDRATEPLANNING_H
#define BENCHMARKFEEDRATEPLANNING_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void BenchmarkFeedratePlanning(const CurvStruct CurvStructs_data[], const
    int CurvStructs_size[2], int Count, double Coeff_data[], int Coeff_size[2],
    double *TElapsed);

#endif

/*
 * File trailer for BenchmarkFeedratePlanning.h
 *
 * [EOF]
 */
