/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: repmat.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "repmat.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : const double a_data[]
 *                const int a_size[2]
 *                double b_data[]
 *                int b_size[2]
 * Return Type  : void
 */
void b_repmat(const double a_data[], const int a_size[2], double b_data[], int
              b_size[2])
{
    int na;
    int k;
    b_size[0] = 3;
    b_size[1] = (unsigned char)a_size[1];
    if ((unsigned char)a_size[1] != 0) {
        na = a_size[1];
        for (k = 0; k < na; k++) {
            b_data[3 * k] = a_data[k];
            b_data[3 * k + 1] = a_data[k];
            b_data[3 * k + 2] = a_data[k];
        }
    }
}

/*
 * Arguments    : const emxArray_real_T *a
 *                double varargin_2
 *                emxArray_real_T *b
 * Return Type  : void
 */
void c_repmat(const emxArray_real_T *a, double varargin_2, emxArray_real_T *b)
{
    int i;
    int t;
    int na;
    int k;
    i = b->size[0] * b->size[1];
    b->size[0] = a->size[0];
    t = (int)varargin_2;
    b->size[1] = t;
    emxEnsureCapacity_real_T(b, i);
    if ((a->size[0] != 0) && (t != 0)) {
        i = t - 1;
        for (t = 0; t <= i; t++) {
            na = a->size[0];
            for (k = 0; k < na; k++) {
                b->data[k + b->size[0] * t] = a->data[k];
            }
        }
    }
}

/*
 * Arguments    : const double a[3]
 *                double varargin_2
 *                double b_data[]
 *                int b_size[2]
 * Return Type  : void
 */
void repmat(const double a[3], double varargin_2, double b_data[], int b_size[2])
{
    int i;
    int t;
    b_size[0] = 3;
    i = (int)varargin_2;
    t = (unsigned char)i;
    b_size[1] = t;
    if (t != 0) {
        i--;
        for (t = 0; t <= i; t++) {
            b_data[3 * t] = a[0];
            b_data[3 * t + 1] = a[1];
            b_data[3 * t + 2] = a[2];
        }
    }
}

/*
 * File trailer for repmat.c
 *
 * [EOF]
 */
