/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: ResampleTick.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "ResampleTick.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "c_spline.h"
#include "sinspace.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const OptStruct *CurOptStruct
 *                const OptStruct *NextOptStruct
 *                const struct0_T *b_Bl
 *                double *u
 *                double dt
 *                boolean_T *pop_next
 *                boolean_T *finished
 * Return Type  : void
 */
void ResampleTick(const OptStruct *CurOptStruct, const OptStruct *NextOptStruct,
                  const struct0_T *b_Bl, double *u, double dt, boolean_T
                  *pop_next, boolean_T *finished)
{
    double coeffs[120];
    double Trest;
    double X[3];
    double ukp1;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    if (CurOptStruct->CurvStruct.Type == CurveType_None) {
        *u = 0.0;
        *pop_next = false;
        *finished = true;
    } else {
        *finished = false;
        memcpy(&coeffs[0], &CurOptStruct->Coeff[0], 120U * sizeof(double));
        Trest = *u;

        /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
        if (*u < 0.0) {
            printf("ERROR: C_BSPLINE_EVAL: X < 0 (%f)\n", *u);
            fflush(stdout);
            Trest = 0.0;
        } else {
            if (*u > 1.0) {
                printf("ERROR: C_BSPLINE_EVAL: X > 1 (%f)\n", *u);
                fflush(stdout);
                Trest = 1.0;
            }
        }

        c_bspline_eval(&b_Bl->handle, &coeffs[0], Trest, &X[0]);
        Trest = sqrt(X[0]);
        ukp1 = (*u + X[1] * pow(dt, 2.0) / 4.0) + Trest * dt;
        if (ukp1 < *u) {
            printf("!!! ukp1 < uk !!!\n");
            fflush(stdout);
            *finished = true;
            *pop_next = false;
            *u = 0.0;
        } else {
            /*  */
            if (ukp1 < 1.0) {
                *u = ukp1;
                *pop_next = false;
            } else {
                memcpy(&coeffs[0], &CurOptStruct->Coeff[0], 120U * sizeof(double));

                /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
                c_bspline_eval(&b_Bl->handle, &coeffs[0], 1.0, &X[0]);
                Trest = 2.0 * (1.0 - *u) / (sqrt(X[0]) + Trest);
                if (Trest > dt) {
                    Trest = dt;
                    printf("!!! Trest > dt !!!\n");
                    fflush(stdout);
                }

                /*  */
                Trest = dt - Trest;
                if (NextOptStruct->CurvStruct.Type != CurveType_None) {
                    memcpy(&coeffs[0], &NextOptStruct->Coeff[0], 120U * sizeof
                           (double));

                    /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
                    c_bspline_eval(&b_Bl->handle, &coeffs[0], 0.0, &X[0]);
                    ukp1 = X[1] * pow(Trest, 2.0) / 4.0 + sqrt(X[0]) * Trest;
                    if (ukp1 < 0.0) {
                        printf("!!! ukp1 < uk !!!\n");
                        fflush(stdout);
                        *finished = true;
                        *pop_next = false;
                        *u = 0.0;
                    } else {
                        *u = ukp1;
                        *pop_next = true;
                    }
                } else {
                    *finished = true;
                    *pop_next = false;
                    *u = 1.0;
                }
            }
        }
    }
}

/*
 * File trailer for ResampleTick.c
 *
 * [EOF]
 */
