/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: BenchmarkFeedratePlanning.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "FeedoptTypes_initialize.h"
#include "FeedratePlanning_v4.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "c_spline.h"
#include "linspace.h"
#include "sinspace.h"
#include "tic.h"
#include "toc.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const CurvStruct CurvStructs_data[]
 *                const int CurvStructs_size[2]
 *                int Count
 *                double Coeff_data[]
 *                int Coeff_size[2]
 *                double *TElapsed
 * Return Type  : void
 */
void BenchmarkFeedratePlanning(const CurvStruct CurvStructs_data[], const int
    CurvStructs_size[2], int Count, double Coeff_data[], int Coeff_size[2],
    double *TElapsed)
{
    emxArray_real_T *y;
    int i;
    int loop_ub;
    int k;
    emxArray_real_T *r;
    double u_vec_data[200];
    emxArray_real_T *b_BasisVal;
    int Bl_n;
    unsigned long Bl_handle;
    emxArray_real_T *b_BasisValD;
    int i1;
    emxArray_real_T *b_BasisValDD;
    emxArray_real_T *b_BasisIntegr;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    (void)CurvStructs_size;
    emxInit_real_T(&y, 2);
    linspace(g_FeedoptConfig.NDiscr, y);
    i = y->size[0] * y->size[1];
    y->size[0] = 1;
    emxEnsureCapacity_real_T(y, i);
    loop_ub = y->size[1];
    for (i = 0; i < loop_ub; i++) {
        y->data[i] *= 3.1415926535897931;
    }

    i = y->size[1];
    for (k = 0; k < i; k++) {
        y->data[k] = cos(y->data[k]);
    }

    emxInit_real_T(&r, 2);
    i = r->size[0] * r->size[1];
    r->size[0] = 1;
    r->size[1] = y->size[1];
    emxEnsureCapacity_real_T(r, i);
    loop_ub = y->size[1];
    for (i = 0; i < loop_ub; i++) {
        r->data[i] = y->data[i] * 0.5 + 0.5;
    }

    k = y->size[1];
    loop_ub = y->size[1];
    for (i = 0; i < loop_ub; i++) {
        u_vec_data[i] = y->data[i] * 0.5 + 0.5;
    }

    emxFree_real_T(&y);
    emxInit_real_T(&b_BasisVal, 2);
    Bl_n = (g_FeedoptConfig.NBreak + g_FeedoptConfig.SplineDegree) - 2;
    c_bspline_create(&Bl_handle, 0.0, 1.0, g_FeedoptConfig.SplineDegree,
                     g_FeedoptConfig.NBreak);

    /*  n, bspline_n */
    i = b_BasisVal->size[0] * b_BasisVal->size[1];
    b_BasisVal->size[0] = k;
    b_BasisVal->size[1] = Bl_n;
    emxEnsureCapacity_real_T(b_BasisVal, i);
    for (i = 0; i < Bl_n; i++) {
        for (i1 = 0; i1 < k; i1++) {
            b_BasisVal->data[i1 + b_BasisVal->size[0] * i] = 0.0;
        }
    }

    emxInit_real_T(&b_BasisValD, 2);
    i = b_BasisValD->size[0] * b_BasisValD->size[1];
    b_BasisValD->size[0] = r->size[1];
    b_BasisValD->size[1] = Bl_n;
    emxEnsureCapacity_real_T(b_BasisValD, i);
    for (i = 0; i < Bl_n; i++) {
        loop_ub = r->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_BasisValD->data[i1 + b_BasisValD->size[0] * i] = 0.0;
        }
    }

    emxInit_real_T(&b_BasisValDD, 2);
    i = b_BasisValDD->size[0] * b_BasisValDD->size[1];
    b_BasisValDD->size[0] = r->size[1];
    b_BasisValDD->size[1] = Bl_n;
    emxEnsureCapacity_real_T(b_BasisValDD, i);
    for (i = 0; i < Bl_n; i++) {
        loop_ub = r->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_BasisValDD->data[i1 + b_BasisValDD->size[0] * i] = 0.0;
        }
    }

    emxInit_real_T(&b_BasisIntegr, 1);
    i = b_BasisIntegr->size[0];
    b_BasisIntegr->size[0] = Bl_n;
    emxEnsureCapacity_real_T(b_BasisIntegr, i);
    for (i = 0; i < Bl_n; i++) {
        b_BasisIntegr->data[i] = 0.0;
    }

    /* , */
    c_bspline_base_eval(&Bl_handle, k, &u_vec_data[0], &b_BasisVal->data[0],
                        &b_BasisValD->data[0], &b_BasisValDD->data[0],
                        &b_BasisIntegr->data[0]);
    Coeff_size[0] = g_FeedoptConfig.MaxNCoeff;
    Coeff_size[1] = g_FeedoptConfig.NHorz;
    loop_ub = g_FeedoptConfig.NHorz;
    for (i = 0; i < loop_ub; i++) {
        k = g_FeedoptConfig.MaxNCoeff;
        for (i1 = 0; i1 < k; i1++) {
            Coeff_data[i1 + Coeff_size[0] * i] = 0.0;
        }
    }

    tic();
    for (k = 0; k < Count; k++) {
        FeedratePlanning_v4(CurvStructs_data, g_FeedoptConfig.amax,
                            g_FeedoptConfig.jmax, g_FeedoptConfig.v_0,
                            g_FeedoptConfig.at_0, g_FeedoptConfig.v_1,
                            g_FeedoptConfig.at_1, b_BasisVal, b_BasisValD,
                            b_BasisValDD, b_BasisIntegr, Bl_handle, r->data,
                            r->size, g_FeedoptConfig.NHorz, Coeff_data,
                            Coeff_size);
    }

    emxFree_real_T(&r);
    emxFree_real_T(&b_BasisIntegr);
    emxFree_real_T(&b_BasisValDD);
    emxFree_real_T(&b_BasisValD);
    emxFree_real_T(&b_BasisVal);
    *TElapsed = toc();
    c_bspline_destroy(&Bl_handle);
}

/*
 * File trailer for BenchmarkFeedratePlanning.c
 *
 * [EOF]
 */
