/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: bsxfun.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef BSXFUN_H
#define BSXFUN_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void b_bsxfun(const emxArray_real_T *a, const double b_data[], const int
                     b_size[1], emxArray_real_T *c);
extern void bsxfun(const double a_data[], const int a_size[1], const
                   emxArray_real_T *b, emxArray_real_T *c);

#endif

/*
 * File trailer for bsxfun.h
 *
 * [EOF]
 */
