/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CalcFrenet.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CALCFRENET_H
#define CALCFRENET_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void CalcFrenet(const double rD1[3], const double rD2[3], double t[3],
                       double n[3], double *kappa);

#endif

/*
 * File trailer for CalcFrenet.h
 *
 * [EOF]
 */
