/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes_emxutil.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "FeedoptTypes_emxutil.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
static void emxFreeStruct_cell_wrap_3(cell_wrap_3 *pStruct);
static void emxInitStruct_cell_wrap_3(cell_wrap_3 *pStruct);

/* Function Definitions */

/*
 * Arguments    : cell_wrap_3 *pStruct
 * Return Type  : void
 */
static void emxFreeStruct_cell_wrap_3(cell_wrap_3 *pStruct)
{
    emxFree_real_T(&pStruct->f1);
}

/*
 * Arguments    : cell_wrap_3 *pStruct
 * Return Type  : void
 */
static void emxInitStruct_cell_wrap_3(cell_wrap_3 *pStruct)
{
    emxInit_real_T(&pStruct->f1, 2);
}

/*
 * Arguments    : coder_internal_sparse *pStruct
 * Return Type  : void
 */
void c_emxFreeStruct_coder_internal_(coder_internal_sparse *pStruct)
{
    emxFree_real_T(&pStruct->d);
    emxFree_int32_T(&pStruct->colidx);
    emxFree_int32_T(&pStruct->rowidx);
}

/*
 * Arguments    : coder_internal_sparse *pStruct
 * Return Type  : void
 */
void c_emxInitStruct_coder_internal_(coder_internal_sparse *pStruct)
{
    emxInit_real_T(&pStruct->d, 1);
    emxInit_int32_T(&pStruct->colidx, 1);
    emxInit_int32_T(&pStruct->rowidx, 1);
}

/*
 * Arguments    : emxArray_int32_T *emxArray
 *                int oldNumel
 * Return Type  : void
 */
void emxEnsureCapacity_int32_T(emxArray_int32_T *emxArray, int oldNumel)
{
    int newNumel;
    int i;
    void *newData;
    if (oldNumel < 0) {
        oldNumel = 0;
    }

    newNumel = 1;
    for (i = 0; i < emxArray->numDimensions; i++) {
        newNumel *= emxArray->size[i];
    }

    if (newNumel > emxArray->allocatedSize) {
        i = emxArray->allocatedSize;
        if (i < 16) {
            i = 16;
        }

        while (i < newNumel) {
            if (i > 1073741823) {
                i = MAX_int32_T;
            } else {
                i *= 2;
            }
        }

        newData = calloc((unsigned int)i, sizeof(int));
        if (emxArray->data != NULL) {
            memcpy(newData, emxArray->data, sizeof(int) * oldNumel);
            if (emxArray->canFreeData) {
                free(emxArray->data);
            }
        }

        emxArray->data = (int *)newData;
        emxArray->allocatedSize = i;
        emxArray->canFreeData = true;
    }
}

/*
 * Arguments    : emxArray_real_T *emxArray
 *                int oldNumel
 * Return Type  : void
 */
void emxEnsureCapacity_real_T(emxArray_real_T *emxArray, int oldNumel)
{
    int newNumel;
    int i;
    void *newData;
    if (oldNumel < 0) {
        oldNumel = 0;
    }

    newNumel = 1;
    for (i = 0; i < emxArray->numDimensions; i++) {
        newNumel *= emxArray->size[i];
    }

    if (newNumel > emxArray->allocatedSize) {
        i = emxArray->allocatedSize;
        if (i < 16) {
            i = 16;
        }

        while (i < newNumel) {
            if (i > 1073741823) {
                i = MAX_int32_T;
            } else {
                i *= 2;
            }
        }

        newData = calloc((unsigned int)i, sizeof(double));
        if (emxArray->data != NULL) {
            memcpy(newData, emxArray->data, sizeof(double) * oldNumel);
            if (emxArray->canFreeData) {
                free(emxArray->data);
            }
        }

        emxArray->data = (double *)newData;
        emxArray->allocatedSize = i;
        emxArray->canFreeData = true;
    }
}

/*
 * Arguments    : cell_wrap_3 pMatrix[3]
 * Return Type  : void
 */
void emxFreeMatrix_cell_wrap_3(cell_wrap_3 pMatrix[3])
{
    int i;
    for (i = 0; i < 3; i++) {
        emxFreeStruct_cell_wrap_3(&pMatrix[i]);
    }
}

/*
 * Arguments    : emxArray_int32_T **pEmxArray
 * Return Type  : void
 */
void emxFree_int32_T(emxArray_int32_T **pEmxArray)
{
    if (*pEmxArray != (emxArray_int32_T *)NULL) {
        if (((*pEmxArray)->data != (int *)NULL) && (*pEmxArray)->canFreeData) {
            free((*pEmxArray)->data);
        }

        free((*pEmxArray)->size);
        free(*pEmxArray);
        *pEmxArray = (emxArray_int32_T *)NULL;
    }
}

/*
 * Arguments    : emxArray_real_T **pEmxArray
 * Return Type  : void
 */
void emxFree_real_T(emxArray_real_T **pEmxArray)
{
    if (*pEmxArray != (emxArray_real_T *)NULL) {
        if (((*pEmxArray)->data != (double *)NULL) && (*pEmxArray)->canFreeData)
        {
            free((*pEmxArray)->data);
        }

        free((*pEmxArray)->size);
        free(*pEmxArray);
        *pEmxArray = (emxArray_real_T *)NULL;
    }
}

/*
 * Arguments    : cell_wrap_3 pMatrix[3]
 * Return Type  : void
 */
void emxInitMatrix_cell_wrap_3(cell_wrap_3 pMatrix[3])
{
    int i;
    for (i = 0; i < 3; i++) {
        emxInitStruct_cell_wrap_3(&pMatrix[i]);
    }
}

/*
 * Arguments    : emxArray_int32_T **pEmxArray
 *                int numDimensions
 * Return Type  : void
 */
void emxInit_int32_T(emxArray_int32_T **pEmxArray, int numDimensions)
{
    emxArray_int32_T *emxArray;
    int i;
    *pEmxArray = (emxArray_int32_T *)malloc(sizeof(emxArray_int32_T));
    emxArray = *pEmxArray;
    emxArray->data = (int *)NULL;
    emxArray->numDimensions = numDimensions;
    emxArray->size = (int *)malloc(sizeof(int) * numDimensions);
    emxArray->allocatedSize = 0;
    emxArray->canFreeData = true;
    for (i = 0; i < numDimensions; i++) {
        emxArray->size[i] = 0;
    }
}

/*
 * Arguments    : emxArray_real_T **pEmxArray
 *                int numDimensions
 * Return Type  : void
 */
void emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions)
{
    emxArray_real_T *emxArray;
    int i;
    *pEmxArray = (emxArray_real_T *)malloc(sizeof(emxArray_real_T));
    emxArray = *pEmxArray;
    emxArray->data = (double *)NULL;
    emxArray->numDimensions = numDimensions;
    emxArray->size = (int *)malloc(sizeof(int) * numDimensions);
    emxArray->allocatedSize = 0;
    emxArray->canFreeData = true;
    for (i = 0; i < numDimensions; i++) {
        emxArray->size[i] = 0;
    }
}

/*
 * File trailer for FeedoptTypes_emxutil.c
 *
 * [EOF]
 */
