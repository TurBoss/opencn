/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_assert.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "c_assert.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : boolean_T condition
 * Return Type  : boolean_T
 */
boolean_T b_c_assert(boolean_T condition)
{
    int i;
    char message[13];
    if (!condition) {
        for (i = 0; i < 13; i++) {
            message[i] = cv2[i];
        }

        c_assert_(&message[0]);
    }

    return condition;
}

/*
 * Arguments    : boolean_T condition
 * Return Type  : boolean_T
 */
boolean_T c_assert(boolean_T condition)
{
    int i;
    char message[16];
    if (!condition) {
        for (i = 0; i < 16; i++) {
            message[i] = cv1[i];
        }

        c_assert_(&message[0]);
    }

    return condition;
}

/*
 * Arguments    : boolean_T condition
 * Return Type  : void
 */
void c_c_assert(boolean_T condition)
{
    int i;
    char message[41];
    static const char b_message[41] = { 'n', 'o', ' ', 'p', 'o', 's', 'i', 't',
        'i', 'v', 'e', ' ', 's', 'o', 'l', 'u', 't', 'i', 'o', 'n', ' ', 'o',
        'f', ' ', 'p', 'o', 'l', 'y', 'n', 'o', 'm', 'i', 'a', 'l', ' ', 's',
        'y', 's', 't', 'e', 'm' };

    if (!condition) {
        for (i = 0; i < 41; i++) {
            message[i] = b_message[i];
        }

        c_assert_(&message[0]);
    }
}

/*
 * Arguments    : boolean_T condition
 * Return Type  : void
 */
void d_c_assert(boolean_T condition)
{
    int i;
    char message[38];
    static const char b_message[38] = { 'e', 'r', 'r', 'o', 'r', ' ', 'i', 'n',
        ' ', 's', 'o', 'l', 'u', 't', 'i', 'o', 'n', ' ', 'o', 'f', ' ', 'p',
        'o', 'l', 'y', 'n', 'o', 'm', 'i', 'a', 'l', ' ', 's', 'y', 's', 't',
        'e', 'm' };

    if (!condition) {
        for (i = 0; i < 38; i++) {
            message[i] = b_message[i];
        }

        c_assert_(&message[0]);
    }
}

/*
 * File trailer for c_assert.c
 *
 * [EOF]
 */
