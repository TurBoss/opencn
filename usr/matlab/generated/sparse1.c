/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: sparse1.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "sparse1.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : coder_internal_sparse *this
 *                const emxArray_real_T *rhs
 *                const double varargin_1_data[]
 *                const int varargin_1_size[2]
 *                const emxArray_real_T *varargin_2
 * Return Type  : void
 */
void sparse_parenAssign(coder_internal_sparse *this, const emxArray_real_T *rhs,
                        const double varargin_1_data[], const int
                        varargin_1_size[2], const emxArray_real_T *varargin_2)
{
    int sm;
    int sn;
    int rhsIter_idx;
    emxArray_int32_T *rowidxt;
    emxArray_real_T *dt;
    int cidx;
    double d;
    int ridx;
    int i;
    int low_ip1;
    int low_i;
    int overflow;
    boolean_T found;
    int high_i;
    double thisv;
    double rhsv;
    int mid_i;
    int idx;
    sm = varargin_1_size[1];
    sn = varargin_2->size[1];
    rhsIter_idx = 0;
    emxInit_int32_T(&rowidxt, 1);
    emxInit_real_T(&dt, 1);
    for (cidx = 0; cidx < sn; cidx++) {
        d = varargin_2->data[cidx];
        for (ridx = 0; ridx < sm; ridx++) {
            i = (int)d;
            low_ip1 = i - 1;
            if (this->colidx->data[low_ip1] < this->colidx->data[i]) {
                low_i = this->colidx->data[low_ip1] - 1;
                overflow = (int)varargin_1_data[ridx];
                if (overflow < this->rowidx->data[low_i]) {
                    found = false;
                } else {
                    high_i = this->colidx->data[i];
                    low_i = this->colidx->data[low_ip1];
                    low_ip1 = this->colidx->data[low_ip1];
                    while (high_i > low_ip1 + 1) {
                        mid_i = (low_i >> 1) + (high_i >> 1);
                        if (((low_i & 1) == 1) && ((high_i & 1) == 1)) {
                            mid_i++;
                        }

                        if (overflow >= this->rowidx->data[mid_i - 1]) {
                            low_i = mid_i;
                            low_ip1 = mid_i;
                        } else {
                            high_i = mid_i;
                        }
                    }

                    found = (this->rowidx->data[low_i - 1] == overflow);
                }
            } else if (this->colidx->data[low_ip1] == this->colidx->data[i]) {
                low_i = this->colidx->data[low_ip1] - 1;
                found = false;
            } else {
                low_i = 0;
                found = false;
            }

            if (found) {
                thisv = this->d->data[low_i - 1];
            } else {
                thisv = 0.0;
            }

            rhsv = rhs->data[rhsIter_idx];
            rhsIter_idx++;
            if ((thisv != 0.0) || (rhsv != 0.0)) {
                high_i = this->colidx->data[this->colidx->size[0] - 1] - 1;
                if ((thisv != 0.0) && (rhsv != 0.0)) {
                    this->d->data[low_i - 1] = rhsv;
                } else if (thisv == 0.0) {
                    idx = low_i + 1;
                    if (this->colidx->data[this->colidx->size[0] - 1] - 1 ==
                            this->maxnz) {
                        i = rowidxt->size[0];
                        rowidxt->size[0] = this->rowidx->size[0];
                        emxEnsureCapacity_int32_T(rowidxt, i);
                        low_ip1 = this->rowidx->size[0];
                        for (i = 0; i < low_ip1; i++) {
                            rowidxt->data[i] = this->rowidx->data[i];
                        }

                        i = dt->size[0];
                        dt->size[0] = this->d->size[0];
                        emxEnsureCapacity_real_T(dt, i);
                        low_ip1 = this->d->size[0];
                        for (i = 0; i < low_ip1; i++) {
                            dt->data[i] = this->d->data[i];
                        }

                        low_ip1 = this->m & 65535;
                        mid_i = low_ip1 * (this->n >> 16);
                        overflow = mid_i >> 16;
                        if (overflow <= 0) {
                            overflow = 0;
                            if (low_ip1 * (this->n & 65535) > MAX_int32_T
                                    - (mid_i << 16)) {
                                overflow = 1;
                            }
                        }

                        if (overflow == 0) {
                            low_ip1 = this->m * this->n;
                            if (this->colidx->data[this->colidx->size[0] - 1] +
                                    9 <= low_ip1) {
                                low_ip1 = this->colidx->data[this->colidx->size
                                    [0] - 1] + 9;
                            }

                            if (1 >= low_ip1) {
                                low_ip1 = 1;
                            }
                        } else if (1 >= this->colidx->data[this->colidx->size[0]
                                   - 1] + 9) {
                            low_ip1 = 1;
                        } else {
                            low_ip1 = this->colidx->data[this->colidx->size[0] -
                                1] + 9;
                        }

                        i = this->rowidx->size[0];
                        this->rowidx->size[0] = low_ip1;
                        emxEnsureCapacity_int32_T(this->rowidx, i);
                        for (i = 0; i < low_ip1; i++) {
                            this->rowidx->data[i] = 0;
                        }

                        i = this->d->size[0];
                        this->d->size[0] = low_ip1;
                        emxEnsureCapacity_real_T(this->d, i);
                        for (i = 0; i < low_ip1; i++) {
                            this->d->data[i] = 0.0;
                        }

                        this->maxnz = low_ip1;
                        for (mid_i = 0; mid_i < low_i; mid_i++) {
                            this->rowidx->data[mid_i] = rowidxt->data[mid_i];
                            this->d->data[mid_i] = dt->data[mid_i];
                        }

                        for (mid_i = idx; mid_i <= high_i; mid_i++) {
                            this->rowidx->data[mid_i] = rowidxt->data[mid_i - 1];
                            this->d->data[mid_i] = dt->data[mid_i - 1];
                        }

                        this->rowidx->data[low_i] = (int)varargin_1_data[ridx];
                        this->d->data[low_i] = rhsv;
                    } else {
                        low_ip1 = (this->colidx->data[this->colidx->size[0] - 1]
                                   - low_i) - 1;
                        if (low_ip1 > 0) {
                            memmove((void *)&this->rowidx->data[low_i + 1],
                                    (void *)&this->rowidx->data[low_i],
                                    (unsigned int)((size_t)low_ip1 * sizeof(int)));
                            memmove((void *)&this->d->data[low_i + 1], (void *)
                                    &this->d->data[low_i], (unsigned int)
                                    ((size_t)low_ip1 * sizeof(double)));
                        }

                        this->d->data[low_i] = rhsv;
                        this->rowidx->data[low_i] = (int)varargin_1_data[ridx];
                    }

                    i = (int)d + 1;
                    low_ip1 = this->n + 1;
                    for (mid_i = i; mid_i <= low_ip1; mid_i++) {
                        this->colidx->data[mid_i - 1]++;
                    }
                } else {
                    low_ip1 = (this->colidx->data[this->colidx->size[0] - 1] -
                               low_i) - 1;
                    if (low_ip1 > 0) {
                        memmove((void *)&this->rowidx->data[low_i - 1], (void *)
                                &this->rowidx->data[low_i], (unsigned int)
                                ((size_t)low_ip1 * sizeof(int)));
                        memmove((void *)&this->d->data[low_i - 1], (void *)
                                &this->d->data[low_i], (unsigned int)((size_t)
                                 low_ip1 * sizeof(double)));
                    }

                    i++;
                    low_ip1 = this->n + 1;
                    for (mid_i = i; mid_i <= low_ip1; mid_i++) {
                        this->colidx->data[mid_i - 1]--;
                    }
                }
            }
        }
    }

    emxFree_real_T(&dt);
    emxFree_int32_T(&rowidxt);
}

/*
 * Arguments    : int m
 *                int n
 *                int nzmax
 *                emxArray_real_T *s_d
 *                emxArray_int32_T *s_colidx
 *                emxArray_int32_T *s_rowidx
 *                int *s_m
 *                int *s_n
 *                int *s_maxnz
 * Return Type  : void
 */
void sparse_spallocLike(int m, int n, int nzmax, emxArray_real_T *s_d,
                        emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx,
                        int *s_m, int *s_n, int *s_maxnz)
{
    int numalloc;
    int i;
    int c;
    if (nzmax >= 1) {
        numalloc = nzmax;
    } else {
        numalloc = 1;
    }

    i = s_d->size[0];
    s_d->size[0] = numalloc;
    emxEnsureCapacity_real_T(s_d, i);
    for (i = 0; i < numalloc; i++) {
        s_d->data[i] = 0.0;
    }

    i = s_colidx->size[0];
    s_colidx->size[0] = n + 1;
    emxEnsureCapacity_int32_T(s_colidx, i);
    s_colidx->data[0] = 1;
    i = s_rowidx->size[0];
    s_rowidx->size[0] = numalloc;
    emxEnsureCapacity_int32_T(s_rowidx, i);
    for (i = 0; i < numalloc; i++) {
        s_rowidx->data[i] = 0;
    }

    for (c = 0; c < n; c++) {
        s_colidx->data[c + 1] = 1;
    }

    i = s_colidx->size[0];
    for (c = 0; c <= i - 2; c++) {
        s_colidx->data[c] = 1;
    }

    s_colidx->data[s_colidx->size[0] - 1] = 1;
    *s_m = m;
    *s_n = n;
    *s_maxnz = numalloc;
}

/*
 * File trailer for sparse1.c
 *
 * [EOF]
 */
