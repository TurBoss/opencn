/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: mldivide.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "mldivide.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : double A[2][2]
 *                const double B[2]
 *                double Y[2]
 * Return Type  : void
 */
void mldivide(double A[2][2], const double B[2], double Y[2])
{
    int r1;
    int r2;
    double a21;
    if (fabs(A[0][1]) > fabs(A[0][0])) {
        r1 = 1;
        r2 = 0;
    } else {
        r1 = 0;
        r2 = 1;
    }

    a21 = A[0][r2] / A[0][r1];
    Y[1] = (B[r2] - B[r1] * a21) / (A[1][r2] - a21 * A[1][r1]);
    Y[0] = (B[r1] - Y[1] * A[1][r1]) / A[0][r1];
}

/*
 * File trailer for mldivide.c
 *
 * [EOF]
 */
