/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: ConstrLineStruct.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CONSTRLINESTRUCT_H
#define CONSTRLINESTRUCT_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void ConstrLineStruct(const double P0[3], const double P1[3], double
    FeedRate, ZSpdMode b_ZSpdMode, CurvStruct *b_CurvStruct);

#endif

/*
 * File trailer for ConstrLineStruct.h
 *
 * [EOF]
 */
