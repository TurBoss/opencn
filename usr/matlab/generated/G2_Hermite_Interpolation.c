/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: G2_Hermite_Interpolation.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "G2_Hermite_Interpolation.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcAlpha0.h"
#include "CalcFrenet.h"
#include "CalcTransition.h"
#include "Calc_beta0_beta1.h"
#include "Calc_u_v4.h"
#include "CharPolyAlpha1.h"
#include "CoefPolySys.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCostIntegral.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_assert.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "find.h"
#include "mldivide.h"
#include "sinspace.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * compute Frenet frame
 * Arguments    : const double r0D0[3]
 *                const double r0D1[3]
 *                const double r0D2[3]
 *                const double r1D0[3]
 *                const double r1D1[3]
 *                const double r1D2[3]
 *                double p5_3D[6][3]
 * Return Type  : void
 */
void G2_Hermite_Interpolation(const double r0D0[3], const double r0D1[3], const
    double r0D2[3], const double r1D0[3], const double r1D1[3], const double
    r1D2[3], double p5_3D[6][3])
{
    double t0[3];
    double n0[3];
    double kappa0;
    double t1[3];
    double n1[3];
    double kappa1;
    double CoefPS[16];
    creal_T alpha0_v[3];
    double b_CoefPS[2][2];
    creal_T alpha1_v[9];
    double dv[10];
    int partialTrueCount;
    double c_CoefPS[2];
    double dv1[10];
    double dv2[4];
    double X[2];
    int N;
    int k;
    double alpha0;
    double alpha1;
    int alpha1_t_size[1];
    boolean_T unnamed_idx_2;
    boolean_T alpha0_t_data[9];
    double a;
    double b_a;
    char message[37];
    static const char b_message[37] = { 'n', 'o', ' ', 'p', 'o', 's', 'i', 't',
        'i', 'v', 'e', ' ', 's', 'o', 'l', 'u', 't', 'i', 'o', 'n', ' ', 'o',
        'f', ' ', 'l', 'i', 'n', 'e', 'a', 'r', ' ', 's', 'y', 's', 't', 'e',
        'm' };

    boolean_T unnamed_idx_0;
    double alpha1_t_data[9];
    double b_alpha0_t_data[9];
    int alpha0_t_size[1];
    int b_alpha0_t_size[1];
    boolean_T unnamed_idx_1;
    double c_a;
    double ex;
    int tmp_data[9];
    double b_r0D0[6][3];
    static const signed char b[6] = { -6, 15, -10, 0, 0, 1 };

    int Idx_size_idx_0;
    double b_alpha0[6][3];
    static const signed char b_b[6] = { -3, 8, -6, 0, 1, 0 };

    signed char b_tmp_data[3];
    static const signed char c_b[6] = { 6, -15, 10, 0, 0, 0 };

    double Idx_data[9];
    double c_r0D0[6][3];
    static const double d_b[6] = { -0.5, 1.5, -1.5, 0.5, 0.0, 0.0 };

    static const double e_b[6] = { 0.5, -1.0, 0.5, 0.0, 0.0, 0.0 };

    double CostInt_data[9];
    static const signed char f_b[6] = { -3, 7, -4, 0, 0, 0 };

    double beta0_u_data[9];
    double beta1_u_data[9];
    boolean_T b_alpha1_t_data[3];

    /*  */
    /*      This file is part of the Optimal G^2 Hermite Interpolation Software. */
    /*  */
    /*      Copyright (C) 2017-2019 Raoul Herzog, Philippe Blanc */
    /*                              mecatronYx group at HEIG-VD */
    /*                              University of Applied Sciences Western Switzerland */
    /*                              CH-1401 Yverdon-les-Bains */
    /*                              All rights reserved. */
    /*  */
    /*      This is free software; you can redistribute it and/or */
    /*      modify it under the terms of the GNU Lesser General Public */
    /*      License as published by the Free Software Foundation; either */
    /*      version 3 of the License, or (at your option) any later version. */
    /*  */
    /*      This software is distributed in the hope that it will be useful, */
    /*      but WITHOUT ANY WARRANTY; without even the implied warranty of */
    /*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU */
    /*      Lesser General Public License for more details. */
    /*  */
    /*      You should have received a copy of the GNU Lesser General Public */
    /*      License along with this software; if not, write to the Free Software */
    /*      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA */
    /*  */
    /*  */
    /*  [p5_3D, alpha0, alpha1] = G2_Hermite_Interpolation(r0D0, r0D1, r0D2, r1D0, r1D1, r1D2) */
    /*  */
    /*  Compute an optimal trajectory in R^3, connecting the point r0 to r1 with */
    /*  C^2 smoothness while minimizing the integral of the norm of the third */
    /*  derivative.  */
    CalcFrenet(r0D1, r0D2, t0, n0, &kappa0);
    CalcFrenet(r1D1, r1D2, t1, n1, &kappa1);

    /*  reduce to polynomial system of 2 equations in unknowns alpha0 and alpha1 */
    /*  p1   = (a1*alpha1+a0)*alpha0^2 + (b1*alpha1+b0)*alpha0 +  */
    /*          c3*alpha1^3+c2*alpha1^2+c1*alpha1+c0; */
    /*  p2   = (d1*alpha0+d0)*alpha1^2 + (e1*alpha0+e0)*alpha1 +  */
    /*          f3*alpha0^3+f2*alpha0^2+f1*alpha0+f0; */
    /*  */
    /*  compute CoefPS = [a1 a0 b1 b0 c3 c2 c1 c0 d1 d0 e1 e0 f3 f2 f1 f0] */
    CoefPolySys(r0D0, t0, n0, kappa0, r1D0, t1, n1, kappa1, CoefPS);

    /*  */
    /*  */
    if ((kappa0 == 0.0) && (kappa1 == 0.0)) {
        /*  degenerated case where the polynomial system degenerates to a linear one */
        /*         */
        /*  */
        b_CoefPS[0][0] = CoefPS[3];
        b_CoefPS[1][0] = CoefPS[6];
        b_CoefPS[0][1] = CoefPS[14];
        b_CoefPS[1][1] = CoefPS[11];
        c_CoefPS[0] = -CoefPS[7];
        c_CoefPS[1] = -CoefPS[15];
        mldivide(b_CoefPS, c_CoefPS, X);

        /*  resolution of linear system */
        alpha0 = X[0];
        alpha1 = X[1];
        if ((X[0] <= 0.0) || (X[1] <= 0.0)) {
            for (partialTrueCount = 0; partialTrueCount < 37; partialTrueCount++)
            {
                message[partialTrueCount] = b_message[partialTrueCount];
            }

            c_assert_(&message[0]);
        }

        /*  */
        Calc_beta0_beta1(X[0], X[1], r0D0, t0, n0, kappa0, r1D0, t1, n1, kappa1,
                         &a, &b_a);
    } else if (kappa0 == 0.0) {
        /*  compute resultant of the polynomial system */
        /*  */
        alpha0_v[0].re = 0.0;
        alpha0_v[0].im = 0.0;
        alpha0_v[1].re = 0.0;
        alpha0_v[1].im = 0.0;
        alpha0_v[2].re = 0.0;
        alpha0_v[2].im = 0.0;
        dv2[0] = CoefPS[3] * CoefPS[15] - CoefPS[7] * CoefPS[14];
        dv2[1] = (CoefPS[3] * CoefPS[11] + CoefPS[2] * CoefPS[15]) - CoefPS[6] *
            CoefPS[14];
        dv2[2] = (CoefPS[3] * CoefPS[9] + CoefPS[2] * CoefPS[11]) - CoefPS[5] *
            CoefPS[14];
        dv2[3] = CoefPS[2] * CoefPS[9] - CoefPS[4] * CoefPS[14];
        c_roots(&dv2[0], &alpha0_v[0], 4);

        /*  all roots of 3th degree polynomial in alpha1 */
        N = 0;
        unnamed_idx_2 = (fabs(alpha0_v[0].im) < 1.0E-11);
        unnamed_idx_0 = unnamed_idx_2;
        if (unnamed_idx_2 && (alpha0_v[0].re > 0.0)) {
            N = 1;
        }

        unnamed_idx_2 = (fabs(alpha0_v[1].im) < 1.0E-11);
        unnamed_idx_1 = unnamed_idx_2;
        if (unnamed_idx_2 && (alpha0_v[1].re > 0.0)) {
            N++;
        }

        unnamed_idx_2 = (fabs(alpha0_v[2].im) < 1.0E-11);
        if (unnamed_idx_2 && (alpha0_v[2].re > 0.0)) {
            N++;
        }

        partialTrueCount = 0;
        if (unnamed_idx_0 && (alpha0_v[0].re > 0.0)) {
            b_tmp_data[0] = 1;
            partialTrueCount = 1;
        }

        if (unnamed_idx_1 && (alpha0_v[1].re > 0.0)) {
            b_tmp_data[partialTrueCount] = 2;
            partialTrueCount++;
        }

        if (unnamed_idx_2 && (alpha0_v[2].re > 0.0)) {
            b_tmp_data[partialTrueCount] = 3;
        }

        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            alpha1_t_data[partialTrueCount] =
                alpha0_v[b_tmp_data[partialTrueCount] - 1].re;
        }

        /*  retain only positive real roots */
        if ((fabs(CoefPS[2]) < 1.0E-11) && (fabs(CoefPS[3]) < 1.0E-11)) {
            alpha0_t_size[0] = N;
            for (k = 0; k < N; k++) {
                b_alpha0_t_data[k] = pow(alpha1_t_data[k], 2.0);
            }

            for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++)
            {
                b_alpha0_t_data[partialTrueCount] = -((CoefPS[9] *
                    b_alpha0_t_data[partialTrueCount] + CoefPS[11] *
                    alpha1_t_data[partialTrueCount]) + CoefPS[15]) / CoefPS[14];
            }
        } else {
            alpha0_t_size[0] = N;
            for (k = 0; k < N; k++) {
                b_alpha0_t_data[k] = pow(alpha1_t_data[k], 3.0);
            }

            for (k = 0; k < N; k++) {
                Idx_data[k] = pow(alpha1_t_data[k], 2.0);
            }

            for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++)
            {
                b_alpha0_t_data[partialTrueCount] = -(((CoefPS[4] *
                    b_alpha0_t_data[partialTrueCount] + CoefPS[5] *
                    Idx_data[partialTrueCount]) + CoefPS[6] *
                    alpha1_t_data[partialTrueCount]) + CoefPS[7]) / (CoefPS[2] *
                    alpha1_t_data[partialTrueCount] + CoefPS[3]);
            }
        }

        b_alpha0_t_size[0] = alpha0_t_size[0];
        N = alpha0_t_size[0];
        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            b_alpha1_t_data[partialTrueCount] =
                (b_alpha0_t_data[partialTrueCount] > 0.0);
        }

        eml_find(b_alpha1_t_data, b_alpha0_t_size, tmp_data, alpha1_t_size);
        Idx_size_idx_0 = alpha1_t_size[0];
        N = alpha1_t_size[0];
        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            Idx_data[partialTrueCount] = tmp_data[partialTrueCount];
        }

        /*  */
        c_c_assert(alpha1_t_size[0] > 0);
        if (alpha1_t_size[0] > 1) {
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&CostInt_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&beta0_u_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&beta1_u_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            for (k = 0; k < Idx_size_idx_0; k++) {
                Calc_beta0_beta1(b_alpha0_t_data[(int)Idx_data[k] - 1],
                                 alpha1_t_data[(int)Idx_data[k] - 1], r0D0, t0,
                                 n0, kappa0, r1D0, t1, n1, kappa1,
                                 &beta0_u_data[k], &beta1_u_data[k]);
                N = (int)Idx_data[k] - 1;
                CostInt_data[k] = EvalCostIntegral(b_alpha0_t_data[N],
                    beta0_u_data[k], alpha1_t_data[N], beta1_u_data[k], r0D0, t0,
                    n0, kappa0, r1D0, t1, n1, kappa1);
            }

            if (alpha1_t_size[0] <= 2) {
                partialTrueCount = (CostInt_data[0] > CostInt_data[1]);
            } else {
                ex = CostInt_data[0];
                partialTrueCount = 0;
                for (k = 2; k <= Idx_size_idx_0; k++) {
                    c_a = CostInt_data[k - 1];
                    if (ex > c_a) {
                        ex = c_a;
                        partialTrueCount = k - 1;
                    }
                }
            }

            N = (int)Idx_data[partialTrueCount] - 1;
            alpha0 = b_alpha0_t_data[N];
            alpha1 = alpha1_t_data[N];
            a = beta0_u_data[partialTrueCount];
            b_a = beta1_u_data[partialTrueCount];
        } else {
            N = (int)Idx_data[0] - 1;
            alpha0 = b_alpha0_t_data[N];
            alpha1 = alpha1_t_data[N];
            Calc_beta0_beta1(b_alpha0_t_data[(int)Idx_data[0] - 1],
                             alpha1_t_data[(int)Idx_data[0] - 1], r0D0, t0, n0,
                             kappa0, r1D0, t1, n1, kappa1, &a, &b_a);
        }
    } else if (kappa1 == 0.0) {
        /*  */
        /*  compute resultant of the polynomial system */
        /*  */
        alpha0_v[0].re = 0.0;
        alpha0_v[0].im = 0.0;
        alpha0_v[1].re = 0.0;
        alpha0_v[1].im = 0.0;
        alpha0_v[2].re = 0.0;
        alpha0_v[2].im = 0.0;
        dv2[0] = CoefPS[6] * CoefPS[15] - CoefPS[7] * CoefPS[11];
        dv2[1] = (CoefPS[6] * CoefPS[14] - CoefPS[7] * CoefPS[10]) - CoefPS[3] *
            CoefPS[11];
        dv2[2] = (CoefPS[6] * CoefPS[13] - CoefPS[3] * CoefPS[10]) - CoefPS[1] *
            CoefPS[11];
        dv2[3] = CoefPS[6] * CoefPS[12] - CoefPS[1] * CoefPS[10];
        c_roots(&dv2[0], &alpha0_v[0], 4);

        /*  all roots of 3th degree polynomial in alpha0 */
        N = 0;
        unnamed_idx_2 = (fabs(alpha0_v[0].im) < 1.0E-11);
        unnamed_idx_0 = unnamed_idx_2;
        if (unnamed_idx_2 && (alpha0_v[0].re > 0.0)) {
            N = 1;
        }

        unnamed_idx_2 = (fabs(alpha0_v[1].im) < 1.0E-11);
        unnamed_idx_1 = unnamed_idx_2;
        if (unnamed_idx_2 && (alpha0_v[1].re > 0.0)) {
            N++;
        }

        unnamed_idx_2 = (fabs(alpha0_v[2].im) < 1.0E-11);
        if (unnamed_idx_2 && (alpha0_v[2].re > 0.0)) {
            N++;
        }

        partialTrueCount = 0;
        if (unnamed_idx_0 && (alpha0_v[0].re > 0.0)) {
            b_tmp_data[0] = 1;
            partialTrueCount = 1;
        }

        if (unnamed_idx_1 && (alpha0_v[1].re > 0.0)) {
            b_tmp_data[partialTrueCount] = 2;
            partialTrueCount++;
        }

        if (unnamed_idx_2 && (alpha0_v[2].re > 0.0)) {
            b_tmp_data[partialTrueCount] = 3;
        }

        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            b_alpha0_t_data[partialTrueCount] =
                alpha0_v[b_tmp_data[partialTrueCount] - 1].re;
        }

        /*  retain only positive real roots */
        if ((fabs(CoefPS[10]) < 1.0E-11) && (fabs(CoefPS[11]) < 1.0E-11)) {
            alpha1_t_size[0] = N;
            for (k = 0; k < N; k++) {
                alpha1_t_data[k] = pow(b_alpha0_t_data[k], 2.0);
            }

            for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++)
            {
                alpha1_t_data[partialTrueCount] = -((CoefPS[1] *
                    alpha1_t_data[partialTrueCount] + CoefPS[3] *
                    b_alpha0_t_data[partialTrueCount]) + CoefPS[7]) / CoefPS[6];
            }
        } else {
            alpha1_t_size[0] = N;
            for (k = 0; k < N; k++) {
                alpha1_t_data[k] = pow(b_alpha0_t_data[k], 3.0);
            }

            for (k = 0; k < N; k++) {
                Idx_data[k] = pow(b_alpha0_t_data[k], 2.0);
            }

            for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++)
            {
                alpha1_t_data[partialTrueCount] = -(((CoefPS[12] *
                    alpha1_t_data[partialTrueCount] + CoefPS[13] *
                    Idx_data[partialTrueCount]) + CoefPS[14] *
                    b_alpha0_t_data[partialTrueCount]) + CoefPS[15]) / (CoefPS
                    [10] * b_alpha0_t_data[partialTrueCount] + CoefPS[11]);
            }
        }

        b_alpha0_t_size[0] = alpha1_t_size[0];
        N = alpha1_t_size[0];
        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            b_alpha1_t_data[partialTrueCount] = (alpha1_t_data[partialTrueCount]
                > 0.0);
        }

        eml_find(b_alpha1_t_data, b_alpha0_t_size, tmp_data, alpha1_t_size);
        Idx_size_idx_0 = alpha1_t_size[0];
        N = alpha1_t_size[0];
        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            Idx_data[partialTrueCount] = tmp_data[partialTrueCount];
        }

        /*  */
        c_c_assert(alpha1_t_size[0] > 0);
        if (alpha1_t_size[0] > 1) {
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&CostInt_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&beta0_u_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&beta1_u_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            for (k = 0; k < Idx_size_idx_0; k++) {
                Calc_beta0_beta1(b_alpha0_t_data[(int)Idx_data[k] - 1],
                                 alpha1_t_data[(int)Idx_data[k] - 1], r0D0, t0,
                                 n0, kappa0, r1D0, t1, n1, kappa1,
                                 &beta0_u_data[k], &beta1_u_data[k]);
                N = (int)Idx_data[k] - 1;
                CostInt_data[k] = EvalCostIntegral(b_alpha0_t_data[N],
                    beta0_u_data[k], alpha1_t_data[N], beta1_u_data[k], r0D0, t0,
                    n0, kappa0, r1D0, t1, n1, kappa1);
            }

            if (alpha1_t_size[0] <= 2) {
                partialTrueCount = (CostInt_data[0] > CostInt_data[1]);
            } else {
                ex = CostInt_data[0];
                partialTrueCount = 0;
                for (k = 2; k <= Idx_size_idx_0; k++) {
                    c_a = CostInt_data[k - 1];
                    if (ex > c_a) {
                        ex = c_a;
                        partialTrueCount = k - 1;
                    }
                }
            }

            N = (int)Idx_data[partialTrueCount] - 1;
            alpha0 = b_alpha0_t_data[N];
            alpha1 = alpha1_t_data[N];
            a = beta0_u_data[partialTrueCount];
            b_a = beta1_u_data[partialTrueCount];
        } else {
            N = (int)Idx_data[0] - 1;
            alpha0 = b_alpha0_t_data[N];
            alpha1 = alpha1_t_data[N];
            Calc_beta0_beta1(b_alpha0_t_data[(int)Idx_data[0] - 1],
                             alpha1_t_data[(int)Idx_data[0] - 1], r0D0, t0, n0,
                             kappa0, r1D0, t1, n1, kappa1, &a, &b_a);
        }

        /*      */
    } else {
        /*  compute resultant of the polynomial system */
        /*  */
        memset(&alpha1_v[0], 0, 9U * sizeof(creal_T));
        CharPolyAlpha1(CoefPS, dv);
        for (partialTrueCount = 0; partialTrueCount < 10; partialTrueCount++) {
            dv1[partialTrueCount] = dv[9 - partialTrueCount];
        }

        c_roots(&dv1[0], &alpha1_v[0], 10);

        /*  all roots of 9th degree polynomial in alpha1 */
        N = 0;
        for (k = 0; k < 9; k++) {
            unnamed_idx_2 = (fabs(alpha1_v[k].im) < 1.0E-11);
            alpha0_t_data[k] = unnamed_idx_2;
            if (unnamed_idx_2 && (alpha1_v[k].re > 0.0)) {
                N++;
            }
        }

        alpha1_t_size[0] = N;
        partialTrueCount = 0;
        for (N = 0; N < 9; N++) {
            if (alpha0_t_data[N] && (alpha1_v[N].re > 0.0)) {
                alpha1_t_data[partialTrueCount] = alpha1_v[N].re;
                partialTrueCount++;
            }
        }

        /*  retain only positive real roots */
        /*  compute corresponding values of alpha0 */
        CalcAlpha0(alpha1_t_data, alpha1_t_size, CoefPS, b_alpha0_t_data,
                   alpha0_t_size);

        /*  */
        b_alpha0_t_size[0] = alpha0_t_size[0];
        N = alpha0_t_size[0];
        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            alpha0_t_data[partialTrueCount] = (b_alpha0_t_data[partialTrueCount]
                > 0.0);
        }

        eml_find(alpha0_t_data, b_alpha0_t_size, tmp_data, alpha1_t_size);
        Idx_size_idx_0 = alpha1_t_size[0];
        N = alpha1_t_size[0];
        for (partialTrueCount = 0; partialTrueCount < N; partialTrueCount++) {
            Idx_data[partialTrueCount] = tmp_data[partialTrueCount];
        }

        /*  */
        c_c_assert(alpha1_t_size[0] > 0);
        if (alpha1_t_size[0] > 1) {
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&CostInt_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&beta0_u_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            if (0 <= Idx_size_idx_0 - 1) {
                memset(&beta1_u_data[0], 0, Idx_size_idx_0 * sizeof(double));
            }

            /*  preallocating */
            for (k = 0; k < Idx_size_idx_0; k++) {
                Calc_beta0_beta1(b_alpha0_t_data[(int)Idx_data[k] - 1],
                                 alpha1_t_data[(int)Idx_data[k] - 1], r0D0, t0,
                                 n0, kappa0, r1D0, t1, n1, kappa1,
                                 &beta0_u_data[k], &beta1_u_data[k]);
                N = (int)Idx_data[k] - 1;
                CostInt_data[k] = EvalCostIntegral(b_alpha0_t_data[N],
                    beta0_u_data[k], alpha1_t_data[N], beta1_u_data[k], r0D0, t0,
                    n0, kappa0, r1D0, t1, n1, kappa1);
            }

            if (alpha1_t_size[0] <= 2) {
                partialTrueCount = (CostInt_data[0] > CostInt_data[1]);
            } else {
                ex = CostInt_data[0];
                partialTrueCount = 0;
                for (k = 2; k <= Idx_size_idx_0; k++) {
                    c_a = CostInt_data[k - 1];
                    if (ex > c_a) {
                        ex = c_a;
                        partialTrueCount = k - 1;
                    }
                }
            }

            N = (int)Idx_data[partialTrueCount] - 1;
            alpha0 = b_alpha0_t_data[N];
            alpha1 = alpha1_t_data[N];
            a = beta0_u_data[partialTrueCount];
            b_a = beta1_u_data[partialTrueCount];
        } else {
            N = (int)Idx_data[0] - 1;
            alpha0 = b_alpha0_t_data[N];
            alpha1 = alpha1_t_data[N];
            Calc_beta0_beta1(b_alpha0_t_data[(int)Idx_data[0] - 1],
                             alpha1_t_data[(int)Idx_data[0] - 1], r0D0, t0, n0,
                             kappa0, r1D0, t1, n1, kappa1, &a, &b_a);
        }
    }

    /*  */
    /*  Hermite basis  */
    /*  evaluate coefficients as sum of basis functions */
    c_a = kappa0 * pow(alpha0, 2.0);
    ex = kappa1 * pow(alpha1, 2.0);
    for (partialTrueCount = 0; partialTrueCount < 6; partialTrueCount++) {
        b_r0D0[partialTrueCount][0] = r0D0[0] * (double)b[partialTrueCount];
        b_alpha0[partialTrueCount][0] = alpha0 * t0[0] * (double)
            b_b[partialTrueCount];
        b_r0D0[partialTrueCount][1] = r0D0[1] * (double)b[partialTrueCount];
        b_alpha0[partialTrueCount][1] = alpha0 * t0[1] * (double)
            b_b[partialTrueCount];
        b_r0D0[partialTrueCount][2] = r0D0[2] * (double)b[partialTrueCount];
        b_alpha0[partialTrueCount][2] = alpha0 * t0[2] * (double)
            b_b[partialTrueCount];
    }

    for (partialTrueCount = 0; partialTrueCount < 3; partialTrueCount++) {
        t0[partialTrueCount] = a * t0[partialTrueCount] + c_a *
            n0[partialTrueCount];
        for (N = 0; N < 6; N++) {
            c_r0D0[N][partialTrueCount] = (b_r0D0[N][partialTrueCount] +
                b_alpha0[N][partialTrueCount]) + t0[partialTrueCount] * d_b[N];
        }
    }

    for (partialTrueCount = 0; partialTrueCount < 6; partialTrueCount++) {
        b_r0D0[partialTrueCount][0] = r1D0[0] * (double)c_b[partialTrueCount];
        b_r0D0[partialTrueCount][1] = r1D0[1] * (double)c_b[partialTrueCount];
        b_r0D0[partialTrueCount][2] = r1D0[2] * (double)c_b[partialTrueCount];
    }

    for (partialTrueCount = 0; partialTrueCount < 3; partialTrueCount++) {
        n1[partialTrueCount] = b_a * t1[partialTrueCount] + ex *
            n1[partialTrueCount];
        for (N = 0; N < 6; N++) {
            p5_3D[N][partialTrueCount] = (c_r0D0[N][partialTrueCount] + b_r0D0[N]
                [partialTrueCount]) + alpha1 * t1[partialTrueCount] * (double)
                f_b[N];
        }
    }

    for (partialTrueCount = 0; partialTrueCount < 6; partialTrueCount++) {
        p5_3D[partialTrueCount][0] += n1[0] * e_b[partialTrueCount];
        p5_3D[partialTrueCount][1] += n1[1] * e_b[partialTrueCount];
        p5_3D[partialTrueCount][2] += n1[2] * e_b[partialTrueCount];
    }

    /*  last cross check ... */
    /*  */
    d_c_assert(fabs((((((CoefPS[0] * alpha1 + CoefPS[1]) * pow(alpha0, 2.0) +
                        (CoefPS[2] * alpha1 + CoefPS[3]) * alpha0) + CoefPS[4] *
                       pow(alpha1, 3.0)) + CoefPS[5] * pow(alpha1, 2.0)) +
                     CoefPS[6] * alpha1) + CoefPS[7]) < 1.0E-7);
    d_c_assert(fabs((((((CoefPS[8] * alpha0 + CoefPS[9]) * pow(alpha1, 2.0) +
                        (CoefPS[10] * alpha0 + CoefPS[11]) * alpha1) + CoefPS[12]
                       * pow(alpha0, 3.0)) + CoefPS[13] * pow(alpha0, 2.0)) +
                     CoefPS[14] * alpha0) + CoefPS[15]) < 1.0E-7);
}

/*
 * File trailer for G2_Hermite_Interpolation.c
 *
 * [EOF]
 */
