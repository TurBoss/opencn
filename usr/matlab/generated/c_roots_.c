/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_roots_.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "c_roots_.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "sinspace.h"
#include <string.h>

/* Function Declarations */
static int div_s32_floor(int numerator, int denominator);

/* Function Definitions */

/*
 * Arguments    : int numerator
 *                int denominator
 * Return Type  : int
 */
static int div_s32_floor(int numerator, int denominator)
{
    int quotient;
    unsigned int absNumerator;
    unsigned int absDenominator;
    boolean_T quotientNeedsNegation;
    unsigned int tempAbsQuotient;
    if (denominator == 0) {
        if (numerator >= 0) {
            quotient = MAX_int32_T;
        } else {
            quotient = MIN_int32_T;
        }
    } else {
        if (numerator < 0) {
            absNumerator = ~(unsigned int)numerator + 1U;
        } else {
            absNumerator = (unsigned int)numerator;
        }

        if (denominator < 0) {
            absDenominator = ~(unsigned int)denominator + 1U;
        } else {
            absDenominator = (unsigned int)denominator;
        }

        quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
        tempAbsQuotient = absNumerator / absDenominator;
        if (quotientNeedsNegation) {
            absNumerator %= absDenominator;
            if (absNumerator > 0U) {
                tempAbsQuotient++;
            }

            quotient = -(int)tempAbsQuotient;
        } else {
            quotient = (int)tempAbsQuotient;
        }
    }

    return quotient;
}

/*
 * Arguments    : const double coeffs_data[]
 *                const int coeffs_size[2]
 *                creal_T Y_data[]
 *                int Y_size[1]
 * Return Type  : void
 */
void c_roots_(const double coeffs_data[], const int coeffs_size[2], creal_T
              Y_data[], int Y_size[1])
{
    int loop_ub;
    int i;
    int i1;
    int i2;
    double tmp_data[13];
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    Y_size[0] = coeffs_size[1] - 1;
    loop_ub = coeffs_size[1] - 1;
    if (0 <= loop_ub - 1) {
        memset(&Y_data[0], 0, loop_ub * sizeof(creal_T));
    }

    if (1 > coeffs_size[1]) {
        i = 0;
        i1 = 1;
        i2 = -1;
    } else {
        i = coeffs_size[1] - 1;
        i1 = -1;
        i2 = 0;
    }

    loop_ub = div_s32_floor(i2 - i, i1);
    for (i2 = 0; i2 <= loop_ub; i2++) {
        tmp_data[i2] = coeffs_data[i + i1 * i2];
    }

    c_roots(&tmp_data[0], &Y_data[0], coeffs_size[1]);
}

/*
 * File trailer for c_roots_.c
 *
 * [EOF]
 */
