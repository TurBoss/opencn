/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: BuildConstrJerk_v4.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef BUILDCONSTRJERK_V4_H
#define BUILDCONSTRJERK_V4_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void BuildConstrJerk_v4(const CurvStruct CurvStructs_data[], const int
    CurvStructs_size[2], const double Coeff_data[], const int Coeff_size[2],
    const double jmax[3], const emxArray_real_T *b_BasisVal, const
    emxArray_real_T *b_BasisValD, const emxArray_real_T *b_BasisValDD, const
    double u_vec_data[], const int u_vec_size[2], coder_internal_sparse *A,
    double b_data[], int b_size[1]);

#endif

/*
 * File trailer for BuildConstrJerk_v4.h
 *
 * [EOF]
 */
