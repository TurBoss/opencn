/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalHelix.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "EvalHelix.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_assert.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "repmat.h"
#include "sinspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 *                const double u_vec_data[]
 *                const int u_vec_size[2]
 *                double r0D_data[]
 *                int r0D_size[2]
 *                double r1D_data[]
 *                int r1D_size[2]
 *                double r2D_data[]
 *                int r2D_size[2]
 *                double r3D_data[]
 *                int r3D_size[2]
 * Return Type  : void
 */
void EvalHelix(const double CurvStruct_P0[3], const double CurvStruct_P1[3],
               const double CurvStruct_evec[3], double CurvStruct_theta, double
               CurvStruct_pitch, const double u_vec_data[], const int
               u_vec_size[2], double r0D_data[], int r0D_size[2], double
               r1D_data[], int r1D_size[2], double r2D_data[], int r2D_size[2],
               double r3D_data[], int r3D_size[2])
{
    double P0P1[3];
    double EcrP0P1[3];
    int loop_ub;
    int i;
    emxArray_real_T *y;
    emxArray_real_T *x;
    double Sign;
    double a;
    double d;
    int phi_vec_size[2];
    double phi_vec_data[200];
    int k;
    int sphi_size[2];
    double sphi_data[200];
    double cphiTEcrCP0_data[600];
    int cphiTEcrCP0_size[2];
    double sphiTEcrCP0_data[600];
    int sphiTEcrCP0_size[2];
    double b_x[3];
    double tmp_data[600];

    /*  */
    P0P1[0] = CurvStruct_P1[0] - CurvStruct_P0[0];
    P0P1[1] = CurvStruct_P1[1] - CurvStruct_P0[1];
    P0P1[2] = CurvStruct_P1[2] - CurvStruct_P0[2];
    EcrP0P1[0] = CurvStruct_evec[1] * P0P1[2] - CurvStruct_evec[2] * P0P1[1];
    EcrP0P1[1] = CurvStruct_evec[2] * P0P1[0] - CurvStruct_evec[0] * P0P1[2];
    EcrP0P1[2] = CurvStruct_evec[0] * P0P1[1] - CurvStruct_evec[1] * P0P1[0];
    r0D_size[0] = 3;
    r0D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r0D_data[3 * i] = 0.0;
        r0D_data[3 * i + 1] = 0.0;
        r0D_data[3 * i + 2] = 0.0;
    }

    r1D_size[0] = 3;
    r1D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r1D_data[3 * i] = 0.0;
        r1D_data[3 * i + 1] = 0.0;
        r1D_data[3 * i + 2] = 0.0;
    }

    r2D_size[0] = 3;
    r2D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r2D_data[3 * i] = 0.0;
        r2D_data[3 * i + 1] = 0.0;
        r2D_data[3 * i + 2] = 0.0;
    }

    r3D_size[0] = 3;
    r3D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r3D_data[3 * i] = 0.0;
        r3D_data[3 * i + 1] = 0.0;
        r3D_data[3 * i + 2] = 0.0;
    }

    /*  */
    if (c_assert(sqrt((pow(EcrP0P1[0], 2.0) + pow(EcrP0P1[1], 2.0)) + pow
                      (EcrP0P1[2], 2.0)) > 2.2204460492503131E-16)) {
        /*  */
        emxInit_real_T(&y, 2);
        emxInit_real_T(&x, 2);
        if ((CurvStruct_pitch == 0.0) || b_c_assert((CurvStruct_evec[0] * P0P1[0]
              + CurvStruct_evec[1] * P0P1[1]) + CurvStruct_evec[2] * P0P1[2] >
                2.2204460492503131E-16)) {
            /*  */
            Sign = (P0P1[0] * CurvStruct_evec[0] + P0P1[1] * CurvStruct_evec[1])
                + P0P1[2] * CurvStruct_evec[2];
            a = 1.0 / tan(CurvStruct_theta / 2.0);
            d = ((CurvStruct_P0[0] + (CurvStruct_P1[0] - Sign * CurvStruct_evec
                   [0])) + a * EcrP0P1[0]) / 2.0;
            EcrP0P1[0] = d;
            P0P1[0] = CurvStruct_P0[0] - d;
            d = ((CurvStruct_P0[1] + (CurvStruct_P1[1] - Sign * CurvStruct_evec
                   [1])) + a * EcrP0P1[1]) / 2.0;
            EcrP0P1[1] = d;
            P0P1[1] = CurvStruct_P0[1] - d;
            d = ((CurvStruct_P0[2] + (CurvStruct_P1[2] - Sign * CurvStruct_evec
                   [2])) + a * EcrP0P1[2]) / 2.0;
            EcrP0P1[2] = d;
            P0P1[2] = CurvStruct_P0[2] - d;
            i = y->size[0] * y->size[1];
            y->size[0] = 1;
            y->size[1] = u_vec_size[1];
            emxEnsureCapacity_real_T(y, i);
            loop_ub = u_vec_size[1];
            for (i = 0; i < loop_ub; i++) {
                y->data[i] = CurvStruct_theta * u_vec_data[i];
            }

            phi_vec_size[0] = 1;
            phi_vec_size[1] = u_vec_size[1];
            loop_ub = u_vec_size[1];
            for (i = 0; i < loop_ub; i++) {
                phi_vec_data[i] = CurvStruct_theta * u_vec_data[i];
            }

            i = x->size[0] * x->size[1];
            x->size[0] = 1;
            x->size[1] = y->size[1];
            emxEnsureCapacity_real_T(x, i);
            loop_ub = y->size[1];
            for (i = 0; i < loop_ub; i++) {
                x->data[i] = y->data[i];
            }

            i = u_vec_size[1];
            for (k = 0; k < i; k++) {
                x->data[k] = cos(x->data[k]);
            }

            sphi_size[0] = 1;
            sphi_size[1] = y->size[1];
            loop_ub = y->size[1];
            for (i = 0; i < loop_ub; i++) {
                sphi_data[i] = y->data[i];
            }

            i = y->size[1];
            for (k = 0; k < i; k++) {
                sphi_data[k] = sin(sphi_data[k]);
            }

            /*  */
            b_repmat(x->data, x->size, cphiTEcrCP0_data, cphiTEcrCP0_size);
            repmat(P0P1, u_vec_size[1], r3D_data, r3D_size);
            r2D_size[1] = cphiTEcrCP0_size[1];
            loop_ub = cphiTEcrCP0_size[1];
            for (i = 0; i < loop_ub; i++) {
                r2D_data[3 * i] = cphiTEcrCP0_data[3 * i] * r3D_data[3 * i];
                k = 3 * i + 1;
                r2D_data[k] = cphiTEcrCP0_data[k] * r3D_data[k];
                k = 3 * i + 2;
                r2D_data[k] = cphiTEcrCP0_data[k] * r3D_data[k];
            }

            b_repmat(sphi_data, sphi_size, sphiTEcrCP0_data, sphiTEcrCP0_size);
            r3D_size[1] = sphiTEcrCP0_size[1];
            loop_ub = sphiTEcrCP0_size[1];
            for (i = 0; i < loop_ub; i++) {
                r3D_data[3 * i] *= sphiTEcrCP0_data[3 * i];
                k = 3 * i + 1;
                r3D_data[k] *= sphiTEcrCP0_data[k];
                k = 3 * i + 2;
                r3D_data[k] *= sphiTEcrCP0_data[k];
            }

            b_x[0] = CurvStruct_evec[1] * P0P1[2] - CurvStruct_evec[2] * P0P1[1];
            b_x[1] = CurvStruct_evec[2] * P0P1[0] - CurvStruct_evec[0] * P0P1[2];
            b_x[2] = CurvStruct_evec[0] * P0P1[1] - CurvStruct_evec[1] * P0P1[0];
            repmat(b_x, u_vec_size[1], tmp_data, sphi_size);
            loop_ub = cphiTEcrCP0_size[1];
            for (i = 0; i < loop_ub; i++) {
                cphiTEcrCP0_data[3 * i] *= tmp_data[3 * i];
                k = 3 * i + 1;
                cphiTEcrCP0_data[k] *= tmp_data[k];
                k = 3 * i + 2;
                cphiTEcrCP0_data[k] *= tmp_data[k];
            }

            loop_ub = sphiTEcrCP0_size[1];
            for (i = 0; i < loop_ub; i++) {
                sphiTEcrCP0_data[3 * i] *= tmp_data[3 * i];
                k = 3 * i + 1;
                sphiTEcrCP0_data[k] *= tmp_data[k];
                k = 3 * i + 2;
                sphiTEcrCP0_data[k] *= tmp_data[k];
            }

            if (Sign < 0.0) {
                Sign = -1.0;
            } else {
                if (Sign > 0.0) {
                    Sign = 1.0;
                }
            }

            /*  */
            a = Sign * CurvStruct_pitch / 6.2831853071795862;
            repmat(CurvStruct_evec, u_vec_size[1], r1D_data, r1D_size);
            repmat(EcrP0P1, u_vec_size[1], r0D_data, r0D_size);
            b_repmat(phi_vec_data, phi_vec_size, tmp_data, sphi_size);
            r0D_size[0] = 3;
            loop_ub = r0D_size[1];
            for (i = 0; i < loop_ub; i++) {
                r0D_data[3 * i] = ((r0D_data[3 * i] + r2D_data[3 * i]) +
                                   sphiTEcrCP0_data[3 * i]) + a * tmp_data[3 * i]
                    * r1D_data[3 * i];
                k = 3 * i + 1;
                r0D_data[k] = ((r0D_data[k] + r2D_data[k]) + sphiTEcrCP0_data[k])
                    + a * tmp_data[k] * r1D_data[k];
                k = 3 * i + 2;
                r0D_data[k] = ((r0D_data[k] + r2D_data[k]) + sphiTEcrCP0_data[k])
                    + a * tmp_data[k] * r1D_data[k];
            }

            a = Sign * CurvStruct_theta * CurvStruct_pitch / 6.2831853071795862;
            r1D_size[0] = 3;
            r1D_size[1] = sphiTEcrCP0_size[1];
            loop_ub = sphiTEcrCP0_size[1];
            for (i = 0; i < loop_ub; i++) {
                r1D_data[3 * i] = (-CurvStruct_theta * r3D_data[3 * i] +
                                   CurvStruct_theta * cphiTEcrCP0_data[3 * i]) +
                    a * r1D_data[3 * i];
                k = 3 * i + 1;
                r1D_data[k] = (-CurvStruct_theta * r3D_data[k] +
                               CurvStruct_theta * cphiTEcrCP0_data[k]) + a *
                    r1D_data[k];
                k = 3 * i + 2;
                r1D_data[k] = (-CurvStruct_theta * r3D_data[k] +
                               CurvStruct_theta * cphiTEcrCP0_data[k]) + a *
                    r1D_data[k];
            }

            a = pow(CurvStruct_theta, 2.0);
            r2D_size[0] = 3;
            loop_ub = cphiTEcrCP0_size[1];
            for (i = 0; i < loop_ub; i++) {
                r2D_data[3 * i] = -a * r2D_data[3 * i] - a * sphiTEcrCP0_data[3 *
                    i];
                k = 3 * i + 1;
                r2D_data[k] = -a * r2D_data[k] - a * sphiTEcrCP0_data[k];
                k = 3 * i + 2;
                r2D_data[k] = -a * r2D_data[k] - a * sphiTEcrCP0_data[k];
            }

            a = pow(CurvStruct_theta, 3.0);
            r3D_size[0] = 3;
            loop_ub = sphiTEcrCP0_size[1];
            for (i = 0; i < loop_ub; i++) {
                r3D_data[3 * i] = a * r3D_data[3 * i] - a * cphiTEcrCP0_data[3 *
                    i];
                k = 3 * i + 1;
                r3D_data[k] = a * r3D_data[k] - a * cphiTEcrCP0_data[k];
                k = 3 * i + 2;
                r3D_data[k] = a * r3D_data[k] - a * cphiTEcrCP0_data[k];
            }
        }

        emxFree_real_T(&x);
        emxFree_real_T(&y);
    }
}

/*
 * Arguments    : const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 *                double b_u_vec
 *                double r0D[3]
 *                double r1D[3]
 *                double r2D[3]
 *                double r3D[3]
 * Return Type  : void
 */
void b_EvalHelix(const double CurvStruct_P0[3], const double CurvStruct_P1[3],
                 const double CurvStruct_evec[3], double CurvStruct_theta,
                 double CurvStruct_pitch, double b_u_vec, double r0D[3], double
                 r1D[3], double r2D[3], double r3D[3])
{
    double P0P1_idx_0;
    double P0P1_idx_1;
    double P0P1_idx_2;
    double EcrP0P1_idx_0;
    double EcrP0P1_idx_1;
    double EcrP0P1_idx_2;
    int i;
    char message[16];
    double Sign;
    double a;
    char b_message[13];
    double d;
    double phi_vec;
    double EcrCP0_idx_0;
    double EcrCP0_idx_1;
    double EcrCP0_idx_2;
    double d1;
    double a_tmp;

    /*  */
    P0P1_idx_0 = CurvStruct_P1[0] - CurvStruct_P0[0];
    r0D[0] = 0.0;
    r1D[0] = 0.0;
    r2D[0] = 0.0;
    r3D[0] = 0.0;
    P0P1_idx_1 = CurvStruct_P1[1] - CurvStruct_P0[1];
    r0D[1] = 0.0;
    r1D[1] = 0.0;
    r2D[1] = 0.0;
    r3D[1] = 0.0;
    P0P1_idx_2 = CurvStruct_P1[2] - CurvStruct_P0[2];
    r0D[2] = 0.0;
    r1D[2] = 0.0;
    r2D[2] = 0.0;
    r3D[2] = 0.0;
    EcrP0P1_idx_0 = CurvStruct_evec[1] * P0P1_idx_2 - CurvStruct_evec[2] *
        P0P1_idx_1;
    EcrP0P1_idx_1 = CurvStruct_evec[2] * P0P1_idx_0 - CurvStruct_evec[0] *
        P0P1_idx_2;
    EcrP0P1_idx_2 = CurvStruct_evec[0] * P0P1_idx_1 - CurvStruct_evec[1] *
        P0P1_idx_0;

    /*  */
    if (sqrt((pow(EcrP0P1_idx_0, 2.0) + pow(EcrP0P1_idx_1, 2.0)) + pow
             (EcrP0P1_idx_2, 2.0)) <= 2.2204460492503131E-16) {
        for (i = 0; i < 16; i++) {
            message[i] = cv1[i];
        }

        c_assert_(&message[0]);
    } else {
        /*  */
        if ((CurvStruct_pitch != 0.0) && ((CurvStruct_evec[0] * P0P1_idx_0 +
                CurvStruct_evec[1] * P0P1_idx_1) + CurvStruct_evec[2] *
                P0P1_idx_2 <= 2.2204460492503131E-16)) {
            for (i = 0; i < 13; i++) {
                b_message[i] = cv2[i];
            }

            c_assert_(&b_message[0]);
        } else {
            /*  */
            Sign = (P0P1_idx_0 * CurvStruct_evec[0] + P0P1_idx_1 *
                    CurvStruct_evec[1]) + P0P1_idx_2 * CurvStruct_evec[2];
            a = 1.0 / tan(CurvStruct_theta / 2.0);
            d = ((CurvStruct_P0[0] + (CurvStruct_P1[0] - Sign * CurvStruct_evec
                   [0])) + a * EcrP0P1_idx_0) / 2.0;
            EcrP0P1_idx_0 = d;
            P0P1_idx_0 = CurvStruct_P0[0] - d;
            d = ((CurvStruct_P0[1] + (CurvStruct_P1[1] - Sign * CurvStruct_evec
                   [1])) + a * EcrP0P1_idx_1) / 2.0;
            EcrP0P1_idx_1 = d;
            P0P1_idx_1 = CurvStruct_P0[1] - d;
            d = ((CurvStruct_P0[2] + (CurvStruct_P1[2] - Sign * CurvStruct_evec
                   [2])) + a * EcrP0P1_idx_2) / 2.0;
            P0P1_idx_2 = CurvStruct_P0[2] - d;
            phi_vec = CurvStruct_theta * b_u_vec;
            EcrCP0_idx_0 = CurvStruct_evec[1] * P0P1_idx_2 - CurvStruct_evec[2] *
                P0P1_idx_1;
            EcrCP0_idx_1 = CurvStruct_evec[2] * P0P1_idx_0 - CurvStruct_evec[0] *
                P0P1_idx_2;
            EcrCP0_idx_2 = CurvStruct_evec[0] * P0P1_idx_1 - CurvStruct_evec[1] *
                P0P1_idx_0;
            a = cos(phi_vec);
            EcrP0P1_idx_2 = sin(phi_vec);

            /*  */
            r2D[0] = a * P0P1_idx_0;
            r3D[0] = EcrP0P1_idx_2 * P0P1_idx_0;
            d1 = EcrCP0_idx_0;
            EcrCP0_idx_0 *= a;
            d1 *= EcrP0P1_idx_2;
            P0P1_idx_0 = d1;
            r2D[1] = a * P0P1_idx_1;
            r3D[1] = EcrP0P1_idx_2 * P0P1_idx_1;
            d1 = EcrCP0_idx_1;
            EcrCP0_idx_1 *= a;
            d1 *= EcrP0P1_idx_2;
            P0P1_idx_1 = d1;
            r2D[2] = a * P0P1_idx_2;
            r3D[2] = EcrP0P1_idx_2 * P0P1_idx_2;
            d1 = EcrCP0_idx_2;
            EcrCP0_idx_2 *= a;
            d1 *= EcrP0P1_idx_2;
            if (Sign < 0.0) {
                Sign = -1.0;
            } else {
                if (Sign > 0.0) {
                    Sign = 1.0;
                }
            }

            /*  */
            a = Sign * CurvStruct_theta * CurvStruct_pitch / 6.2831853071795862;
            P0P1_idx_2 = pow(CurvStruct_theta, 2.0);
            a_tmp = pow(CurvStruct_theta, 3.0);
            EcrP0P1_idx_2 = Sign * CurvStruct_pitch / 6.2831853071795862 *
                phi_vec;
            r0D[0] = ((EcrP0P1_idx_0 + r2D[0]) + P0P1_idx_0) + EcrP0P1_idx_2 *
                CurvStruct_evec[0];
            r1D[0] = (-CurvStruct_theta * r3D[0] + CurvStruct_theta *
                      EcrCP0_idx_0) + a * CurvStruct_evec[0];
            r2D[0] = -P0P1_idx_2 * r2D[0] - P0P1_idx_2 * P0P1_idx_0;
            r3D[0] = a_tmp * r3D[0] - a_tmp * EcrCP0_idx_0;
            r0D[1] = ((EcrP0P1_idx_1 + r2D[1]) + P0P1_idx_1) + EcrP0P1_idx_2 *
                CurvStruct_evec[1];
            r1D[1] = (-CurvStruct_theta * r3D[1] + CurvStruct_theta *
                      EcrCP0_idx_1) + a * CurvStruct_evec[1];
            r2D[1] = -P0P1_idx_2 * r2D[1] - P0P1_idx_2 * P0P1_idx_1;
            r3D[1] = a_tmp * r3D[1] - a_tmp * EcrCP0_idx_1;
            r0D[2] = ((d + r2D[2]) + d1) + EcrP0P1_idx_2 * CurvStruct_evec[2];
            r1D[2] = (-CurvStruct_theta * r3D[2] + CurvStruct_theta *
                      EcrCP0_idx_2) + a * CurvStruct_evec[2];
            r2D[2] = -P0P1_idx_2 * r2D[2] - P0P1_idx_2 * d1;
            r3D[2] = a_tmp * r3D[2] - a_tmp * EcrCP0_idx_2;
        }
    }
}

/*
 * Arguments    : const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 *                double b_u_vec
 *                double r0D[3]
 * Return Type  : void
 */
void c_EvalHelix(const double CurvStruct_P0[3], const double CurvStruct_P1[3],
                 const double CurvStruct_evec[3], double CurvStruct_theta,
                 double CurvStruct_pitch, double b_u_vec, double r0D[3])
{
    double P0P1_idx_0;
    double P0P1_idx_1;
    double P0P1_idx_2;
    double EcrP0P1_idx_0;
    double EcrP0P1_idx_1;
    double EcrP0P1_idx_2;
    int i;
    char message[16];
    double Sign;
    double a;
    char b_message[13];
    double d;
    double b_idx_2;

    /*  */
    P0P1_idx_0 = CurvStruct_P1[0] - CurvStruct_P0[0];
    r0D[0] = 0.0;
    P0P1_idx_1 = CurvStruct_P1[1] - CurvStruct_P0[1];
    r0D[1] = 0.0;
    P0P1_idx_2 = CurvStruct_P1[2] - CurvStruct_P0[2];
    r0D[2] = 0.0;
    EcrP0P1_idx_0 = CurvStruct_evec[1] * P0P1_idx_2 - CurvStruct_evec[2] *
        P0P1_idx_1;
    EcrP0P1_idx_1 = CurvStruct_evec[2] * P0P1_idx_0 - CurvStruct_evec[0] *
        P0P1_idx_2;
    EcrP0P1_idx_2 = CurvStruct_evec[0] * P0P1_idx_1 - CurvStruct_evec[1] *
        P0P1_idx_0;

    /*  */
    if (sqrt((pow(EcrP0P1_idx_0, 2.0) + pow(EcrP0P1_idx_1, 2.0)) + pow
             (EcrP0P1_idx_2, 2.0)) <= 2.2204460492503131E-16) {
        for (i = 0; i < 16; i++) {
            message[i] = cv1[i];
        }

        c_assert_(&message[0]);
    } else {
        /*  */
        if ((CurvStruct_pitch != 0.0) && ((CurvStruct_evec[0] * P0P1_idx_0 +
                CurvStruct_evec[1] * P0P1_idx_1) + CurvStruct_evec[2] *
                P0P1_idx_2 <= 2.2204460492503131E-16)) {
            for (i = 0; i < 13; i++) {
                b_message[i] = cv2[i];
            }

            c_assert_(&b_message[0]);
        } else {
            /*  */
            Sign = (P0P1_idx_0 * CurvStruct_evec[0] + P0P1_idx_1 *
                    CurvStruct_evec[1]) + P0P1_idx_2 * CurvStruct_evec[2];
            a = 1.0 / tan(CurvStruct_theta / 2.0);
            P0P1_idx_2 = CurvStruct_theta * b_u_vec;
            d = ((CurvStruct_P0[0] + (CurvStruct_P1[0] - Sign * CurvStruct_evec
                   [0])) + a * EcrP0P1_idx_0) / 2.0;
            EcrP0P1_idx_0 = d;
            b_idx_2 = CurvStruct_P0[0] - d;
            P0P1_idx_0 = b_idx_2;
            d = ((CurvStruct_P0[1] + (CurvStruct_P1[1] - Sign * CurvStruct_evec
                   [1])) + a * EcrP0P1_idx_1) / 2.0;
            EcrP0P1_idx_1 = d;
            b_idx_2 = CurvStruct_P0[1] - d;
            P0P1_idx_1 = b_idx_2;
            d = ((CurvStruct_P0[2] + (CurvStruct_P1[2] - Sign * CurvStruct_evec
                   [2])) + a * EcrP0P1_idx_2) / 2.0;
            b_idx_2 = CurvStruct_P0[2] - d;
            a = cos(P0P1_idx_2);
            EcrP0P1_idx_2 = sin(P0P1_idx_2);

            /*  */
            if (Sign < 0.0) {
                Sign = -1.0;
            } else {
                if (Sign > 0.0) {
                    Sign = 1.0;
                }
            }

            /*  */
            P0P1_idx_2 *= Sign * CurvStruct_pitch / 6.2831853071795862;
            r0D[0] = ((EcrP0P1_idx_0 + a * P0P1_idx_0) + EcrP0P1_idx_2 *
                      (CurvStruct_evec[1] * b_idx_2 - CurvStruct_evec[2] *
                       P0P1_idx_1)) + P0P1_idx_2 * CurvStruct_evec[0];
            r0D[1] = ((EcrP0P1_idx_1 + a * P0P1_idx_1) + EcrP0P1_idx_2 *
                      (CurvStruct_evec[2] * P0P1_idx_0 - CurvStruct_evec[0] *
                       b_idx_2)) + P0P1_idx_2 * CurvStruct_evec[1];
            r0D[2] = ((d + a * b_idx_2) + EcrP0P1_idx_2 * (CurvStruct_evec[0] *
                       P0P1_idx_1 - CurvStruct_evec[1] * P0P1_idx_0)) +
                P0P1_idx_2 * CurvStruct_evec[2];
        }
    }
}

/*
 * File trailer for EvalHelix.c
 *
 * [EOF]
 */
