/*
 * Copyright (C) 2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * Copyright (C) 2019 Kevin Joly <kevin.jolyr@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/*
 * Definition and prototypes for the Access Facility library.
 */

#ifndef AF_H
#define AF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <uapi/hal.h>

void af_init(void);
void af_exit(void);

hal_pin_t *pin_find_by_name(const char *name);
hal_data_u *pin_get_value(hal_pin_t *pin);
hal_type_t pin_get_type(hal_pin_t *pin);

hal_pin_t *pin_new(const char *name, hal_type_t type, hal_pin_dir_t dir);
void pin_delete(const char *name);

hal_bit_t* pin_get_bit_ptr(const char* name);
hal_float_t* pin_get_float_ptr(const char* name);
hal_s32_t* pin_get_s32_ptr(const char* name);
hal_u32_t* pin_get_u32_ptr(const char* name);

typedef struct _communicationChannel communicationChannel_t;

/*!
 * \brief Open the communication channel.
 *
 * \param channel Pointer to a non-allocated communicationChannel_t*
 * \return 0 on success, false otherwise
 */
int open_GCodeFilePath_channel(communicationChannel_t **channel);
int open_struct_channel(communicationChannel_t **channel);

/* open_sample_channel() must specify the R/W operation according to the flag: O_RDONLY or O_WRONLY */
int open_sample_channel(communicationChannel_t **channel, int flags);

/*!
 * \brief Close the communication channel.
 *
 * Must be called before terminateChannel.
 *
 * \param channel Pointer to the channel to be closed.
 */
void close_channel(communicationChannel_t *channel);

/*!
 * Write Gcode file path to the FIFO intended for the interpreting program.
 *
 * \param channel Channel to use
 * \param GCode file path
 * \param len Length of the GCode file path
 * \return Path length sent if success, -1 otherwise.
 */
ssize_t writeGCodeFilePath(communicationChannel_t *channel, const char *path, size_t len);


/*!
 * Read Gcode file path to the FIFO intended for the interpreting program.
 *
 * \param channel Channel to use
 * \param path GCode file path to read
 * \param maxlen Maximum length to receive. Expect error if this value is returned.
 * \return Path length sent if success, -1 otherwise.
 */
ssize_t readGCodeFilePath(communicationChannel_t *channel, char *path, size_t maxlen);

/*!
 * Read an arbitrary structure from the channel.
 *
 * \param channel Channel to use
 * \param structData Data to send through the channel
 * \param structSize Size of the data to send
 * \return Size of data sent if success, -1 otherwise
 */
ssize_t writeStruct(communicationChannel_t *channel, const void *structData, size_t structSize);

/*!
 * Write an arbitrary structure on the channel.
 *
 * \param channel Channel to use
 * \param structData Data to read from the channel
 * \param structSize Size of the data to read
 * \return Size of data read if success, -1 otherwise
 */
ssize_t readStruct(communicationChannel_t *channel, void *structData, size_t structSize, unsigned int timeoutms);

/*!
 * Write a sample as structured by sampler component intended to the GUI typically.
 *
 * \param channel Channel to use
 * \param sample Structured sample to be written
 * \param len Length of the sample
 * \return Path length sent if success, -1 otherwise.
 */
ssize_t write_sample(communicationChannel_t *channel, const char *sample, size_t len);

/*!
 * Read a sample from the FIFO intended to the GUI typically.
 *
 * \param channel Channel to use
 * \param sample Sample to be read
 * \param maxlen Maximum length to receive. Expect error if this value is returned.
 * \return Path length sent if success, -1 otherwise.
 */
ssize_t read_sample(communicationChannel_t *channel, char *sample, size_t maxlen);

/*
 * Set the rate (frequency) at which expected samples are stored in the ring.
 * Example: sample_channel_set_freq(10) means a sample will be stored in the ring every 10 samples got from the sampler component.
 * \return True if succeed, false otherwise
 */
bool sample_channel_set_freq(unsigned int freq);

/*
 * Get the current rate (frequency) associated to the sample channel.
 */
int sample_channel_get_freq(void);

/*
 * Enable/disable the production fo samples.
 * \return True if succeed, false otherwise
 */
bool sample_channel_enable(bool enable);

/*
 * Query if the sample channel is enabled or not.
 */
bool sample_channel_enabled(void);

int rtapi_log_enable(bool enable);
int rtapi_log_read(char *msg, ssize_t maxlen);

#ifdef __cplusplus
}
#endif

#endif /* AF_H */
