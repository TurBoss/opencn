#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "feedopt.hpp"

#include <QMainWindow>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>

#include "feedoptwindow.h"
#include <QFileDialog>

#include "pin.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void streamingFileChanged(const QString &path);
    void gcodeFileChanged(const QString &path);
    void timer_update();

    void on_tabWidget_currentChanged(int index);

    void on_push_gcodecommit_clicked();

    void on_push_gcoderefresh_clicked();

    void on_space_pressed();


private:
    Ui::MainWindow *ui;
    QFileDialog *_streamingFileDialog;
    QFileDialog *_gcodeFileDialog;
    QTimer *_timer;
//    FeedoptWindow *_feedopt_window;

    // Mode Select
    PinBool modeHoming{"lcct.set-machine-mode-homing"};
    PinBool modeStream{"lcct.set-machine-mode-stream"};
    PinBool modeJog{"lcct.set-machine-mode-jog"};
    PinBool modeInactive{"lcct.set-machine-mode-inactive"};
    PinBool modeGCode{"lcct.set-machine-mode-gcode"};

    PinBool inModeHoming{"lcct.in-machine-mode-homing"};
    PinBool inModeStream{"lcct.in-machine-mode-stream"};
    PinBool inModeJog{"lcct.in-machine-mode-jog"};
    PinBool inModeInactive{"lcct.in-machine-mode-inactive"};
    PinBool inModeGCode{"lcct.in-machine-mode-gcode"};

    PinBool homingFinished{"lcct.homing-finished"};
    PinBool streamFinished{"lcct.stream-finished"};
    PinBool jogFinished{"lcct.jog-finished"};
    PinBool gcodeFinished{"lcct.gcode-finished"};

    // Homing
    PinBool startHoming{"lcct.home.start-homing-sequence"};
    PinBool stopHoming{"lcct.home.stop-homing-sequence"};

    // Homing done PINs
    PinBool homingDoneX{"lcct.home.homed-0"};
    PinBool homingDoneY{"lcct.home.homed-1"};
    PinBool homingDoneZ{"lcct.home.homed-2"};

    PinDouble homePositionX{"lcct.home-position-X"};
    PinDouble homePositionY{"lcct.home-position-Y"};
    PinDouble homePositionZ{"lcct.home-position-Z"};

    // Spindle
    PinDouble speedSpindle{"lcct.spindle-target-velocity"};
    PinBool activeSpindle{"lcct.spindle-active"};
    PinBool activeAir{"lcct.electrovalve"};

    // Jog axis select
    PinBool JogX{"lcct.jog.axis-X"};
    PinBool JogY{"lcct.jog.axis-Y"};
    PinBool JogZ{"lcct.jog.axis-Z"};

    // Jog relative
    PinDouble relJog{"lcct.jog.move-rel"};
    PinBool plusJog{"lcct.jog.plus"};
    PinBool minusJog{"lcct.jog.minus"};

    // JogAbs
    PinDouble absJog{"lcct.jog.move-abs"};
    PinBool goJog{"lcct.jog.goto"};

    // Jog speed
    PinDouble speedJog{"lcct.jog.velocity"};

    // Jog Stop
    PinBool stopJog{"lcct.jog.stop"};

    // Stream offset
    PinDouble offsetXStream{"lcct.spinbox-offset-X"};
    PinDouble offsetYStream{"lcct.spinbox-offset-Y"};
    PinDouble offsetZStream{"lcct.spinbox-offset-Z"};

    // Stream control
    PinBool startStream{"lcct.stream.start"};
    PinBool stopStream{"lcct.stream.stop"};
    PinBool reloadStream{"lcct.stream.streamer-reload"};

    // GCode control
    PinBool gcodeStart{"lcct.gcode.start-in"};
//    PinBool gcodePrepare{"lcct.gcode.prepare-in"};
    PinBool gcodePause{"lcct.gcode.pause-in"};
    PinBool feedoptReady{"feedopt.ready"};

    // Fault reset
    PinBool faultReset{"lcct.fault-reset"};

    // Curr Pos
    PinDouble currPosX{"lcct.joint-pos-cur-in-0"};
    PinDouble currPosY{"lcct.joint-pos-cur-in-1"};
    PinDouble currPosZ{"lcct.joint-pos-cur-in-2"};

    //Curr Velocity Spindle
    PinDouble currVelSpindle{"lcct.spindle-cur-in"};

    //Modes
    //Inactive
    PinBool curr_modeInactiveX{"lcct.in-mode-inactive-0"};
    PinBool curr_modeInactiveY{"lcct.in-mode-inactive-1"};
    PinBool curr_modeInactiveZ{"lcct.in-mode-inactive-2"};
    PinBool curr_modeInactiveSpindle{"lcct.in-mode-inactive-3"};

    //Fault
    PinBool curr_modeFaultX{"lcct.in-fault-0"};
    PinBool curr_modeFaultY{"lcct.in-fault-1"};
    PinBool curr_modeFaultZ{"lcct.in-fault-2"};
    PinBool curr_modeFaultSpindle{"lcct.in-fault-3"};

    //Homing
    PinBool curr_modeHomingX{"lcct.in-mode-hm-0"};
    PinBool curr_modeHomingY{"lcct.in-mode-hm-1"};
    PinBool curr_modeHomingZ{"lcct.in-mode-hm-2"};
    PinBool curr_modeHomingSpindle{"lcct.in-mode-hm-3"};
    PinBool is_homed{"lcct.homed"};

    //CSP
    PinBool curr_modeCSPX{"lcct.in-mode-csp-0"};
    PinBool curr_modeCSPY{"lcct.in-mode-csp-1"};
    PinBool curr_modeCSPZ{"lcct.in-mode-csp-2"};
    PinBool curr_modeCSPSpindle{"lcct.in-mode-csp-3"};

    //CSP
    PinBool curr_modeCSVX{"lcct.in-mode-csv-0"};
    PinBool curr_modeCSVY{"lcct.in-mode-csv-1"};
    PinBool curr_modeCSVZ{"lcct.in-mode-csv-2"};
    PinBool curr_modeCSVSpindle{"lcct.in-mode-csv-3"};

    // FEEDOPT
    PinDouble feedopt_sample_x{"feedopt.sample-0"};
    PinDouble feedopt_sample_y{"feedopt.sample-1"};
    PinDouble feedopt_sample_z{"feedopt.sample-2"};

    PinDouble feedopt_step_dt{"feedopt.step-dt"};

    PinBool commit_cfg{"feedopt.commit-cfg"};

    PinInt32 queue_size{"feedopt.queue-size"};
    PinDouble current_u{"feedopt.current-u"};
    PinDouble feedrate_scale{"feedopt.feedrate-scale"};
    PinBool feedopt_reset{"lcct.gcode.feedopt-reset"};
    PinBool feedopt_us_active{"feedopt.us-active"};
    PinBool feedopt_rt_active{"feedopt.rt-active"};
//    PinBool wait_start{"lcct.feedopt-wait-start"};

    // shared memory for configuration
    key_t config_key{0};
    int config_shmid{0};
    FeedoptConfigStruct* config_mem{nullptr};

    key_t curv_key{0};
    int curv_shmid{0};
    SharedCurvStructs* curv_mem{nullptr};
    int current_curv_generation = 0;

    std::vector<CurvStruct> curv_structs;

    int curv_fifo_fd;
};

#endif // MAINWINDOW_H
