#include <stdlib.h>
#include <stdio.h>

#include <af.h>
#include <EvalCurvStruct.h>

#define MAX_GCODE_FILEPATH_LEN 256

int sendFile(communicationChannel_t *channel, const char *filePath)
{
	long pos;
	size_t ret;
	size_t size;
	ssize_t rets;
	FILE *f;
	CurvStruct s;

	f = fopen(filePath, "r");
	if (!f) {
		return -1;
	}

	/* We need to know how much elements there is in the file and tell it to the receiver. */
	if (fseek(f, 0, SEEK_END)) {
		fprintf(stderr, "fseek failed\n");
		fclose(f);
		return -1;
	}

	pos = ftell(f);
	if (pos < 0) {
		fprintf(stderr, "ftell failed\n");
		fclose(f);
		return -1;
	}

	size = ((unsigned long) pos)/sizeof(s);

	rets = writeStruct(channel, &size, sizeof(size));
	if (rets != sizeof(size)) {
		fprintf(stderr, "writeStruct failed");
		fclose(f);
		return -1;
	}

	/* Read every structure and send it to the receiver. */
	if (fseek(f, 0, SEEK_SET)) {
		fprintf(stderr, "fseek failed\n");
		fclose(f);
		return -1;
	}

	do {
		ret = fread(&s, sizeof(s), 1, f);
		if (ret == 1) {
			rets = writeStruct(channel, &s, sizeof(s));
			if (rets != sizeof(s)) {
				fprintf(stderr, "writeStruct failed");
				fclose(f);
				return -1;
			}
		}
	} while (ret == 1);

	fclose(f);

	return 0;
}

int main(void)
{
	int ret;
	ssize_t rets;
	communicationChannel_t *path_channel, *struct_channel;
	char gcodeFilePath[MAX_GCODE_FILEPATH_LEN];

	ret = open_GCodeFilePath_channel(&path_channel);
	if (ret) {
		fprintf(stderr, "open_GCodeFilePath_channel() failed\n");
		return EXIT_FAILURE;
	}

	ret = open_struct_channel(&struct_channel);
	if (ret) {
		close_channel(path_channel);
		fprintf(stderr, "open_struct_channel() failed\n");
		return EXIT_FAILURE;
	}

	rets = readGCodeFilePath(path_channel, gcodeFilePath, MAX_GCODE_FILEPATH_LEN);
	if ((rets <= 0) || (rets >= MAX_GCODE_FILEPATH_LEN)) {
		fprintf(stderr, "Bad GCode file size\n");
		close_channel(path_channel);
		close_channel(struct_channel);
		return EXIT_FAILURE;
	}

	sendFile(struct_channel, gcodeFilePath);

	close_channel(path_channel);
	close_channel(struct_channel);

	return EXIT_SUCCESS;
}
