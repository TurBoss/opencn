#ifndef GL_CAMERA_H
#define GL_CAMERA_H

#include <QQuaternion>
#include <QVector3D>
#include <cmath>

class GlCamera {
public:
    GlCamera();

    void look();

    /*
     * \param pos: Position in the global frame
     */
    void setPosition(const QVector3D& pos);

    QVector3D getPosition();

    void setOrbitTargetPoint(const QVector3D& target);

    /*
     * \param pos: Quaternion in the global frame
     */
    void setCameraQuaternion(const QQuaternion& quat);

    QQuaternion getCameraQuaternion();

    void incrementRotationX(float inc);
    void incrementRotationY(float inc);

    void incrementPositionX(float inc);
    void incrementPositionY(float inc);
    void incrementPositionZ(float inc);

public:
    enum CameraMode {
        ORBIT,
    };

private:
    void update();

private:
    QQuaternion _quaternion; /* Quaternion of the camera */
    QVector3D _position; /* Position of the camera in the global frame */
    QVector3D _target; /* target point where the camera is looking */
    QVector3D _orbitTarget;
    CameraMode _mode;

    float _orbit_pitch = M_PI_4, _orbit_yaw = M_PI_4;
    float _orbit_distance = 80e-3;
};

#endif /* GL_CAMERA_H */
