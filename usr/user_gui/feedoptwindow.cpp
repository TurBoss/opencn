#include "feedoptwindow.h"
#include "ui_feedoptwindow.h"

#include <QGraphicsTextItem>
#include <QDebug>


FeedoptWindow::FeedoptWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::FeedoptWindow)
{
    ui->setupUi(this);
    _timer = new QTimer(this);
    connect(_timer, &QTimer::timeout, this, &FeedoptWindow::timer_update);
    _timer->setInterval(30);
    _timer->start();

//    connect(ui->check_rt_active, SIGNAL(toggled(bool)), &rt_active, SLOT(setValue(bool)));
//    connect(ui->check_us_active, SIGNAL(toggled(bool)), &us_active, SLOT(setValue(bool)));
//    connect(ui->push_single_shot, SIGNAL(pressed()), &rt_single_shot, SLOT(setTrue()));

    ui->progress_queue_size->setMaximum(FEEDOPT_RT_QUEUE_SIZE);

    // setup shared memory
    config_key = ftok("feedopt_config", 65);
    config_shmid = shmget(config_key, sizeof(FeedoptConfigStruct), 0666 | IPC_CREAT);
    config_mem = static_cast<FeedoptConfigStruct*>(shmat(config_shmid, (void*)0, 0));
}

FeedoptWindow::~FeedoptWindow() {
    delete ui;
    shmdt(config_mem);
}

void FeedoptWindow::timer_update()
{
    ui->spin_feedopt_sample_x->setValue(feedopt_sample_x.getValue());
    ui->spin_feedopt_sample_y->setValue(feedopt_sample_y.getValue());
    ui->spin_feedopt_sample_z->setValue(feedopt_sample_z.getValue());

//    ui->check_rt_active->setChecked(rt_active.getValue());
//    ui->check_us_active->setChecked(us_active.getValue());

    ui->progress_queue_size->setValue(queue_size.getValue());
    ui->spin_step_dt->setValue(feedopt_step_dt.getValue()*1000);
}

void FeedoptWindow::on_push_commit_clicked()
{
    if (!config_mem)
    {
        qDebug() << "COMMIT: config_mem = 0";
        return;
    }

    qDebug() << "COMMIT: OK";
    config_mem->NHorz = ui->spin_nhorz->value();
    config_mem->NDiscr = ui->spin_nhorz->value();
    config_mem->SplineDegree = ui->spin_nhorz->value();
    config_mem->NBreak = ui->spin_nbreak->value();
    config_mem->LSplit = ui->spin_lsplit->value();
    config_mem->CutOff = ui->spin_cutoff->value();

    commit_cfg.setTrue();
}

void FeedoptWindow::on_push_refresh_clicked()
{
    if (!config_mem) return;

    ui->spin_nhorz->setValue(config_mem->NHorz);
    ui->spin_ndiscr->setValue(config_mem->NDiscr);
    ui->spin_splinedegree->setValue(config_mem->SplineDegree);
    ui->spin_nbreak->setValue(config_mem->NBreak);
    ui->spin_lsplit->setValue(config_mem->LSplit);
    ui->spin_cutoff->setValue(config_mem->CutOff);
}
