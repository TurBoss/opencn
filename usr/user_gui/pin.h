#ifndef PIN_H
#define PIN_H

#include <QObject>
#include <QtGlobal>

#include "af.h"

/**
 * Pin class. Must be created thanks to the PinFactoryClass.
 */
class Pin : public QObject
{

    Q_OBJECT

public:
    /**
     * \param halPin Internal pin
     * \param local If the pin was created locally (true) or not (false)
     */
    Pin(hal_pin_t *halPin, bool local = false);

    bool getValueBit();
    qint32 getValueS32();
    quint32 getValueU32();
    float getValueF32();

    bool isLocal();
    const char *getName();

public slots:

    void setValue(bool bit);
    void setValue(qint32 s);
    void setValue(quint32 u);
    void setValue(float f);

private:
    hal_pin_t *_halPin;
    hal_data_u *_halData;
    bool _local;
};

class PinDouble : public QObject
{
    Q_OBJECT
public:
    PinDouble(const QString &name, bool local = false);
    ~PinDouble();
    void update();
    double getValue();

public slots:
    void setValue(double d);

signals:
    void valueChanged(double d);

private:
    hal_pin_t *_halPin{nullptr};
    volatile real_t *_data{nullptr};
    bool _local{false};
    QString _name;
};

class PinBool : public QObject
{
    Q_OBJECT
public:
    PinBool(const QString &name, bool local = false);
    ~PinBool();
    void update();
    bool getValue();

public slots:
    void setValue(bool b = true);
    void setTrue();
    void setFalse();

signals:
    void valueChanged(bool);

private:
    hal_pin_t *_halPin{nullptr};
    volatile bool *_data{nullptr};
    bool _local{false};
    QString _name;
};

class PinInt32 : public QObject
{
    Q_OBJECT
public:
    PinInt32(const QString &name, bool local = false);
    ~PinInt32();
    void update();
    int getValue();

public slots:
    void setValue(int v);

signals:
    void valueChanged(int);

private:
    hal_pin_t *_halPin{nullptr};
    volatile int *_data{nullptr};
    bool _local{false};
    QString _name;
};

#endif /* PIN_H */
