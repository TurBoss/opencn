#include "axes.h"

#include <GL/glu.h>

const float Axes::AxesLength = 0.02f;
const float Axes::AxesTextLength = 0.001f;
const float Axes::AxesTextSize = 0.005f;

void Axes::draw()
{
    glEnable(GL_COLOR_MATERIAL);
    glLineWidth(1.0f);

    /* OpenCN X axis (X OpenGL axis) */
    glColor3f(0.0f, 1.0f, 0.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(AxesLength, 0.0f, 0.0f);
    glEnd();

    glPushMatrix();
    glTranslatef(AxesLength + AxesTextLength, 0.0, -AxesTextSize/2.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(AxesTextSize, 0.0f, AxesTextSize);
        glVertex3f(AxesTextSize, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, AxesTextSize);
    glEnd();
    glPopMatrix();

    /* OpenCN Y axis (-Z OpenGL axis) */
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, -AxesLength);
    glEnd();

    glPushMatrix();
    glTranslatef(-AxesTextSize/2.0f, 0.0f, -(AxesLength + AxesTextLength));
    glBegin(GL_LINES);
        glVertex3f(AxesTextSize/2.0f, 0.0f, 0.0f);
        glVertex3f(AxesTextSize/2.0f, 0.0f, -AxesTextSize/2.0f);
        glVertex3f(AxesTextSize/2.0f, 0.0f, -AxesTextSize/2.0f);
        glVertex3f(0.0f, 0.0f, -AxesTextSize);
        glVertex3f(AxesTextSize/2.0f, 0.0f, -AxesTextSize/2.0f);
        glVertex3f(AxesTextSize, 0.0f, -AxesTextSize);
    glEnd();
    glPopMatrix();

    /* OpenCN Z axis (Y OpenGL axis) */
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, AxesLength, 0.0f);
    glEnd();

    glPushMatrix();
    glTranslatef(-AxesTextSize/2.0f, AxesLength + AxesTextLength, 0.0f);
    glBegin(GL_LINES);
        glVertex3f(AxesTextSize, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(AxesTextSize, AxesTextSize, 0.0f);
        glVertex3f(AxesTextSize, AxesTextSize, 0.0f);
        glVertex3f(0.0f, AxesTextSize, 0.0f);
    glEnd();
    glPopMatrix();

    /* OpenCN workspace cube*/
    glColor3f(1.0f, 1.0f, 0.0f);

    // top face
    glBegin(GL_LINE_LOOP);
        glVertex3f(xmin, zmin, ymin);
        glVertex3f(xmin, zmin, ymax);
        glVertex3f(xmax, zmin, ymax);
        glVertex3f(xmax, zmin, ymin);
    glEnd();

    // bottom face
    glBegin(GL_LINE_LOOP);
        glVertex3f(xmin, zmax, ymin);
        glVertex3f(xmin, zmax, ymax);
        glVertex3f(xmax, zmax, ymax);
        glVertex3f(xmax, zmax, ymin);
    glEnd();

    // origin face
    glBegin(GL_LINE_LOOP);
        glVertex3f(xmin, 0, ymin);
        glVertex3f(xmin, 0, ymax);
        glVertex3f(xmax, 0, ymax);
        glVertex3f(xmax, 0, ymin);
    glEnd();

    // vertical edges
    glBegin(GL_LINES);
        glVertex3f(xmin, zmax, ymin);
        glVertex3f(xmin, zmin, ymin);

        glVertex3f(xmin, zmax, ymax);
        glVertex3f(xmin, zmin, ymax);

        glVertex3f(xmax, zmax, ymax);
        glVertex3f(xmax, zmin, ymax);

        glVertex3f(xmax, zmax, ymin);
        glVertex3f(xmax, zmin, ymin);
    glEnd();

    glDisable(GL_COLOR_MATERIAL);
}
