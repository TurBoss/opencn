#ifndef AXES_H
#define AXES_H

class Axes
{
public:
    void draw();

private:
    static const float AxesLength;
    static const float AxesTextLength;
    static const float AxesTextSize;

    static constexpr float xmin = -24e-3;
    static constexpr float xmax = 24e-3;
    static constexpr float ymin = -24e-3;
    static constexpr float ymax = 24e-3;
    static constexpr float zmin = -20e-3;
    static constexpr float zmax = 30e-3;
};

#endif /* AXES_H */
