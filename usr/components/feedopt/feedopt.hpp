#pragma once
#include <vector>
#include <semaphore.h>
#include <matlab_headers.h>

extern "C" {
//#include <uapi/matlab/gen_matlab_us/ConstrHelixStruct.h>
//#include <uapi/matlab/gen_matlab_us/ConstrLineStruct.h>
}

#define MAX_SHARED_CURV_STRUCTS (1024*1024)

struct SharedCurvStructs {
	sem_t mutex;
	int generation;
	int curv_struct_count;
	CurvStruct curv_structs[MAX_SHARED_CURV_STRUCTS];
};

//#define DUMMY_FN do {fprintf(stderr, "DUMMY_CALL to %s%s%s\n", ASCII_CYAN, __FUNCTION__, ASCII_RESET);} while(0)
//#define FNLOG(fmt, args...) fprintf(stderr, "[%s%s%s] " fmt "\n", ASCII_GREEN,__FUNCTION__, ASCII_RESET, ##args);

#define DUMMY_FN
#define FNLOG(...)

