#pragma once

#include "config.h"

#define __FILENAME__ (strrchr("/" __FILE__, '/') + 1)

/*
// #define DBG(msg, ...)                                                                                                  \
//     do {                                                                                                               \
//         fprintf(stderr, "%s:%d: ", __FILENAME__, __LINE__);                                                            \
//         fprintf(stderr, msg __VA_OPT__(, ) __VA_ARGS__);                                                               \
//     } while (0);
*/


#define UNIMPLEMENTED(expr)                                                                                            \
    do {                                                                                                               \
        fprintf(stderr, "[UNIMPLEMENTED] %s:%d: \n", __FILENAME__, __LINE__);                                          \
        fprintf(stderr, #expr);                                                                                        \
        fprintf(stderr, "----------------------\n");                                                                   \
    } while (0);

#define UNSUPPORTED(expr)                                                                                              \
    do {                                                                                                               \
        fprintf(stderr, "[UNSUPPORTED] %s:%d: \n", __FILENAME__, __LINE__);                                            \
        fprintf(stderr, #expr);                                                                                        \
        fprintf(stderr, "----------------------\n");                                                                   \
    } while (0);

void qc_reset();

// #define INCH_PER_MM (1.0 / 25.4)

// constexpr int CANON_POCKETS_MAX = 1;
