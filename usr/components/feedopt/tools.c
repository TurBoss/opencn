#include "tools.h"
#include <stdio.h>

void spline_base_eval(unsigned long *handle, int n_sample, double *x, spline_base_t *base) {
    base->n_sample = n_sample;
    base->n_coeff = c_bspline_ncoeff(handle);
    base->size_val[0] = base->n_sample;
    base->size_val[1] = base->n_coeff;
    base->size_integr[0] = base->n_coeff;
    base->size_integr[1] = 1;
    c_bspline_base_eval(handle, n_sample, x, base->BasisVal, base->BasisValD, base->BasisValDD, base->BasisIntegr);
}
