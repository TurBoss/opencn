/********************************************************************
 * Description:  sampler.c
 *               User space part of "streamer", a HAL component that
 *		can be used to stream data from a file onto HAL pins
 *		 at a specific realtime sample rate.
 *
 * Author: John Kasunich <jmkasunich at sourceforge dot net>
 * License: GPL Version 2
 *
 * Copyright (c) 2006 All rights reserved.
 *
 ********************************************************************/
/** This file, 'streamer_usr.c', is the user part of a HAL component
 that allows numbers stored in a file to be "streamed" onto HAL
 pins at a uniform realtime sample rate.  When the realtime module
 is loaded, it creates a stream in shared memory.  Then, the user
 space program 'hal_stream' is invoked.  'hal_stream' takes
 input from stdin and writes it to the stream, and the realtime
 part transfers the data from the stream to HAL pins.

 Invoking:


 'chan_num', if present, specifies the streamer channel to use.
 The default is channel zero.  Since hal_stream takes its data
 from stdin, it will almost always either need to have stdin
 redirected from a file, or have data piped into it from some
 other program.
 */

/** This program is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General
 Public License as published by the Free Software Foundation.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 THE AUTHORS OF THIS LIBRARY ACCEPT ABSOLUTELY NO LIABILITY FOR
 ANY HARM OR LOSS RESULTING FROM ITS USE.  IT IS _EXTREMELY_ UNWISE
 TO RELY ON SOFTWARE ALONE FOR SAFETY.  Any machinery capable of
 harming persons must have provisions for completely removing power
 from all motors, etc, before persons enter any danger area.  All
 machinery must be designed to comply with local and national safety
 codes, and the authors of this software can not, and do not, take
 any responsibility for such compliance.

 This code was written as part of the EMC HAL project.  For more
 information, go to www.linuxcnc.org.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <sampler.h>
#include <af.h>
#include <cmdline.h>

#include <uapi/sampler.h>

#include <debug.h>

int exitval = 1; /* program return code - 1 means error */
int ignore_sig = 0; /* used to flag critical regions */
int linenumber = 0; /* used to print linenumber on errors */
char comp_name[HAL_NAME_LEN + 1]; /* name for this instance of streamer */

int devfd;

/*
 * Connect to the kernel HAL.
 * @mod_name: name of the component (unique)
 * @returns a comp_id (component ID).
 */
int sampler_connect(char *comp_name, char *devname, int channel, int depth, char *cfg) {
	sampler_connect_args_t args = {0};
	int ret;

	devfd = open(devname, O_RDWR);

	if (comp_name)
		strncpy(args.name, comp_name, sizeof(args.name));
	args.channel = channel;
	args.depth = depth;
	if (cfg)
		strncpy(args.cfg, cfg, sizeof(args.cfg));

	ret = ioctl(devfd, SAMPLER_IOCTL_CONNECT, &args);
	if (ret != 0)
		BUG()

	printf("hal_connect returned with ret = %d\n", ret);

	return ret;
}

void sampler_disconnect(void) {

	ioctl(devfd, SAMPLER_IOCTL_DISCONNECT);

	close(devfd);
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/* signal handler */
static sig_atomic_t stop;
static void quit(int sig) {
	if (ignore_sig) {
		return;
	}
	stop = 1;
}

#define BUF_SIZE 120

/*
 * Syntax: sampler depth=x cfg=y filename
 */
int main(int argc, char **argv) {
	int n, channel, tag;
	char *cp, *cp2;
	long int samples;
	char buf[BUF_SIZE];
	int ret;
	unsigned int this_sample, last_sample = 0;
	int depth = 0;
	char *cfg = "";
	char *params = NULL, *value = NULL;
	int fd;
	char filename[255] = { 0 };
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	char *folder_separator;
	char folder_path[255] = { 0 };
	struct stat st = { 0 };
	communicationChannel_t *sample_channel;
	/* set return code to "fail", clear it later if all goes well */
	exitval = 1;
	channel = 0;
	tag = 0;
	samples = -1; /* -1 means run forever */
	uint32_t curr_sample_freq = 0;

	/* Check for an existing pin giving the number of samples retrieved for the GUI. */
	sample_channel_set_freq(SAMPLE_CHANNEL_FREQ_NR);

	/* Open the pipe communication channel to store samples intended to various consumers (such as the GUI for example) */
	open_sample_channel(&sample_channel, O_WRONLY);

	for (n = 1; n < argc; n++) {
		cp = argv[n];

		/* First parse the option with "=" */

		next_arg(argv[n], &params, &value);

		if (params) {
			if (!strcmp(params, "depth")) {
				depth = atoi(value);
				continue ;
			}

			if (!strcmp(params, "cfg")) {
			cfg = value;
			continue ;
			}
		}

		if (*cp != '-') {
			break;
		}
		switch (*(++cp)) {
			case 'c':
				if ((*(++cp) == '\0') && (++n < argc)) {
					cp = argv[n];
				}
				channel = strtol(cp, &cp2, 10);
				if ((*cp2) || (channel < 0) || (channel >= MAX_SAMPLERS)) {
					fprintf(stderr, "ERROR: invalid channel number '%s'\n", cp);
					exit(1);
				}
				break;
			case 'n':
				if ((*(++cp) == '\0') && (++n < argc)) {
					cp = argv[n];
				}
				samples = strtol(cp, &cp2, 10);
				if ((*cp2) || (samples < 0)) {
					fprintf(stderr, "ERROR: invalid sample count '%s'\n", cp);
					exit(1);
				}
				break;
			case 't':
				tag = 1;
				break;
			default:
				fprintf(stderr, "ERROR: unknown option '%s'\n", cp);
				exit(1);
				break;
		}
	}
	if (n < argc) {

		if (argc > n + 1) {
			fprintf(stderr, "ERROR: At most one filename may be specified\n");
			exit(1);
		}
		/*
		 * make stdout be the named file
		 * generate unique filename using the current date+time and the user-supplied name
		 * find the last occurence of '/' in the user filename -> handle folders
		 */

		folder_separator = strrchr(argv[n], '/');
		if (folder_separator == NULL) {
			snprintf(filename, sizeof(filename), "%d-%d-%d_%d-%d-%d_%s", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
											tm.tm_hour, tm.tm_min, tm.tm_sec,
											argv[n]);
		} else {
			memcpy(folder_path, argv[n], folder_separator - argv[n]);

			/* create folder if it does not exist */
			if (stat(folder_path, &st) == -1) {
				mkdir(folder_path, 0666);
			}
			snprintf(filename, sizeof(filename), "%s/%d-%d-%d_%d-%d-%d_%s", folder_path, tm.tm_year + 1900,
											tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
											tm.tm_min, tm.tm_sec,
											folder_separator + 1);
		}

		fd = open(filename, O_WRONLY | O_CREAT, 0666);
		close(1);
		dup2(fd, 1);
	}
	/* register signal handlers - if the process is killed
	 we need to call hal_exit() to free the shared memory */
	signal(SIGINT, quit);
	signal(SIGTERM, quit);
	signal(SIGPIPE, SIG_IGN);

	/* connect to HAL */
	/* create a unique module name, to allow for multiple streamers */
	snprintf(comp_name, sizeof(comp_name), "halsampler%d", getpid());

	/* connect to the HAL */
	ignore_sig = 1;
	ret = sampler_connect(comp_name, SAMPLER_DEV_NAME, channel, depth, cfg);
	ignore_sig = 0;

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	/* check result */
	if (ret < 0) {
		fprintf(stderr, "ERROR: sampler_connect failed with ret = %d\n", ret);
		goto out;
	}

	while (!stop && (samples != 0)) {

		this_sample = read(devfd, buf, BUF_SIZE);

		++last_sample;
		if (this_sample != last_sample) {
			printf("overrun\n");
			last_sample = this_sample;
		}

		if (tag)
			printf("%d ", this_sample-1);

		printf("%s\n", buf);

		if (sample_channel_enabled()) {
			/* Forward the entry in the sample channel so that the GUI can use it. */
			if (curr_sample_freq == 0) {
				ret = write_sample(sample_channel, buf, strlen(buf)+1);
				if (ret == 0)
					printf("!! sample ring buffer full - skipping this sample\n");
			}
			curr_sample_freq = (curr_sample_freq + 1) % sample_channel_get_freq();
		}

		if (samples > 0)
			samples--;

	}

	/* run was succesfull */
	exitval = 0;

	close_channel(sample_channel);

out:
	ignore_sig = 1;

	sampler_disconnect();

	return exitval;
}
