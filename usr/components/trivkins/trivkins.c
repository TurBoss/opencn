/********************************************************************
* Description: trivkins.c
*   general trivkins for 3 axis Cartesian machine
*
*   Derived from a work by Fred Proctor & Will Shackleford
*
* License: GPL Version 2
*
* Copyright (c) 2009 All rights reserved.
*
********************************************************************/

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include <cmdline.h>

#include <uapi/kins.h>

static int devfd;

int kins_connect(char *comp_name, char *devname, kins_connect_args_t *args)
{
	int ret;

	printf("comp_name: %s\n", comp_name);
	printf("type: %s\n", args->type);
	printf("coordinates: %s\n", args->coordinates);

	devfd = open(devname, O_RDWR);
		if (!devfd) {
		fprintf(stderr, "%s: ERROR: failed to open %s\n", comp_name, KINS_DEV_NAME);
		return -EIO;
	}

	ret = ioctl(devfd, KINS_IOCTL_CONNECT, args);
	printf("ret: %d\n", ret);

	return ret;
}

/*
 * Syntax: trivkins type=1 coordinates=XYZ
 */
int main(int argc, char **argv)
{
	int exitval = -1;
	char *params = NULL, *value = NULL;
	char *comp_name = "trivkins";
	int ret;
	int n;
	kins_connect_args_t args = {0};

	// args.type = "";

	if (argc != 3)
		goto out;

	for (n = 1; n < argc; n++) {

		next_arg(argv[n], &params, &value);

		if (params) {
			if (!strcmp(params, "coordinates")) {
				strncpy(args.coordinates, value, sizeof(args.coordinates));
				continue;
			}

			if (!strcmp(params, "type")) {
				strncpy(args.type, value, sizeof(args.type));
				continue;
			}

			/* wrong parameter */
			goto out;

		} else {
			/* no parameter found */
			goto out;
		}
	}

	ret = kins_connect(comp_name, KINS_DEV_NAME, &args);
	if (!ret) {
		goto out;
	}

	exitval = 0;

out:
	if (exitval)
		fprintf(stderr, "ERROR: invalid parameters\n");

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	return exitval;
}
