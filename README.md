# OpenCN Release 1.0

# Qemu compilation

./configure --target-list=x86_64-softmmu


# Linux compilation
make x86_64_defconfig

# Rootfs 
## rootfs in RAM
.config => INITRAMFS_SOURCE="../rootfs/images/rootfs.cpio"


# Build qemu-4.1.0 with qxl (spice)
## Install packages
**Debian**
sudo apt install libspice-protocol-dev libspice-server-dev libpixman-1-dev libepoxy-dev libgbm-dev libegl1-mesa-dev libsdl2-dev

## Configure
./configure --target-list=x86_64-softmmu --enable-kvm --enable-spice --enable-avx2 --enable-opengl --enable-sdl
